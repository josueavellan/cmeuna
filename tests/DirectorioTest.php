<?php

class DirectorioTest extends \PHPUnit\Framework\TestCase{

  
    /** @test */
    public function asignar_obtener_cedula_encargado_directorio(){
        
        $directorio = new \CDominioT\directorio;
        

        $directorio->setCedulaIntegranteD(112450256);
        $this->assertEquals($directorio->getCedulaIntegranteD(), 112450256);
    }


    /** @test */
    public function asignar_obtener_puesto_integrante_directorio(){
        
        $directorio = new \CDominioT\directorio;
            
    
        $directorio->setPuestoIntegranteD('Asistente');
        $this->assertEquals($directorio->getPuestoIntegranteD(), 'Asistente');
    }




    /** @test */
    public function asignar_obtener_apellido1_integrante_directorio(){
        
        $directorio = new \CDominioT\directorio;
            
    
        $directorio->setApellido1IntegranteD('gonzales');
        $this->assertEquals($directorio->getApellido1IntegranteD(), 'gonzales');
    }



    /** @test */
    public function asignar_obtener_apellido2_integrante_directorio(){
        
        $directorio = new \CDominioT\directorio;
            
    
        $directorio->setApellido2IntegranteD('jimenez');
        $this->assertEquals($directorio->getApellido2IntegranteD(), 'jimenez');
    }


}