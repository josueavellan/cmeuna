<?php


	class directorio{

		private $cedulaIntegranteD;
		private $nombreIntegranteD;
		private $apellido1IntegranteD;
		private $apellido2IntegranteD;
		private $telefonoIntegranteD;
		private $institucionIntegranteD;
		private $puestoIntegranteD;
		private $comiteIntegranteD;
		private $correo;

		public function directorio($cedulaIntegranteD,$nombreIntegranteD,$apellido1IntegranteD,$apellido2IntegranteD,$telefonoIntegranteD,$institucionIntegranteD,$puestoIntegranteD, $comiteIntegranteD, $correoIntegranteD){

			$this->cedulaIntegranteD = $cedulaIntegranteD;
			$this->nombreIntegranteD = $nombreIntegranteD;
			$this->apellido1IntegranteD = $apellido1IntegranteD;
			$this->apellido2IntegranteD = $apellido2IntegranteD;
			$this->telefonoIntegranteD = $telefonoIntegranteD;
			$this->institucionIntegranteD = $institucionIntegranteD;
			$this->puestoIntegranteD = $puestoIntegranteD;
			$this->comiteIntegranteD = $comiteIntegranteD;
			$this->correoIntegranteD = $correoIntegranteD;
		}

		public function setCedulaIntegranteD($cedulaIntegranteD){

			$this->cedulaIntegranteD = $cedulaIntegranteD;
		}

		public function setNombreIntegranteD($nombreIntegranteD){

			$this->nombreIntegranteD = $nombreIntegranteD;
		}

		public function setApellido1IntegranteD($apellido1IntegranteD){

			$this->apellido1IntegranteD = $apellido1IntegranteD;
		}

		public function setApellido2IntegranteD($apellido2IntegranteD){

			$this->apellido2IntegranteD = $apellido2IntegranteD;
		}

		public function setTelefonoIntegranteD($telefonoIntegranteD){

			$this->telefonoIntegranteD = $telefonoIntegranteD;
		}

		public function setInstitucionIntegranteD($institucionIntegranteD){

			$this->institucionIntegranteD = $institucionIntegranteD;
		}

		public function setPuestoIntegranteD($puestoIntegranteD){

			$this->puestoIntegranteD = $puestoIntegranteD;
		}

		public function setComiteIntegranteD($comiteIntegranteD){
			$this->comiteIntegranteD = $comiteIntegranteD;
		}

		public function setCorreoIntegrante($correoIntegranteD){
			$this->correoIntegranteD = $correoIntegranteD;
		}
		public function getCedulaIntegranteD(){

			return $this->cedulaIntegranteD;
		}

		public function getNombreIntegranteD(){

			return $this->nombreIntegranteD;
		}

		public function getApellido1IntegranteD(){

			return $this->apellido1IntegranteD;
		}

		public function getApellido2IntegranteD(){

			return $this->apellido2IntegranteD;
		}

		public function getTelefonoIntegranteD(){

			return $this->telefonoIntegranteD;
		}

		public function getInstitucionIntegranteD(){

			return $this->institucionIntegranteD;
		}

		public function getPuestoIntegranteD(){

			return $this->puestoIntegranteD;
		}

		public function getComiteIntegranteD(){
			return $this->comiteIntegranteD;
		}

		public function getCorreoIntegranteD(){
			return $this->correoIntegranteD;
		}
		public function toString(){

			return $this->cedulaIntegranteD.", ".$this->nombreIntegranteD.", ". $this->apellido1IntegranteD.", ". $this->apellido2IntegranteD.", ".$this->telefonoIntegranteD.", ".$this->institucionIntegranteD.", ".$this->puestoIntegranteD;
		}
	}

?>