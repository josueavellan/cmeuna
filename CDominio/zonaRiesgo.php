<?php

	class zonaRiesgo {

		private $id;
		private $lugar;
		private $tipo;
		private $albergue;
		private $latitud;
		private $longitud;

		public function zonaRiesgo($id, $lugar, $tipo, $albergue, $latitud, $longitud) {

			$this->id = $id;
			$this->lugar = $lugar;
			$this->tipo = $tipo;
			$this->albergue = $albergue;
			$this->latitud = $latitud;
			$this->longitud = $longitud;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}

		public function setLugar($lugar) {

			$this->lugar = $lugar;
		}

		public function setTipo($tipo) {

			$this->tipo = $tipo;
		}

		public function setAlbergue($albergue) {

			$this->albergue = $albergue;
		}

		public function setLatitud($latitud) {

			$this->latitud = $latitud;
		}

		public function setLongitud($longitud) {

			$this->longitud = $longitud;
		}

		// Gets...
		public function getId() {

			return $this->id;
		}

		public function getLugar() {

			return $this->lugar;
		}

		public function getTipo() {

			return $this->tipo;
		}

		public function getAlbergue() {

			return $this->albergue;
		}

		public function getLatitud() {

			return $this->latitud;
		}

		public function getLongitud() {

			return $this->longitud;
		}

		// toString...
		public function toString() {

			return $this->id."_".$this->lugar."_".$this->tipo."_".$this->albergue."_".$this->latitud."_".$this->longitud;
		}
	}
?>