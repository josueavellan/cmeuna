<?php


	class vehiculo{

		private $placa;
		private $tipoVehiculo;
		private $tipoCombustible;
		private $capacidad;
		private $descripcion;
		private $institucion;

		public function vehiculo($placa,$tipoVehiculo,$tipoCombustible,$capacidad,$descripcion,$institucion){

			$this->placa = $placa;
			$this->tipoVehiculo = $tipoVehiculo;
			$this->tipoCombustible = $tipoCombustible;
			$this->capacidad = $capacidad;
			$this->descripcion = $descripcion;
			$this->institucion = $institucion;

		}

		public function setPlaca($placa){
			$this->placa = $placa;
		}
		public function setTipoVehiculo($tipoVehiculo){
			$this->tipoVehiculo = $tipoVehiculo;
		}
		public function setTipoCombustible($tipoCombustible){
			$this->tipoCombustible = $tipoCombustible;
		}
		public function setCapacidad($capacidad){
			$this->capacidad = $capacidad;
		}
		public function setDescripcion($descripcion){
			$this->descripcion = $descripcion;
		}
		public function setInstitucion($institucion){
			$this->institucion = $institucion;
		}
		public function getPlaca(){
			return $this->placa;
		}
		public function getTipoVehiculo(){
			return $this->tipoVehiculo;
		}
		public function getTipoCombustible(){
			return $this->tipoCombustible;
		}
		public function getCapacidad(){
			return $this->capacidad;
		}
		public function getDescripcion(){
			return $this->descripcion;
		}
		public function getInstitucion(){
			return $this->institucion;
		}
	}

?>