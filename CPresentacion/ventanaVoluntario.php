<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Voluntario</title>

     <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
     
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />  
    <link rel="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="../CPresentacion/css/estiloVoluntario.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="../CPresentacion/ajax/jquery-3.3.1.min.js"></script>
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/ajaxVoluntario.js" type="text/javascript"></script>
</head>
<body>
    <!-- HEADER -->
    <?php include("includes/generic-header.html"); ?>
    <div id="page" class="container">
        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de Voluntarios</p> 
            <p id = "encargado">Administrado(a) por: Michael Salas</p>
            <!-- Linea divisora -->
            <table id="opcionesMenuDirectorio">
                <thead>
                    <tr>
                        <td>
                            <!-- Boton Registrar nuevo Bodega -->
                            <button id="btn_registrar">Registrar nuevo volunario</button>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>
        <input type = 'text' id = 'buscar' maxlength = '40' size = '40' title = "Nombre, Apellido es lo que busca" onkeyup = "campoVacio()" onkeydown = "detectarTeclaEnter_enBusqueda(event);" placeholder = "Buscar participante"></input>
        <!-- Formulario -->
        <div id="modal">
            <!-- El cuadro visible -->
            <div id="modal-content">
                <!-- Cabecera -->
                <div class="modal-header">
                    <span class="close" onclick = "CancelarModal();">&times;</span>
                    <h2><p id="cedulaDetalle">Registrar Voluntario</p></h2>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-body">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr id = "id_registro">
                            <td>
                                <font color=red><div id="mensaje1" class="mensaje">Error!</div></font>
                                <font color=green ><div id="mensaje2" class="mensaje">Registrado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje3" class="mensaje">Eliminado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje4" class="mensaje">Actualizado Correctamente!</div></font>
                                <font color=red ><div id="mensaje5" class="mensaje">Nombre incorrecto! Llene el campo.</div></font>
                                <label id="id_id"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="nombre">Nombre Completo:</label>
                                <font color=red ><label id="lNombre" class="asterisco"> *</label></font>
                                <input type="text" name="nombre" id="nombre" size="30" maxlength="30" placeholder="Michael Andres Salas Granados" onkeypress="return AceptarLetrasNumeros(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="telefono">Tel&eacute;fono:</label>
                                <input type="text" name="telefono" id="telefono" size="10" maxlength="11" onkeypress="return AceptarNumeros(event)" placeholder="88-77-66-55" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="detalle">Detalle:</label>
                                <textarea id="detalle" rows="5" cols="52" maxlength="300" onkeypress="return AceptarLetrasNumerosComa(event)" placeholder="Escribe aqu&iacute; porqu&eacute; quieres ser voluntario"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footer">
                    <input type="button" value="Registrar" id="btn" class="btn" onclick="BTN();" />
                    <input type="button" value="Cancelar" id="cancelar" class="btn" />
                    <!--<input type = 'BUTTON' id = 'actualizarIntegrante' name = 'actualizarIntegrante' value = 'Actualizar' onclick = "actualizarIntegrante()">
                    <input type = 'BUTTON' id = 'cancelarActualizacion' name = 'cancelarActualizacion' value = 'Cancelar' onclick = 'cancelarActualizacion()'>-->
                </div>
            </div>
        </div>
        <!-- Tabla de consultas-->
        <div id="div_Tabla">
            <table id="tabla">
                <tr>
                    <th id="id_numero-fila">Fila</th>
                    <th class="class_th">Nombre</th>
                    <th class="class_th">Teléfono</th>
                    <th class="class_th">Detalle</th>
                    <th class="class_th">Acción</th>
                </tr>
            </table>
        </div>
        <nav>
            <ul id= "paginacion" class= "paginacion" style='list-style-type: none;'>
                <li class = "page-item" id = 'paginaAnterior'>
                    <a class = "page-link" id = 'primeraPagina' onclick = "cargarPreviaInformacion()">Anterior
                    </a>
                </li>
                <li id = 'pagination' class = "page-item">
                </li>
                <li class = "page-item" id = 'siguientePagina'>
                    <a class = "page-link " onclick = "cargarSiguienteInformacion()">Siguiente
                    </a>
                </li>
            </ul>
        </nav>
        <!-- Modal confirmacion -->
        <div id="modalEliminar">
            <!-- El cuadro visible -->
            <div id="modal-contentEliminar">
                <!-- Cabecera -->
                <div class="modal-headerEliminar">
                    <span class="closeEliminar" onclick = "CancelarModal();">&times;</span>
                    <h2><p id="cedulaDetalle">Eliminar Punto de Riesgo</p></h2>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-bodyEliminar">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr>
                            <td id = "label_confirmacion">¿Desea realmente eliminarlo?</td>                                
                        </tr>
                        <tr>
                            <td><label id = "label_id_voluntario" value = ""></td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footerEliminar">
                    <input type="button" value="Eliminar" id="btn_confirmar" class="btn_confirmacion" onclick="Eliminar();" />
                    <input type="button" value="Cancelar" id="btn_cancelar_confirmacion" class="btn_confirmacion " onclick="Cancelar();"/>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">   
        <p>Pagina en desarrollo para el Comite Municipal de Emergencias de Sarapiqui</p>
        <p>Elaborado en el curso de Ingenieria de Software II UNA</p>
    </div>
    <?php include("includes/generic-footer.html"); ?>      
</body>
</html>