window.onload = function (event) {

    LimpiarCampos();
    Consultar();
    Cargar();
    crearPaginacion();
    verificarPagina();    
}

$(document).ready(function(){
    
    $('#btn_registrar').click(function(){ // Boton de registrar una zona de riesgo.

        document.getElementById("modal").style.top = "0px"; // Se baja el modal.
        LimpiarCampos();
        initMap(); // Inicia el mapa.
    });

    $('#cancelar').click(function(){ // Cancelar el modal de registrar.

        document.getElementById("modal").style.top = "-500vh"; // Se sube el modal.
        LimpiarCampos();
    });

    document.getElementsByClassName("close")[0].onclick = function() {

        document.getElementById('modal').style.top = "-500vh";
    }

    window.onclick = function(event) {

        if (event.target == document.getElementById('modal')) {

            document.getElementById('modal').style.top = "-500vh";
        }
    }

    //CUANDO EL USUARIO DA CLICK EN LA X CIERRA EL MODAL DE ELIMINAR.
    document.getElementsByClassName("closeEliminar")[0].onclick = function() {

            document.getElementById('modalEliminar').style.top = "-500vh";
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL DE ELIMINAR.
    window.onclick = function(event) {

        if (event.target == document.getElementById('modalEliminar')) {
            
            document.getElementById('modalEliminar').style.top = "-500vh";
        }
    }   
});

function LimpiarCampos() {

    OcultarMensajes();

    document.getElementById("btn").value = "Registrar";
    document.getElementById("lugar").value = "";
    document.getElementById("tipo").value = "";
    document.getElementById("label_latitud").innerHTML = "";
    document.getElementById("label_longitud").innerHTML = "";
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() {

    $(".mensaje").hide();
    $(".asterisco").hide();
    $("#divNoHaydatos").hide();
    $("#div_mapa").hide();
}

function BTN() { // Metodo que verifica si es registrar o actualizar.

    OcultarMensajes();

    if (document.getElementById("btn").value == "Registrar") {

        Registrar();
    } else {

        ActualizarBodega();
    }
}

// Solo aceptara caracteres numericos.
function AceptarTexto(e) {

    tecla = (document.all) ? e.keyCode : e.which;
    
    //alert(tecla);
    //Tecla de retroceso para borrar, siempre la permite
    /*
        0 -> Tabulador.
        8 -> Borrar.
        32 -> Espacio.
        44 -> Coma(,);
    */
    if (tecla == 8 || tecla == 0 || tecla == 32 || tecla == 44) {
        
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function Cargar() {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("cargar");

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != "") {

                //alert(resultado);
                var res = resultado.split("+");
                var select = document.getElementById("albergue");
                select.options[0] = new Option("Sin definir");

                for (var i = 0; i < res.length - 1; i++) {

                    select.options[i + 1] = new Option(res[i]);
                }
            } else {

                //alert("Error en cargar datos del responsable!\n" + resultado);
            }
        }
    }
}

function Registrar() { // metodo que registra una nuevo voluntario.

    // Datos minimos requeridos.
    var lugar = document.getElementById("lugar").value;
    var tipo = document.getElementById("tipo").value;

    // Se puede continuar si solo si, el lugar se ingreso y tipo.
    if (lugar != "" && tipo != "") {

        var albergue = document.getElementById("albergue").value;
        var latitud = document.getElementById("label_latitud").innerHTML;
        var longitud = document.getElementById("label_longitud").innerHTML;
        
        // Creando el STRING para enviar a la controladora.
        var enviar ="registrar=true" + 
                    "&lugar=" + lugar + 
                    "&tipo=" + tipo + 
                    "&albergue=" + albergue + 
                    "&latitud=" + latitud + 
                    "&longitud=" + longitud;
        //alert(enviar);
        
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    //alert(resultado);
                    LimpiarCampos();
                    Consultar();
                    $("#mensaje2").show();

                } else { // Error !!!.

                    $("#mensaje3").show();
                    //alert("Error en la registrar un voluntario.\n" + resultado);
                }
            }
        }
    } else {

        if (lugar == "") {
            $("#lLugar").show();
        }
        if (tipo == "") {
            $("#lTipo").show();
        }
        $("#mensaje5").show();
    }
}

function Consultar() { // Metodo que consulta la lista de bodegas regstradas.

    var pagina = calcularPaginaparaRegistros();

    if (pagina == 1) {

        pagina = parseInt(pagina-1);
    }
    else {

        pagina = parseInt(pagina*3);
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("consultar=true" + "&pagina=" + pagina);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            } else {

                $("tr").remove(".cla");
                $("#divNoHaydatos").show();
            }
        }
    }
}

function ConfirmarEliminacion(id) {

    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_puntoriesgo").innerHTML = id;
}

function Eliminar() { // Metodo que elimina un voluntario.

    var temp = document.getElementById("label_id_puntoriesgo").innerHTML;
    OcultarMensajes();

    document.getElementById("modalEliminar").style.top = "-500vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + temp);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.

                    Consultar();
                    $("#mensaje3").show();
                    document.getElementById("label_id_puntoriesgo").innerHTML = "";
                }
            } else {

                $("#mensaje1").show();
                //alert("Error en eliminar voluntario.\n" + resultado);
            }
        }
    }
    if (document.getElementById("btn").value == "Actualizar") {

        document.getElementById("btn").value = "Registrar";
    }
}

function Cancelar() {

    document.getElementById("label_id_puntoriesgo").innerHTML = "";
    document.getElementById('modalEliminar').style.top = "-500vh";
}

function Seleccionar(id) { // Metodo que selecciona un voluntario.

    OcultarMensajes();
    LimpiarCampos();

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                document.getElementById("modal").style.top = "0px";
                var cadena = resultado.split("_");
                document.getElementById("id_id").value = cadena[0];
                document.getElementById("lugar").value = cadena[1];
                document.getElementById("tipo").value = cadena[2];
                document.getElementById("albergue").value = cadena[3];
                document.getElementById("btn").value = "Actualizar";
            } else {

                $("#mensaje1").show();
                //alert("Error en seleccionar.\n" + resultado);
            }
        }
    }
}

function ActualizarBodega() { // Metodo que actualiza una bodega.

    var lugar = document.getElementById("lugar").value;
    var tipo = document.getElementById("tipo").value;

    // Lugar y tipo seran los datos requeridos como minimo.
    if (lugar != "" && tipo != "") {

        var id = document.getElementById("id_id").value;
        var albergue = document.getElementById("albergue").value;

        // Creando un STRING para enviarlo a la controadora.
        var enviar = "actualizar=true" + "&id=" + id + 
                                        "&lugar=" + lugar + 
                                        "&tipo=" + tipo + 
                                        "&albergue=" + albergue;

        //alert(enviar);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    //alert(resultado);
                    LimpiarCampos();
                    Consultar();
                    $("#mensaje4").show();
                } else {

                    $("#mensaje1").show();
                    //alert("Error en actualizar.\n" + resultado);
                }
            }
        }
    } else {

        if (lugar == "") {

            $("#lLugar").show();
        }
        if (tipo == "") {

            $("#lTipo").show();
        }
        $("#mensaje5").show();
    }
}

function Mostrar(id) {

    $("#div_mapa").show();
    $("#div_Tabla").hide();

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                var cadena = resultado.split("_");
                $("tr").remove(".cla");
                $("#tabla_mapa").append("<tr class='cla'>" +
                    "<td></td>" +
                    "<td>" + cadena[1] + "</td>" +
                    "<td>" + cadena[2] + "</td>" +
                    "<td>" + cadena[3] + "</td>" +
                    "<td><button id='btn_regresar' onclick='Regresar();'><i class='fas fa-undo'></i></button></td>" +
                    "</tr>"
                );
                var imgURL = "https://maps.googleapis.com/maps/api/staticmap?center="+cadena[4]+","+cadena[5]+"&size=900x900&markers=color:red%7C"+cadena[4]+","+cadena[5]+"&key=AIzaSyDNZCK5NCo8SO8f14wMWGAn2W2oT6nSGcE";
                document.getElementById('mapa').innerHTML ="<img src='"+imgURL+"'>";
            }
        }
    }
}

function CancelarModal() {

    document.getElementById("modal").style.top = "-500vh";
    LimpiarCampos();
}

function Regresar() {

    $("#div_mapa").hide();
    $("#div_Tabla").show();
    Consultar();
}

// Metodo que obtiene las coordenadas de la posicion actual del dispositivo.
function GenerarCoordenadas() {

    //alert("Generando coordenadas!");
    console.log("Generando coordenadas...!");

    //Obtenemos latitud y longitud
    function localizacion(posicion) {

        var latitude = posicion.coords.latitude;
        var longitude = posicion.coords.longitude;

        console.log(" Latitud: " + latitude + " Longitud: " + longitude);

        document.getElementById("label_latitud").innerHTML = latitude;
        document.getElementById("label_longitud").innerHTML = longitude;
    }
    
    navigator.geolocation.getCurrentPosition(localizacion);
}

/*////////////////////////////////////////////////////*/
var marker;          //variable del marcador
var coords = {};    //coordenadas obtenidas con la geolocalización
//Funcion principal
initMap = function () {

    console.log("Iniciando mapa!");
    //usamos la API para geolocalizar el usuario
    navigator.geolocation.getCurrentPosition(
    function (position) {
        coords =  {
            lng: position.coords.longitude,
            lat: position.coords.latitude
        };
        setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa
    }, function(error){console.log(error);});
    console.log("4");
}

function setMapa (coords) {

    console.log("Enviando mapa!");
    //Se crea una nueva instancia del objeto mapa
    var map = new google.maps.Map(document.getElementById('map'),
    {
        zoom: 13,
        center:new google.maps.LatLng(coords.lat,coords.lng),
    });
    console.log("Si entra aqui!");
    //Creamos el marcador en el mapa con sus propiedades
     //para nuestro obetivo tenemos que poner el atributo draggable en true
    //position pondremos las mismas coordenas que obtuvimos en la geolocalización
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat,coords.lng),
    });
    //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
    //cuando el usuario a soltado el marcador
    marker.addListener('click', toggleBounce);
    marker.addListener( 'dragend', function (event)
    {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
        //document.getElementById("coords").value = this.getPosition().lat()+","+ this.getPosition().lng();
        document.getElementById("label_latitud").innerHTML = this.getPosition().lat();
        document.getElementById("label_longitud").innerHTML = this.getPosition().lng();

        //console.log("Latitud: ", coords.lat);
        //console.log("Longitud: ", coords.lng);
        console.log("Latitud: ", this.getPosition().lat());
        console.log("Longitud: ", this.getPosition().lng());
    });
}
//callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
function toggleBounce() {

    if (marker.getAnimation() !== null) {

        marker.setAnimation(null);
    } else {

        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}

function detectarTeclaEnter_enBusqueda(e) {

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        buscarPuntoRiesgo();
    }
}

function buscarPuntoRiesgo() {

    var buscar = document.getElementById('buscar').value;
    var tablaPuntoRiesgo = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;

    if (buscar != "") {

        while (i < tablaPuntoRiesgo.rows.length) {

            tr = tablaPuntoRiesgo.rows[i];
            for (var j = 1; j < tr.cells.length-1; j++) {

                td = tr.cells[j].innerHTML;

                if (td.toUpperCase().includes(buscar.toUpperCase())) {

                    temp = tr.cells[0].innerHTML + ",";
                    if (!puerta){

                        puerta = true;
                        dato = "";
                    }
                    if (!dato.includes(temp)) {

                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if (dato != "Sin resultado") {

            for (var i = 1 ; i < tablaPuntoRiesgo.rows.length; i++) {

                if (dato.includes(tablaPuntoRiesgo.rows[i].cells[0].innerHTML)){

                    tablaPuntoRiesgo.rows[i].style.display = "";
                }
                else {
                    tablaPuntoRiesgo.rows[i].style.display = "none";
                }
            }
        }
        else {
            if (dato == "Sin resultado") {

                buscar_en_baseD(buscar);
            }
            else {
                resultadoBusqueda(dato);
            }
        }
    }
    if (buscar == "") {

        resultadoBusqueda(buscar);
    }
}

function buscar_en_baseD(variable) {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarPuntoRiesgoEspecifico=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else{
                resultadoBusqueda("Sin resultado");
            }
        }
    }
}

function campoVacio() {

    if (document.getElementById('buscar').value == "") {

        resultadoBusqueda("");
    }   
}

function resultadoBusqueda(texto) {

    if (texto == "Sin resultado") {

        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if (document.getElementById('buscar').value == "") {

        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}

function verificarPagina(paginaLimite) {

    if (window.location.search == "") {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
        location.href = "../CPresentacion/ventanaZonaRiesgo.php?pagina=1";
    }
    if (window.location.search.includes(paginaLimite)) {

        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if (window.location.search.includes(1)) {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
}

function crearPaginacion() {

    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraZonaRiesgo.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4 && xhr.status == 200) {

            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;

            for (var i = 0; i < resultado; i++) {

                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('href',"../CPresentacion/ventanaZonaRiesgo.php?pagina="+(i+1));
                liNode.appendChild(href);
                listNode.appendChild(liNode);
            }
            verificarPagina(resultado);
        }
    }
}

function calcularPaginaparaRegistros() {

    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;

    if (url.length == 12) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                if (url[i] == "%") {

                }
                else if (i >= 11) {

                    numUrl += url[i];           
                }
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    else if (url.length == 9) {

        for (var i = 0; i < url.length; i++) {
            if (puertaRegistro) {

                numUrl += url[i];           
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    return  numUrl;
}