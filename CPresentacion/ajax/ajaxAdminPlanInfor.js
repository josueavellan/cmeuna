window.onload = function(event){

	document.getElementById('tituloCabecera').innerHTML += "Administraci&oacute;n de Planificaci&oacute;n e Informaci&oacute;n";
	mostrarTituloAdministrador();
}


$(document).ready(function(){
    var modal = document.getElementById('myModal');

    //LLAMA EL OBJETO QUE PERMITE CERRAR EL MODAL
    var span = document.getElementsByClassName("close")[0];

    //CUANDO EL USUARIO DA CLICK EN LA X DEL MODAL HARA QUE SE CIERRE
    span .onclick = function() {
        cancelarDato();
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL HARA QUE SE CIERRE
    window.onclick = function(event) {
        if (event.target == modal) {
            cancelarDato();
        }
    }
});

function mostrarTituloAdministrador(){

	document.getElementById('encargado').innerHTML += "Dennis Muñoz";
}

var guardarArea = 0;
function desplegarInformacion(valor){
	document.getElementById('buscar').value = "";
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");

    if(valor == '1')
    	informacionAlertas();
	   	
    if(valor == '2')    		
    	informacionAnuncios();
	   	
    if(valor == '3')
    	informacionDirectorio();
        	
    if(valor == '4')
    	informacionVoluntarios();
    
    
}

//METODOS DE CARGA DE INFORMACION
function informacionAlertas(){
	document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Alertas";
	document.getElementById('opcionesMenu').style.display = "block";
}
function informacionAnuncios(){
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Anuncios";
    document.getElementById('opcionesMenu').style.display = "block";
}

function informacionDirectorio(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 3;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Directorio";
    document.getElementById('buscar').placeholder = "Buscar integrante";
    document.getElementById('buscar').title = "Nombre, Apellido, Instituci&oacute;n es lo que busca";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "block";
    document.getElementById('btn_registrar').value = "Registrar Integrante";
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    //LLAMADA DE METODOS
    crearPaginacion("../CNegocio/controladoraDirectorio.php");
    mostrarIntegrantes(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Nombre</TH>"+
                    "<TH>Primer Apellido:</TH>"+

                    "<TH>Segundo Apellido:</TH>"+
                    "<TH>Tel&eacute;fono:</TH>"+
                    "<TH>Correo electr&oacute;nico:</TH>"+
                    "<TH>Instituci&oacute;n Representada:</TH>"+
                    "<TH>Comit&eacute; Representado:</TH>"+
                    "<TH>Opciones</TH>"+
                "</TR>" +
        "</THEAD><TR>"
        );
    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);

}

function informacionVoluntarios(){
    //VARIABLE QUE PERMITE LE INGRESO Y LA CARGA DE INFORMACION DE ACUERDO AL AREA
    guardarArea = 4;
    //MODIFICACION DE CAMPOS GENERALES Y APARICION
    document.getElementById('tituloCabecera').innerHTML = "Secci&oacute;n de Voluntarios";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('buscar').title = "Nombre, Apellido es lo que busca";
    document.getElementById('buscar').style.display = "block";
    document.getElementById('opcionesMenu').style.display = "block";
    document.getElementById('btn_registrar').style.display = "block";
    document.getElementById('btn_registrar').value = "Registrar Voluntario";
    document.getElementById('tabla').style.display = "block";
    document.getElementById('paginacion').style.display = "block";
    crearPaginacion("../CNegocio/controladoraVoluntario.php");
    //LLAMADA DE METODOS
    mostrarIntegrantes(calcularPaginaparaRegistros());
    //ELIMINA EL CONTENIDO DE LA TABLA PARA HACER LA CARGA RESPECTIVA
    $("#tabla THEAD").remove();
    $("#tablaDirectorio tr").remove();
    $("#tabla").append(""+
        "<THEAD>"+
                "<TR>" +
                    "<TH>Fila</TH>"+
                    "<TH>Nombre:</TH>"+
                    "<TH>Tel&eacute;fono:</TH>"+
                    "<TH>Detalle:</TH>"+
                    "<TH>Opciones</TH>"+
                "</TR>" +
        "</THEAD><TR>"
    );
    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);
}

//METODOS DEL AREA DE VOLUNTARIOS

function Mas(i){
    if (document.getElementById(i).style.display == "none" || document.getElementById(i).style.display == "") {

        document.getElementById(i).style.display = "block";
        document.getElementById("btn" + i).value = "Menos...";
    } else {
        document.getElementById(i).style.display = "none";
        document.getElementById("btn" + i).value = "Mas...";
    }
    
    //alert(i);
}

function registrarVoluntario() { // metodo que registra una nuevo voluntario.

    // Obteniendo los valores de los inputs.
    var nombre = document.getElementById("nombre").value;

    // Se puede continuar si solo si, el nombre se ingreso.
    if (nombre != "") {

        // Creando el STRING para enviar a la controladora.
        var enviar = "registrar=true" + "&nombre=" + nombre + "&telefono=" + document.getElementById("telefono").value + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {


                    document.getElementById('myModal').style.top = "-100vh";
                    setTimeout("verVentanaConfirmacion('registro')",500);

                } else { // Error !!!.

                    document.getElementById('myModal').style.top = "-100vh";
                    setTimeout("verVentanaConfirmacion('registroError')",500);
                }
            }
        }
    } 
}

function eliminarVoluntario() { // Metodo que elimina un voluntario.
    document.getElementById("myModal").style.top = "-120vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + document.getElementById("id_id").value);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.

                    setTimeout("verVentanaConfirmacion('eliminar')",300);
                    document.getElementById("id_id").value = "";
                }
            } 
        }
    }
}

function actualizarVoluntario(){

    document.getElementById('myModal').style.top = "-120vh";
    var nombre = document.getElementById("nombre").value;
    var telefono = document.getElementById('telefono').value.split("-")[0] + "" + document.getElementById('telefono').value.split("-")[1];
    if (nombre != "") {
        var enviar = "actualizar=true" + "&id=" + document.getElementById("id_id").value + "&nombre=" + nombre + "&telefono=" + telefono + "&detalle=" + document.getElementById("detalle").value + " ";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    setTimeout("verVentanaConfirmacion('actualizar')",300);
                    document.getElementById("id_id").value = "";
                } else {

                    setTimeout("verVentanaConfirmacion('actualizarError')",300);
                    document.getElementById("id_id").value = "";
                }
            }
        }
    } 
}

function AceptarLetrasNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    if (tecla == 8 || tecla == 0 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function AceptarLetrasNumerosComa(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    // tecla = 44 -> es la d ela ',' "coma".
    if (tecla == 8 || tecla == 0 || tecla == 44 || tecla == 32) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

//FIN DE METODOS DEL AREA DE VOLUNTARIOS
//
//
//



/*METODOS DEL DIRECTORIO */

//PERMITE REGISTRAR NUEVOS INTEGRANTES AL DIRECTORIO
function registrarIntegrante(){
    if(validarCampos("comprobarCampo","") >= 7){
        var telefono = "";
        if(!puertaTelefono && document.getElementById('segundoTelefono').value != "" && document.getElementById('telefono').value.length == 9 && document.getElementById('segundoTelefono').value.length == 9){
            telefono = document.getElementById("telefono").value.split("-");
            telefono = telefono[0]+""+telefono[1]+"/";
            var temp = document.getElementById('segundoTelefono').value.split("-");         
            telefono = telefono + temp[0]+""+temp[1];
            var puertaRegistro = true;
        }
        else{
            telefono = document.getElementById("telefono").value.split("-");
            telefono = telefono[0]+""+telefono[1];
            if(document.getElementById('telefono').value.length == 9){
                var puertaRegistro = true;
            }
        }
        if(puertaRegistro){
            var cedula = document.getElementById("cedula").value;
            var nombre = document.getElementById("nombre").value;
            var primer_apellido = document.getElementById("primer_apellido").value;
            var segundo_apellido = document.getElementById("segundo_apellido").value;
            var institucionRepresentada = document.getElementById('institucionRepresentada').value;
            var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
            var seleccionarComite = document.getElementById('seleccionarComite').value;
            var correo = document.getElementById('correo').value;

            //EN CASO QUE NO SE INGRESE CEDULA, EL REGISTRO DE CEDULA SERA 0
            if(cedula == "")
                cedula = "0";
            //

            var enviar ="registrar=true" +
                       "&cedula=" + cedula +
                       "&nombre=" + nombre +
                       "&primer_apellido=" + primer_apellido +
                       "&segundo_apellido=" + segundo_apellido +
                       "&telefono=" + telefono +
                       "&institucionRepresentada=" + institucionRepresentada + 
                       "&seleccionarPuesto=" + seleccionarPuesto +
                       "&seleccionarComite=" + seleccionarComite +
                       "&correo=" + correo; //envia los datos para registrar el integrante


            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send(enviar);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;

                    if (resultado == 1) {

                        document.getElementById('myModal').style.top = "-100vh";
                        setTimeout("verVentanaConfirmacion('registro')",500);

                    } 

                    else{
                        document.getElementById('myModal').style.top = "-100vh";
                        setTimeout("verVentanaConfirmacion('registroError')",500);
                       //alert("Error en la consulta" + resultado);

                    } // Fin else.
                } // Fin if.
            }// Fin onreadystatechange
        }
    }
}

//TOMA EL ID DE ACUERDO AL INTEGRANTE SELECCIONADO Y MODIFICA LOS CAMBIOS
function actualizarIntegrante(){
    var telefono = "";
    if(document.getElementById('segundoTelefono').value != "" && document.getElementById('segundoTelefono').value.length == 9){
        telefono = document.getElementById("telefono").value;
        if(telefono.includes("-")){
            telefono = telefono.split("-")[0]+""+telefono.split("-")[1];
        }
        var temp = document.getElementById('segundoTelefono').value;
        if(temp.includes("-")){
            temp = temp.split("-")[0]+""+temp.split("-")[1];
        }
        telefono = telefono+"/"+temp;
        var puertaActualizar = true;            
    }
    else{
        telefono = document.getElementById("telefono").value;
        if(document.getElementById('telefono').value.length == 9){
            telefono = telefono.split("-");
            if(document.getElementById('segundoTelefono').value != ""){
                var temp = document.getElementById('segundoTelefono').value;
                temp = temp.split("-")[0]+""+temp.split("-")[1];
                telefono = telefono[0]+""+"/"+temp.split("u")[0];
            }
            else
                telefono = telefono[0]+""+telefono[1];
            var puertaActualizar = true;
        }
    }
    if(puertaActualizar){
        var id = document.getElementById('id').value
        var cedula = document.getElementById("cedula").value;
        var nombre = document.getElementById("nombre").value;
        var primer_apellido = document.getElementById("primer_apellido").value;
        var segundo_apellido = document.getElementById("segundo_apellido").value;
        var institucionRepresentada = document.getElementById('institucionRepresentada').value;
        var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
        var seleccionarComite = document.getElementById('seleccionarComite').value;
        var correo = document.getElementById('correo').value;
        if(cedula == "")
            cedula = 0;
        var envio ="actualizar=true" + 
                    "&idActualizar=" +id +
                    "&cedulaActualizar=" + cedula +
                    "&nombreActualizar=" + nombre +
                    "&primer_apellidoActualizar=" + primer_apellido +
                    "&segundo_apellidoActualizar=" + segundo_apellido +
                    "&telefonoActualizar=" + telefono +
                    "&institucionRepresentadaActualizar=" + institucionRepresentada + 
                    "&seleccionarPuestoActualizar=" + seleccionarPuesto +
                    "&seleccionarComiteActualizar=" + seleccionarComite +
                    "&correoActualizar=" + correo; //se envian los datos para actualizar el integrante
        
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");            
        xhr.send(envio);
        xhr.onreadystatechange = function(){

            if(xhr.readyState == 4 && xhr.status == 200){

                var resultado = xhr.responseText;
                if(resultado == 1){
                    document.getElementById('myModal').style.top = "-100vh";
                    setTimeout("verVentanaConfirmacion('actualizar')",300);
                }
                else if(resultado == 2){
                    setTimeout("verVentanaConfirmacion('actualizarError')",300);
                    //alert("Error en actualizar");
                }
                else if(resultado == 0){
                    setTimeout("verVentanaConfirmacion('actualizarEspacios')",300);
                    //alert("Error de espacios");
                }
            }
        }   
    }   
}

//MISMO CASO DEL ANTERIOR PERO LO ELIMINA
function eliminarIntegrante(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("eliminar=" + document.getElementById('id').value);//Envia el id del integrante seleccionar para enviar a eliminar
    
    xhr.onreadystatechange = function(){

        if(xhr.readyState == 4 && xhr.status == 200){

            var resultado = xhr.responseText;
            if(resultado != 0){
                document.getElementById('myModal').style.top = "-100vh";
                setTimeout("verVentanaConfirmacion('eliminar')",300);
            }
            else{
                alert("Error al eliminar");
            }
        }
    }
}

//AL CARGAR UN MODAL DE ACTUALIZAR O ELIMINAR CARGA LOS COMITES EXISTENTES REGISTRADOS
function llenarOpcionesComite(valor){
	
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarComite=true");

	xhr.onreadystatechange = function(){

		if(xhr.readyState == 4 && xhr.status == 200){

			var resultado = xhr.responseText;
			if(resultado != ""){
				var datos = resultado.split(',');
				$("#seleccionarComite").empty()
				$("#seleccionarComite").append(new Option("Seleccione una opción","",true,true));
				document.getElementById('seleccionarComite').options[0].disabled = true;
				for(var i = 0; i <= datos.length-1; i++){
					if(datos[i] != ""){
                        if(datos[i] == valor)
                            $("#seleccionarComite").append(new Option(datos[i],datos[i],true,true));
                        else
                            $("#seleccionarComite").append(new Option(datos[i],datos[i],true,false));
						$("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
						$("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
						$("#seleccionarComite").append(new Option("Otro","Otro",true,false));
                        if(valor == "Ninguno")
						  $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,true));
                        else
                            $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false)); 
					}
				}			
			}	
		}
	}
}

//EN CASO DE USAR LA FUNCION DE OTRO AGREGAR EL NUEVO COMITE PARA SU REGISTRO
function agregarNuevoComite(){

	if(document.getElementById('seleccionarComite').value == "Otro"){
		var nuevoComite = prompt("Ingrese el nombre del comite","");
        if(nuevoComite != null){
            $("#seleccionarComite").append(new Option(nuevoComite,nuevoComite,true,true));
            $("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
            $("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
            $("#seleccionarComite").append(new Option("Otro","Otro",true,false));
            $("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false));
        }
        if(nuevoComite == null){
            document.getElementById('seleccionarComite').selectedIndex = 0;
        }
	}

}

//insertar un campo de texto mas para el telefono
var puertaTelefono = true;
function insertarTelefonoExtra(opcion){
	if(opcion == "registrar"){
		document.getElementById('telefonoExtraRegistrar').style.display = "block";
		puertaTelefono = false;
	}
	else{
		document.getElementById('telefonoExtra').style.display = "block";
		puertaTelefono = false;
	}

}
//validacion de números
function validacionNumerico(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

//CUANDO SE CARGA LA INFORMACIÓN SI EL SUJETO SELECCIONADO CUENTA CON DOS NUMEROS
//ESTAN SEPARADOS POR / Y HABILITA EL OTRO CAMPO DE TELEFONO
function habilitarbotonSegundoTelefono(valor){
    if(valor.includes("/")){
        document.getElementById('botonTelefonoExtra').disabled = true;
        document.getElementById('telefonoExtra').style.display = "block";
        document.getElementById('telefono').value = valor.split("/")[0];
        document.getElementById('segundoTelefono').value = valor.split("/")[1];
    }   

}


function validarCedula(opcion){
    var contador = 0;
    var filtro = "123456789";
    var respuesta = false;
    if(opcion == "comprobarCampo" && document.getElementById("cedula").value != ""){
        var cedula = document.getElementById("cedula").value;
        for(var i = 0; i < cedula.length; i++){
            if(!filtro.includes(cedula[i])){
                contador++;
            }
        }

        if(opcion == "comprobarCampo"){
            if(contador >= 3 || cedula.length < 9){
                document.getElementById('cedula').style.borderColor = "red";
                $("#lCedula").show();
            }
            else if(contador < 3 || document.getElementById('cedula').value == ""){
                document.getElementById('cedula').style.borderColor = "";
                $("#lCedula").hide();
                respuesta = true;   
            }
        }   
    }

}

function validarCorreo(opcion){

    var respuesta = false;
    if(opcion == "comprobarCampo"){

        if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(document.getElementById('correo').value)){
            document.getElementById('correo').style.borderColor = "";
            $("#lCorreo").hide();
            respuesta = true;
        } 
        else{
            document.getElementById('correo').style.borderColor = "red";
            $("#lCorreo").show();   
        }
        
    }

    return respuesta;
}

var errores = [0,0,0,0];
function validarCampos(opcion, campoTexto){
    var estado = 0;
    if(opcion == "comprobarCampo"){
        var nombre = document.getElementById("nombre").value;
        var primer_apellido = document.getElementById("primer_apellido").value;
        var segundo_apellido = document.getElementById("segundo_apellido").value;
        var telefono = document.getElementById("telefono").value;
        var segundoTelefono = document.getElementById('segundoTelefono').value;
        var institucionRepresentada = document.getElementById('institucionRepresentada').value;
        var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
        var seleccionarComite = document.getElementById('seleccionarComite').value;
        if(nombre == "" && campoTexto == "nombre" || errores[0] == 1){
            errores[0] = 1;
            document.getElementById('nombre').style.borderColor = "red";
            $("#lNombre").show();
        }
        if(nombre != ""){
            document.getElementById('nombre').style.borderColor = "";   
            $("#lNombre").hide();   
            errores[0] = 0;
            estado++;
        }       
        if(primer_apellido == "" && campoTexto == "apellido1" || errores[1] == 1){
            document.getElementById('primer_apellido').style.borderColor = "red";
            $("#lApellido1").show();
            errores[1] = 1;
        }
        if(primer_apellido != ""){
            document.getElementById('primer_apellido').style.borderColor = "";  
            $("#lApellido1").hide();
            estado++;   
            errores[1] = 0;
        }       
        if(segundo_apellido == "" && campoTexto == "apellido2" || errores[2] == 1){
            document.getElementById('segundo_apellido').style.borderColor = "red";
            errores[2] = 1;
            $("#lApellido2").show();            
        }
        if(segundo_apellido != ""){
            errores[2] = 0;
            document.getElementById('segundo_apellido').style.borderColor = ""; 
            $("#lApellido2").hide();
            estado++;               
        }       
        if(campoTexto == "telefono" && document.getElementById('telefono').value.length < 8){
            document.getElementById('telefono').style.borderColor = "red";
            $("#lTelefono").show();             
        }
        else{
            document.getElementById('telefono').style.borderColor = "";
            $("#lTelefono").hide();
            estado++;   
        }       
        if(campoTexto == "segundoTelefono" && document.getElementById('segundoTelefono').value.length < 8){
            document.getElementById('segundoTelefono').style.borderColor = "red";
            $("#lSegundoTelefono").show();              
        }
        else{
            document.getElementById('segundoTelefono').style.borderColor = "";
            $("#lSegundoTelefono").hide();
        }
        if(institucionRepresentada == "" && campoTexto == "institucion" || errores[3] == 1){
            document.getElementById('institucionRepresentada').style.borderColor = "red";
            errores[3] = 1;
            $("#lInstitucion").show();  
        }
        if(institucionRepresentada != ""){
            errores[3] = 0;
            document.getElementById('institucionRepresentada').style.borderColor = "";  
            $("#lInstitucion").hide();
            estado++;   
        }       
        if(seleccionarPuesto == "" && campoTexto == "puesto"){
            document.getElementById('seleccionarPuesto').style.borderColor = "red";
            $("#lPuesto").show();   
        }
        else{
            document.getElementById('seleccionarPuesto').style.borderColor = "";    
            $("#lPuesto").hide();   
            estado++;
        }       
        if(seleccionarComite == "" && campoTexto == "comite" || errores[4] == 1){
            errores[4] = 1;
            document.getElementById('seleccionarComite').style.borderColor = "red";
            $("#lComite").show();   
        }
        if(seleccionarComite != ""){
            errores[4] = 0;
            document.getElementById('seleccionarComite').style.borderColor = "";    
            $("#lComite").hide();
            estado++;   
        }       
        if(campoTexto == "correo"){
            if(validarCorreo("comprobarCampo") == true){
                estado++;
            }
        }
        return estado;
    }
}

//insertar un campo de texto mas para el telefono
var puertaTelefono = true;
function insertarTelefonoExtra(opcion){
    document.getElementById('telefonoExtra').style.display = "block";
    puertaTelefono = false;

}

function verVentanaConfirmacion(opcion){
    if(opcion == "registro"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').innerHTML = "¡Registro Exitoso!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            if(guardarArea == 3){
                crearPaginacion("../CNegocio/controladoraDirectorio.php");
                verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
            }
            if(guardarArea == 4){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }
            mostrarIntegrantes(calcularPaginaparaRegistros());
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                if(guardarArea == 3){
                    crearPaginacion("../CNegocio/controladoraDirectorio.php");
                    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
                }
                if(guardarArea == 4){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                mostrarIntegrantes(calcularPaginaparaRegistros());
                cancelarDato();
            }
        }
    }
    else if(opcion == "registroError"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡Problema de Registro!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                cancelarDato();
            }
        }
    }
    else if(opcion == "actualizar"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').innerHTML = "¡Actualización Exitosa!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            if(guardarArea == 3){
                crearPaginacion("../CNegocio/controladoraDirectorio.php");
                verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
            }
            if(guardarArea == 4){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }         
            mostrarIntegrantes(calcularPaginaparaRegistros());
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh";          
                if(guardarArea == 3){
                    crearPaginacion("../CNegocio/controladoraDirectorio.php");
                    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
                }
                if(guardarArea == 4){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                mostrarIntegrantes(calcularPaginaparaRegistros());
                cancelarDato();      
            }
        }
    }
    else if(opcion == "actualizarError"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡Problema de Actualización!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                cancelarDato();
            }
        }
    }
    else if(opcion == "actualizarEspacios"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').style.backgroundColor = "#f44336";
        document.getElementById('botonConfirmacion').style.background = "#f44336";
        document.getElementById('confirmacion').innerHTML = "¡NO deje espacios en blanco!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.top = "-120vh"; 
            cancelarDato();
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                cancelarDato();
            }
        }       
    }
    else if(opcion == "eliminar"){
        document.getElementById('modalConfirmacion').style.top= "0px";
        document.getElementById('confirmacion').innerHTML = "¡Registro Eliminado!";
        var modal = document.getElementById('modalConfirmacion');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("closeConfirmacion")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            if(guardarArea == 3){
                crearPaginacion("../CNegocio/controladoraDirectorio.php");
                verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
            }
            if(guardarArea == 4){
                crearPaginacion("../CNegocio/controladoraVoluntario.php");
                verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
            }
            mostrarIntegrantes(calcularPaginaparaRegistros());
            history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");
            cancelarDato();            
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.top = "-120vh"; 
                mostrarIntegrantes(calcularPaginaparaRegistros());
                if(guardarArea == 3){
                    crearPaginacion("../CNegocio/controladoraDirectorio.php");
                    verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);
                }
                if(guardarArea == 4){
                    crearPaginacion("../CNegocio/controladoraVoluntario.php");
                    verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);   
                }
                cancelarDato();
            }
        }       
    }
}

function botonConfirmacion(){

    document.getElementById('modalConfirmacion').style.top = "-100vh";
    if(guardarArea == 3){
        crearPaginacion("../CNegocio/controladoraDirectorio.php");
        mostrarIntegrantes(calcularPaginaparaRegistros());
        verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",false);

    }
    if(guardarArea == 4){
        crearPaginacion("../CNegocio/controladoraVoluntario.php");
        mostrarIntegrantes(calcularPaginaparaRegistros());
        verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",false);
    }
    cancelarDato();
}

/*FIN DE LOS METODOS DEL DIRECTORIO*/
//
//
//

/*METODOS FIJOS */

//CARGA LA INFORMACION GENERAL DE LA TABLA
function mostrarIntegrantes(valor){

    var pagina = 0;
    if(valor == 0)
        pagina = calcularPaginaparaRegistros();
    if(valor > 0)
        pagina = valor;

    if(pagina == 1)
        pagina = parseInt(pagina)-1;
    else
        pagina = parseInt(pagina)*3;

    switch(guardarArea){

        case guardarArea = 3:
            var xhr=new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send("buscarIntegrantes=true" + "&pagina=" + pagina);
            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                    if (resultado != "null") {
                        $("tr").remove(".cla"); 
                        //Se aplica un split, para separar cada integrante.
                        var cadena = resultado.split("+");
                        for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.

                            var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
                            if(datos[2] != "Default"){
                                $("#tablaDirectorio").append(""+
                                    "<tr class = 'cla'>"+
                                        "<td hidden>" +  datos[1]  + " </td>" +
                                        "<td>" + datos[2] + "</td>" +
                                        "<td>" + datos[3] + "</td>" +
                                        "<td>" + datos[4] + "</td>" +
                                        "<td>" + datos[5] + "</td>" +
                                        "<td>" + datos[9] + "</td>" +
                                        "<td>" + datos[6] + "</td>" +
                                        "<td>" + datos[8] + "</td>" +
                                        "<td>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'Seleccionar("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+ 
                                        "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
                                        "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED HIDDEN/>" + "</td>" +
                                    "</tr>"
                                );
                            }
                        } 
                    } 
                }
            }
        break;

        case guardarArea = 4:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("consultar=true" + "&pagina=" + pagina);

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                        
                    if (resultado != 0) {
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    }
                }
            }
        break;

    }
        
}

//CARGA LOS 3 ESTADOS DEL MODAL: REGISTRAR, ACTUALIZAR Y ELIMINAR
//PUERTA 1 ES PARA ACTUALIZAR, PUERTA 2 ES PARA REGISTRAR, PUERTA 3 ES PARA ELIMINAR
function cargaModalGlobal(puerta,variableId){

    switch(guardarArea){

        case guardarArea = 3:
            if(puerta == 1){

                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            if(resultado.split(",")[4] == "")
                                document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3];    
                            else
                                document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3] + " " + resultado.split(",")[4];    
                            var cadena = resultado.split(",");
                            $("#tablaxinformacionDetallada").append("" +
                                "<TBODY>" +
                                    "<TR hidden>" +
                                        "<TD>" + "<INPUT TYPE='text' NAME='id' ID='id' VALUE = '"+cadena[0]+"'/>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>C&eacute;dula:" +
                                            "<INPUT TYPE='text' NAME='cedula' ID='cedula' SIZE = '18' MAXLENGTH = '15' ONBLUR = validarCedula('comprobarCampo') PLACEHOLDER = 'Campo no obligatorio' VALUE = '"+cadena[1]+"' />" +
                                                "<FONT color=red /><LABEL id='lCedula' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Nombre:" +
                                            "<INPUT TYPE='text' NAME='nombre' ID='nombre' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','nombre') PLACEHOLDER = 'Escriba su nombre'VALUE = '"+cadena[2]+"' />" +
                                                "<FONT color=red /><LABEL id='lNombre' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" + 
                                    "<TR>" +
                                        "<TD>Apellidos:"+
                                            "<INPUT TYPE='text' NAME='primer_apellido' ID='primer_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido1') PLACEHOLDER = 'Escriba su primer apellido' VALUE = '"+cadena[3]+"'/>" +
                                                "<FONT color=red /><LABEL id='lApellido1' class='asterisco'> *</LABEL></TH>" +
                                            "<INPUT TYPE='text' NAME='segundo_apellido' ID='segundo_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido2') PLACEHOLDER = 'Escriba su segundo apellido' VALUE = '"+cadena[4]+"'/>" +
                                                "<FONT color=red /><LABEL id='lApellido2' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Tel&eacute;fono:" +
                                            "<INPUT TYPE='text' NAME='telefono' ID='telefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = return validacionNumerico(event) ONBLUR = validarCampos('comprobarCampo','telefono') PLACEHOLDER = 'Escriba su teléfono' VALUE = '"+cadena[5]+"'/>" +
                                                "<FONT color=red /><LABEL id='lTelefono' class='asterisco'> *</LABEL></TH>" +
                                            "<INPUT TYPE ='button' NAME='botonTelefonoExtra' ID='botonTelefonoExtra' VALUE = '+' ONCLICK = insertarTelefonoExtra('registrar')></TD>" +
                                    "</TR>" +
                                        "<TR ID = 'telefonoExtra' style = 'display: none;' ALIGN = 'center'>" +
                                        "<TD>Tel&eacute;fono Extra:" +
                                            "<INPUT TYPE='text' NAME='segundoTelefono' ID='segundoTelefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = return validacionNumerico(event) ONBLUR = validarCampos('comprobarCampo','segundoTelefono') />" +
                                                "<FONT color=red /><LABEL id='lSegundoTelefono' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Correo electr&oacute;nico:"+
                                            "<INPUT TYPE='text' NAME='correo' ID='correo' SIZE = '25' MAXLENGTH = '150' ONBLUR = validarCampos('comprobarCampo','correo') PLACEHOLDER = 'Escriba su correo electrónico' VALUE = '"+cadena[9]+"' />" +
                                                "<FONT color=red /><LABEL id='lCorreo' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Instituci&oacute;n:<INPUT TYPE='text' NAME='institucionRepresentada' ID='institucionRepresentada' MAXLENGTH = '40' SIZE = '40' ONBLUR = validarCampos('comprobarCampo','institucion') PLACEHOLDER = 'Escriba su institución o empresa' VALUE = '"+cadena[6]+"'/>" +
                                            "<FONT color=red /><LABEL id='lInstitucion' class='asterisco'> *</LABEL></TH></TD>" +
                                    "</TR>" );
                            if(cadena[7] == "Propietario"){
                                $("#tablaxinformacionDetallada").append("" +
                                        "<TR>" +
                                            "<TD>Puesto:" +
                                                "<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto')>" +
                                                    "<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
                                                    "<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario' SELECTED>Propietario</OPTION>" +
                                                    "<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente'>Asistente</OPTION>" +
                                                "</SELECT>" +
                                                "<FONT color=red /><LABEL id='lPuesto' class='asterisco'> *</LABEL></TH></TD>" +
                                            "</TD>" +
                                        "</TR>"
                                );
                            }
                            else if(cadena[7] == "Asistente"){
                                $("#tablaxinformacionDetallada").append("" +
                                    "<TR>" +
                                        "<TD>Puesto:" +
                                            "<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto')>" +
                                                "<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
                                                "<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario'>Propietario</OPTION>" +
                                                "<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente' SELECTED>Asistente</OPTION>" +
                                            "</SELECT>" +
                                            "<FONT color=red /><LABEL id='lPuesto' class='asterisco'> *</LABEL></TH></TD>" +
                                        "</TD>" +
                                    "</TR>"
                                );
                            }                
                            $("#tablaxinformacionDetallada").append("" +
                                    "<TR>" +
                                        "<TD>Comit&eacute;:"+
                                            "<SELECT ID = 'seleccionarComite' NAME = 'seleccionarComite' ONCLICK = agregarNuevoComite() ONCHANGE = agregarNuevoComite() ONBLUR = validarCampos('comprobarCampo','comite')>" +
                                            "</SELECT>" +
                                            "<FONT color=red /><LABEL id='lComite' class='asterisco'> *</LABEL></TH>" +
                                        "</TD>" +
                                    "</TR>"+
                            "</TBODY>"
                            );

                        }
                        //LLAMA EL METODO QUE DESHABILITA EL BOTON DE AGREGAR UN SEGUNDO TELEFONO Y SEPARA LOS TELEFONOS EN LOS CAMPOS
                        //RESPECTIVOS, EVITANDO COLOCAR LOS DOS TELEFONOS CON EL / EN UN MISMO CAMPO
                        habilitarbotonSegundoTelefono(cadena[5]);
                        llenarOpcionesComite(cadena[8]);
                        $("#telefono").mask("9999-9999");   
                        $("#segundoTelefono").mask("9999-9999");    
                        $(".asterisco").hide();
                    }
                }

            }

            if(puerta == 2){
                $("#tablaxinformacionDetallada").append("" +
                    "<TBODY>" +
                        "<TR>" +
                            "<TD>C&eacute;dula:" +
                                "<INPUT TYPE='text' NAME='cedula' ID='cedula' SIZE = '18' MAXLENGTH = '15' ONBLUR = validarCedula('comprobarCampo') PLACEHOLDER = 'Campo no obligatorio' />" +
                                "<FONT color=red /><LABEL id='lCedula' class='asterisco'> *</LABEL></TH>"+
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Nombre:" +
                                "<INPUT TYPE='text' NAME='nombre' ID='nombre' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','nombre') PLACEHOLDER = 'Escriba su nombre' />" +
                                "<FONT color=red /><LABEL id='lNombre' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" + 
                        "<TR>" +
                            "<TD>Apellidos:"+
                                "<INPUT TYPE='text' NAME='primer_apellido' ID='primer_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido1') PLACEHOLDER = 'Escriba su primer apellido'/>" +
                                "<FONT color=red /><LABEL id='lApellido1' class='asterisco'> *</LABEL></TH>" +
                                "<INPUT TYPE='text' NAME='segundo_apellido' ID='segundo_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido2') PLACEHOLDER = 'Escriba su segundo apellido'/>" +
                                "<FONT color=red /><LABEL id='lApellido2' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Tel&eacute;fono:" +
                                "<INPUT TYPE='text' NAME='telefono' ID='telefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = return validacionNumerico(event) ONBLUR = validarCampos('comprobarCampo','telefono') PLACEHOLDER = 'Escriba su teléfono'/>" +
                                "<FONT color=red /><LABEL id='lTelefono' class='asterisco'> *</LABEL></TH>" +
                                "<INPUT TYPE ='button' NAME='botonTelefonoExtra' ID='botonTelefonoExtra' VALUE = '+' ONCLICK = insertarTelefonoExtra('registrar')></TD>" +
                        "</TR>" +
                        "<TR ID = 'telefonoExtra' style = 'display: none;' ALIGN = 'center'>" +
                            "<TD>Tel&eacute;fono Extra:" +
                                "<INPUT TYPE='text' NAME='segundoTelefono' ID='segundoTelefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = return validacionNumerico(event) ONBLUR = validarCampos('comprobarCampo','segundoTelefono') PLACEHOLDER = 'Escriba su segundo teléfono' />" +
                                "<FONT color=red /><LABEL id='lSegundoTelefono' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Correo electr&oacute;nico:"+
                                "<INPUT TYPE='text' NAME='correo' ID='correo' SIZE = '25' MAXLENGTH = '150' ONBLUR = validarCampos('comprobarCampo','correo') PLACEHOLDER = 'Escriba su correo electrónico' />" +
                                "<FONT color=red /><LABEL id='lCorreo' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Instituci&oacute;n:"+
                                "<INPUT TYPE='text' NAME='institucionRepresentada' ID='institucionRepresentada' MAXLENGTH = '40' SIZE = '40' ONBLUR = validarCampos('comprobarCampo','institucion') PLACEHOLDER = 'Escriba su institución o empresa'/>" +
                                "<FONT color=red /><LABEL id='lInstitucion' class='asterisco'> *</LABEL></TH></TD>" +
                        "</TR>" +
                        "<TR>" +
                            "<TD>Puesto:" +
                                "<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto')>" +
                                    "<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
                                    "<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario'>Propietario</OPTION>" +
                                    "<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente'>Asistente</OPTION>" +
                                "</SELECT>" +
                                "<FONT color=red /><LABEL id='lPuesto' class='asterisco'> *</LABEL></TH></TD>" +
                            "</TD>" +
                        "</TR>" +            
                        "<TR>" +
                            "<TD>Comit&eacute;:"+
                                "<SELECT ID = 'seleccionarComite' NAME = 'seleccionarComite' ONCLICK = agregarNuevoComite() ONCHANGE = agregarNuevoComite() ONBLUR = validarCampos('comprobarCampo','comite')>" +
                                "</SELECT>" +
                                "<FONT color=red /><LABEL id='lComite' class='asterisco'> *</LABEL></TH>" +
                            "</TD>" +
                        "</TR>"+
                    "</TBODY>"
                );
                $("#telefono").mask("9999-9999");   
                $("#segundoTelefono").mask("9999-9999");    
                llenarOpcionesComite();
                $(".asterisco").hide();
            } 

            if(puerta == 3){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            if(resultado.split(",")[4] == "")
                                document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3];    
                            else
                                document.getElementById("encabezadoModal").innerHTML = resultado.split(",")[2] + " " + resultado.split(",")[3] + " " + resultado.split(",")[4];    
                            var cadena = resultado.split(",");
                            $("#tablaxinformacionDetallada").append("" +
                                "<TBODY>" +
                                    "<TR hidden>" +
                                        "<TD>" + "<INPUT TYPE='text' NAME='id' ID='id' VALUE = '"+cadena[0]+"'/>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>C&eacute;dula:" +
                                            "<INPUT TYPE='text' NAME='cedula' ID='cedula' SIZE = '18' MAXLENGTH = '15' ONBLUR = validarCedula('comprobarCampo') PLACEHOLDER = 'Campo no obligatorio' VALUE = '"+cadena[1]+"' disabled/>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Nombre:" +
                                            "<INPUT TYPE='text' NAME='nombre' ID='nombre' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','nombre') PLACEHOLDER = 'Escriba su nombre'VALUE = '"+cadena[2]+"' disabled>" +
                                        "</TD>" +
                                    "</TR>" + 
                                    "<TR>" +
                                        "<TD>Apellidos:"+
                                            "<INPUT TYPE='text' NAME='primer_apellido' ID='primer_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido1') PLACEHOLDER = 'Escriba su primer apellido' VALUE = '"+cadena[3]+"' disabled/>" +
                                            "<INPUT TYPE='text' NAME='segundo_apellido' ID='segundo_apellido' SIZE = '30' MAXLENGTH = '30' ONBLUR = validarCampos('comprobarCampo','apellido2') PLACEHOLDER = 'Escriba su segundo apellido' VALUE = '"+cadena[4]+"' disabled/>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Tel&eacute;fono:" +
                                            "<INPUT TYPE='text' NAME='telefono' ID='telefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = return validacionNumerico(event) ONBLUR = validarCampos('comprobarCampo','telefono') PLACEHOLDER = 'Escriba su teléfono' VALUE = '"+cadena[5]+"' disabled/>" +
                                            "<INPUT TYPE ='button' NAME='botonTelefonoExtra' ID='botonTelefonoExtra' VALUE = '+' ONCLICK = insertarTelefonoExtra('registrar')></TD>" +
                                    "</TR>" +
                                        "<TR ID = 'telefonoExtra' style = 'display: none;' ALIGN = 'center'>" +
                                        "<TD>Tel&eacute;fono Extra:" +
                                            "<INPUT TYPE='text' NAME='segundoTelefono' ID='segundoTelefono' SIZE = '15' MAXLENGTH = '8' ONKEYPRESS = return validacionNumerico(event) ONBLUR = validarCampos('comprobarCampo','segundoTelefono') disabled/>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Correo electr&oacute;nico:"+
                                            "<INPUT TYPE='text' NAME='correo' ID='correo' SIZE = '25' MAXLENGTH = '150' ONBLUR = validarCampos('comprobarCampo','correo') PLACEHOLDER = 'Escriba su correo electrónico' VALUE = '"+cadena[9]+"' disabled/>" +
                                        "</TD>" +
                                    "</TR>" +
                                    "<TR>" +
                                        "<TD>Instituci&oacute;n:<INPUT TYPE='text' NAME='institucionRepresentada' ID='institucionRepresentada' MAXLENGTH = '40' SIZE = '40' ONBLUR = validarCampos('comprobarCampo','institucion') PLACEHOLDER = 'Escriba su institución o empresa' VALUE = '"+cadena[6]+"' disabled/>" +
                                    "</TR>" );
                            if(cadena[7] == "Propietario"){
                                $("#tablaxinformacionDetallada").append("" +
                                        "<TR>" +
                                            "<TD>Puesto:" +
                                                "<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto') disabled>" +
                                                    "<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
                                                    "<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario' SELECTED>Propietario</OPTION>" +
                                                    "<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente'>Asistente</OPTION>" +
                                                "</SELECT>" +
                                            "</TD>" +
                                        "</TR>"
                                );
                            }
                            else if(cadena[7] == "Asistente"){
                                $("#tablaxinformacionDetallada").append("" +
                                    "<TR>" +
                                        "<TD>Puesto:" +
                                            "<SELECT ID = 'seleccionarPuesto' NAME = 'seleccionarPuesto' ONBLUR = validarCampos('comprobarCampo','puesto') disabled>" +
                                                "<OPTION VALUE = '' SELECTED DISABLED>Seleccionar puesto</OPTION>" +
                                                "<OPTION VALUE = 'Propietario' ID = 'Propietario' NAME  = 'Propietario'>Propietario</OPTION>" +
                                                "<OPTION VALUE = 'Asistente' ID = 'Asistente' NAME = 'Asistente' SELECTED>Asistente</OPTION>" +
                                            "</SELECT>" +
                                        "</TD>" +
                                    "</TR>"
                                );
                            }                
                            $("#tablaxinformacionDetallada").append("" +
                                    "<TR>" +
                                        "<TD>Comit&eacute;:"+
                                            "<SELECT ID = 'seleccionarComite' NAME = 'seleccionarComite' ONCLICK = agregarNuevoComite() ONCHANGE = agregarNuevoComite() ONBLUR = validarCampos('comprobarCampo','comite') disabled>" +
                                            "</SELECT>" +
                                        "</TD>" +
                                    "</TR>"+
                            "</TBODY>"
                            );

                        }
                        //LLAMA EL METODO QUE DESHABILITA EL BOTON DE AGREGAR UN SEGUNDO TELEFONO Y SEPARA LOS TELEFONOS EN LOS CAMPOS
                        //RESPECTIVOS, EVITANDO COLOCAR LOS DOS TELEFONOS CON EL / EN UN MISMO CAMPO
                        habilitarbotonSegundoTelefono(cadena[5]);
                        llenarOpcionesComite(cadena[8]);
                        $("#telefono").mask("9999-9999");   
                        $("#segundoTelefono").mask("9999-9999");    
                        $(".asterisco").hide();
                    }
                }

            }
        break;

        case guardarArea = 4:
            if(puerta == 1){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            var cadena = resultado.split("_");
                            document.getElementById('encabezadoModal').innerHTML = cadena[1];
                            $("#tablaxinformacionDetallada").append("" +
                                "<tbody>" +
                                    "<tr hidden><td><input type = 'text' id = 'id_id' value = '"+cadena[0]+"' /></td></tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='nombre'>Nombre Completo:</label>" +
                                            "<font color=red ><label id='lNombre' class='asterisco'> *</label></font>" +
                                            "<input type='text' name='nombre' id='nombre' size='30' maxlength='30' value = '"+cadena[1]+"' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) />" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='telefono'>Tel&eacute;fono:</label>" +
                                            "<input type='text' name='telefono' id='telefono' size='10' maxlength='11' value= '"+cadena[2]+"' onkeypress= return validacionNumerico(event) placeholder='88-77-66-55' />" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='detalle'>Detalle:</label>" +
                                            "<textarea id='detalle' rows='5' cols='52' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' >"+cadena[3]+"</textarea>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody>"
                            );
                            $("#telefono").mask("9999-9999");  
                        }
                    }
                }
            }
            if(puerta == 2){
                $("#tablaxinformacionDetallada").append("" +
                    "<tbody>" +
                        "<tr hidden><td><input type = 'text' id = 'id_id'/></td></tr>" +
                        "<tr>" +
                            "<td>" +
                                "<label for='nombre'>Nombre Completo:</label>" +
                                "<font color=red ><label id='lNombre' class='asterisco'> *</label></font>" +
                                "<input type='text' name='nombre' id='nombre' size='30' maxlength='30' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) />" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>" +
                                "<label for='telefono'>Tel&eacute;fono:</label>" +
                                "<input type='text' name='telefono' id='telefono' size='10' maxlength='11' onkeypress=return validacionNumerico(event) placeholder='88-77-66-55' />" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>" +
                                "<label for='detalle'>Detalle:</label>" +
                                "<textarea id='detalle' rows='5' cols='52' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' ></textarea>" +
                            "</td>" +
                        "</tr>" +
                    "</tbody>"
                );
                $("#telefono").mask("9999-9999");  

            }
            if(puerta == 3){
                var xhr=new XMLHttpRequest();
                xhr.open("POST", "../CNegocio/controladoraVoluntario.php");
                xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xhr.send("verDetalle=true" + "&id=" + variableId);
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4 && xhr.status == 200) {

                        var resultado = xhr.responseText;
                        if (resultado != "null") {
                            var cadena = resultado.split("_");
                            document.getElementById('encabezadoModal').innerHTML = cadena[1];
                            $("#tablaxinformacionDetallada").append("" +
                                "<tbody>" +
                                    "<tr hidden><td><input type = 'text' id = 'id_id' value = '"+cadena[0]+"' /></td></tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='nombre'>Nombre Completo:</label>" +
                                            "<font color=red ><label id='lNombre' class='asterisco'> *</label></font>" +
                                            "<input type='text' name='nombre' id='nombre' size='30' maxlength='30' value = '"+cadena[1]+"' placeholder='Michael Andres Salas Granados' onkeypress=return AceptarLetrasNumeros(event) disabled/>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='telefono'>Tel&eacute;fono:</label>" +
                                            "<input type='text' name='telefono' id='telefono' size='10' maxlength='11' value= '"+cadena[2]+"' onkeypress=return validacionNumerico(event) placeholder='88-77-66-55' disabled/>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<label for='detalle'>Detalle:</label>" +
                                            "<textarea id='detalle' rows='5' cols='52' maxlength='300' onkeypress=return AceptarLetrasNumerosComa(event) placeholder='Escribe aqu&iacute; porqu&eacute; quieres ser voluntario' disabled>"+cadena[3]+"</textarea>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody>"
                            );
                            $("#telefono").mask("9999-9999");  
                        }
                    }
                }                
            }            
        break;
    }
    
}

//CARGA EL MODAL PARA ACTUALIZAR
function Seleccionar(txt){

    if(guardarArea == 3){
        document.getElementById('myModal').style.top = "0px";
        document.getElementById('aceptarDatos').value = "Actualizar";
        cargaModalGlobal(1,txt);        
    }
    if(guardarArea == 4){
        document.getElementById('myModal').style.top = "0px";
        document.getElementById('aceptarDatos').value = "Actualizar";
        cargaModalGlobal(1,txt);               
    }
}

//ACCIONA EL BOTON DE REGISTRO ACORDE AL AREA SERA EL MODAL
function verModalRegistrar(){
    
    if(guardarArea == 3){
        document.getElementById('encabezadoModal').innerHTML = "Registrar Integrante";
        document.getElementById('aceptarDatos').value = "Registrar";
        document.getElementById('myModal').style.top = "0px";   
        cargaModalGlobal(2,"");         
    }
    if(guardarArea == 4){
        document.getElementById('encabezadoModal').innerHTML = "Registrar Voluntario";
        document.getElementById('aceptarDatos').value = "Registrar";
        document.getElementById('myModal').style.top = "0px";   
        cargaModalGlobal(2,"");        
    }
}

//CARGAR EL MODAL PARA ELIMINAR
function verDetalleEliminar(txt){
    
    if(guardarArea == 3){
        document.getElementById('myModal').style.top = "0px";
        document.getElementById('aceptarDatos').value = "Eliminar";
        cargaModalGlobal(3,txt);        
    }
    if(guardarArea == 4){
        ConfirmarEliminacion(txt);          
    }
}

function ocultarError() { // Metodo que oculta (visiblemente), los mensajes (DIV).

    $(".asterisco").hide();
    document.getElementById('cedula').style.borderColor = "";
    document.getElementById('nombre').style.borderColor = "";
    document.getElementById('primer_apellido').style.borderColor = "";
    document.getElementById('segundo_apellido').style.borderColor = "";
    document.getElementById('telefono').style.borderColor = "";
    document.getElementById('institucionRepresentada').style.borderColor = "";
    document.getElementById('seleccionarPuesto').style.borderColor = "";
    document.getElementById('seleccionarComite').style.borderColor = "";
    document.getElementById('correo').style.borderColor = "";
}


//LOS CAMBIOS GENERADOS SON ACEPTADOS ACORDE A LA SECCION ELEGIDA
//LOS CAMBIOS DE ELIMINAR, ACTUALIZAR Y REGISTRAR
function aceptarDatos(){

    if(guardarArea == 3){
        switch(document.getElementById('aceptarDatos').value){
            case document.getElementById('aceptarDatos').value = "Actualizar":
                actualizarIntegrante();
                break;
            case document.getElementById('aceptarDatos').value = "Eliminar":
                eliminarIntegrante();
                break;
            case document.getElementById('aceptarDatos').value = "Registrar":
                registrarIntegrante();
                break;
        }
    }
    if(guardarArea == 4){
        switch(document.getElementById('aceptarDatos').value){
            case document.getElementById('aceptarDatos').value = "Actualizar":
                actualizarVoluntario();
                break;
            case document.getElementById('aceptarDatos').value = "Eliminar":
                eliminarVoluntario();
                break;
            case document.getElementById('aceptarDatos').value = "Registrar":
                registrarVoluntario();
                break;
        }
    }
}

//DESTRUYE EL TBODY PARA LIMPIAR TODO EL MODAL, INDISTINTAMENTE SEA DE REGISTRAR, ACTUALIZAR O ELIMINAR
function cancelarDato(){
    $("#tablaxinformacionDetallada TBODY").remove();
    document.getElementById('myModal').style.top = "-100vh";
    document.getElementById('telefonoExtra').style.display = 'none';
}


function ConfirmarEliminacion(id) {

    if(guardarArea == 4){
        document.getElementById("myModal").style.top = "0px";
        document.getElementById('aceptarDatos').value = "Eliminar";
        cargaModalGlobal(3,id);            
    }
}

function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= " + temp);
    segundaSolicitudInformacion(temp,true);

}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= " + temp);
    segundaSolicitudInformacion(temp,false);
}

function segundaSolicitudInformacion(pagina,avanzo,retrocedio){

    if(guardarArea == 3){
        verificarLimitePaginacion("../CNegocio/controladoraDirectorio.php",avanzo,retrocedio);
        mostrarIntegrantes(pagina);
    }

    if(guardarArea == 4){
        verificarLimitePaginacion("../CNegocio/controladoraVoluntario.php",avanzo,retrocedio);
        mostrarIntegrantes(pagina);        
    }
}


function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        busquedaEspecifica();
    }

}


function busquedaEspecifica(){
    var buscar = document.getElementById('buscar').value;
    var tabla = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;
    var area = "";
    var busquedaEspecifica = "";

    if(guardarArea == 3){
        area = "../CNegocio/controladoraDirectorio.php";
        busquedaEspecifica = "buscarIntegranteEspecifico";
    }
    if(guardarArea == 4){
        area = "../CNegocio/controladoraVoluntario.php";
        busquedaEspecifica = "buscarIntegranteEspecifico";
    }

    if(buscar != ""){
        while(i < tabla.rows.length){
            tr = tabla.rows[i];
            for(var j = 1; j < tr.cells.length-1; j++){
                td = tr.cells[j].innerHTML;
                if(buscar.toUpperCase().includes(td.toUpperCase()) && td != ""){
                    if(tr.cells[0].innerHTML.includes(0))
                        temp = tr.cells[1].innerHTML + ",";
                    if(!tr.cells[0].innerHTML.includes(0))
                        temp = tr.cells[0].innerHTML + ",";

                    if(!puerta){
                        puerta = true;
                        dato = "";
                    }
                    if(!dato.includes(temp)){
                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if(dato != "Sin resultado"){
            for(var i = 1; i < tabla.rows.length; i++){                
                if(dato.includes(tabla.rows[i].cells[0].innerHTML) || dato.includes(tabla.rows[i].cells[1].innerHTML)){
                    tabla.rows[i].style.display = "";
                }
                else{
                    tabla.rows[i].style.display = "none";
                }
            }
        }
        else{
            if(dato == "Sin resultado"){

                buscar_en_baseD(buscar, area, busquedaEspecifica);

            }
            else
                resultadoBusqueda(dato);
        }
        
    }
    if(buscar == ""){
        resultadoBusqueda(buscar);
    }

}

function buscar_en_baseD(variable, area, busquedaEspecifica){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(busquedaEspecifica+"=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            if (resultado != 0) {

                switch(guardarArea){
                    case guardarArea = 3:
                        var datos = resultado.split(",");
                        $("tr").remove(".cla");
                        $("#tablaDirectorio").append(""+
                            "<tr class = 'cla'>"+
                                "<td hidden>" +  datos[1]  + " </td>" +
                                "<td>" + datos[2] + "</td>" +
                                "<td>" + datos[3] + "</td>" +
                                "<td>" + datos[4] + "</td>" +
                                "<td>" + datos[5] + "</td>" +
                                "<td>" + datos[9] + "</td>" +
                                "<td>" + datos[6] + "</td>" +
                                "<td>" + datos[8] + "</td>" +
                                "<td>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'verDetalle("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+ 
                                    "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
                                    "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED HIDDEN/>" + "</td>" +
                            "</tr>"
                        );

                    break;

                    case guardarArea = 4:
                        $("tr").remove(".cla"); 
                        $("#tablaDirectorio").append(resultado);
                    break;
                }
            }
            else{
                resultadoBusqueda("Sin resultado");
            }
        }

    }

}

function campoVacio(){
    if(document.getElementById('buscar').value == ""){
        resultadoBusqueda("");
    }   
}
function resultadoBusqueda(texto){  
    if(texto == "Sin resultado"){
        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
    }
    else if(document.getElementById('buscar').value == ""){
        document.getElementById('buscar').style.color = "black";
        mostrarIntegrantes();
    }
}


function verificarPagina(paginaLimite,avanza){
    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
		history.pushState(null, "", "../CPresentacion/ventanaAdminPlanInfor.php?pagina= 1");
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
    	document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
}   

function verificarLimitePaginacion(area,avanza){
	
    var xhr=new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function crearPaginacion(area){
    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", area);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            for(var i = 0; i < resultado; i++){
                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('href',"../CPresentacion/ventanaAdminPlanInfor.php?pagina= "+(i+1));
                liNode.appendChild(href);
                listNode.appendChild(liNode);

            }
            verificarPagina(resultado);
        }
    }

}

function calcularPaginaparaRegistros(){
    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if(url.length == 12){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                if(url[i] == "%"){

                }
                else if(i >= 11){
                    numUrl += url[i];           
                }
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    else if(url.length == 9){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                numUrl += url[i];           
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    return  numUrl;

}

