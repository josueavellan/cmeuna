window.onload = function(event){
	$("#telefonoActualizar").mask("9999-9999");
	$("#segundoTelefonoActualizar").mask("9999-9999");	
	$("#telefono").mask("9999-9999");	
	$("#segundoTelefono").mask("9999-9999");		
	mostrarIntegrantes(calcularPaginaparaRegistros());
	crearPaginacion();
	llenarOpcionesComite();
	limpiarCampos();
	verificarLimitePaginacion(false);
}

function llenarOpcionesComite(){
	
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarComite=true");

	xhr.onreadystatechange = function(){

		if(xhr.readyState == 4 && xhr.status == 200){

			var resultado = xhr.responseText;
			if(resultado != ""){
				var datos = resultado.split(',');
				$("#seleccionarComite").empty()
				$("#seleccionarComite").append(new Option("Seleccione una opción","",true,true));
				$("#seleccionarComiteActualizar").append(new Option("Seleccione una opción","",true,true));
				var opciones = document.getElementById('seleccionarComite');
				for(var i = 0; i <= opciones.length-1; i++){
					if(opciones.options[i].value == ""){
						opciones[i].disabled = true;
					}
				}
				opciones = document.getElementById('seleccionarComiteActualizar');
				for(var i = 0; i <= opciones.length-1; i++){
					if(opciones.options[i].value == ""){
						opciones[i].disabled = true;
					}
				}
				for(var i = 0; i <= datos.length-1; i++){
						if(datos[i] != ""){
						$("#seleccionarComite").append(new Option(datos[i],datos[i],true,false));
						$("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
						$("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
						$("#seleccionarComite").append(new Option("Otro","Otro",true,false));
						$("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false));

						$("#seleccionarComiteActualizar").append(new Option(datos[i],datos[i],true,false));
						$("#seleccionarComiteActualizar OPTION[VALUE = 'Ninguno']").remove()
						$("#seleccionarComiteActualizar OPTION[VALUE = 'Otro']").remove()
						$("#seleccionarComiteActualizar").append(new Option("Otro","Otro",true,false));
						$("#seleccionarComiteActualizar").append(new Option("Ninguno","Ninguno",true,false));
					}
				}				
			}	
		}
	}
}

//insertar un nuevo comite
function agregarNuevoComite(){

	if(document.getElementById('seleccionarComite').value == "Otro"){
		var nuevoComite = prompt("Ingrese el nombre del comite","");
		$("#seleccionarComite").append(new Option(nuevoComite,nuevoComite,true,true));
		$("#seleccionarComite OPTION[VALUE = 'Ninguno']").remove()
		$("#seleccionarComite OPTION[VALUE = 'Otro']").remove()
		$("#seleccionarComite").append(new Option("Otro","Otro",true,false));
		$("#seleccionarComite").append(new Option("Ninguno","Ninguno",true,false));
	}
	if(document.getElementById('seleccionarComiteActualizar').value == "Otro"){
		var nuevoComite = prompt("Ingrese el nombre del comite","");
		$("#seleccionarComiteActualizar").append(new Option(nuevoComite,nuevoComite,true,true));
		$("#seleccionarComiteActualizar OPTION[VALUE = 'Ninguno']").remove()
		$("#seleccionarComiteActualizar OPTION[VALUE = 'Otro']").remove()
		$("#seleccionarComiteActualizar").append(new Option("Otro","Otro",true,false));
		$("#seleccionarComiteActualizar").append(new Option("Ninguno","Ninguno",true,false));
	}
}

//insertar un campo de texto mas para el telefono
var puertaTelefono = true;
function insertarTelefonoExtra(opcion){
	if(opcion == "registrar"){
		document.getElementById('telefonoExtraRegistrar').style.display = "block";
		puertaTelefono = false;
	}
	else{
		document.getElementById('telefonoExtra').style.display = "block";
		puertaTelefono = false;
	}

}
//validacion de números
function validacionNumerico(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function ocultarError() { // Metodo que oculta (visiblemente), los mensajes (DIV).

    $(".asterisco").hide();
    document.getElementById('cedula').style.borderColor = "";
    document.getElementById('nombre').style.borderColor = "";
    document.getElementById('primer_apellido').style.borderColor = "";
    document.getElementById('segundo_apellido').style.borderColor = "";
    document.getElementById('telefono').style.borderColor = "";
    document.getElementById('institucionRepresentada').style.borderColor = "";
    document.getElementById('seleccionarPuesto').style.borderColor = "";
    document.getElementById('seleccionarComite').style.borderColor = "";
    document.getElementById('correo').style.borderColor = "";

    document.getElementById('idActualizar').style.borderColor = "";
	document.getElementById("cedulaActualizar").style.borderColor = "";
	document.getElementById("nombreActualizar").style.borderColor = "";
	document.getElementById("primer_apellidoActualizar").style.borderColor = "";
	document.getElementById("segundo_apellidoActualizar").style.borderColor = "";
	document.getElementById('segundoTelefonoActualizar').style.borderColor  = "";
	document.getElementById('institucionRepresentadaActualizar').style.borderColor = "";
	document.getElementById('seleccionarPuestoActualizar').style.borderColor = "";
	document.getElementById('seleccionarComiteActualizar').style.borderColor = "";
	document.getElementById("telefonoActualizar").style.borderColor = "";
	document.getElementById('actualizarCorreo').style.borderColor = "";

}

//determina el valor del boton de registrar
function botonintegrante(){
	
	//si el resultado es botonregistrar mantiene el nombre del boton como crear
	if(document.getElementById('botonregistrar').value == "Registrar"){

		registrarIntegrante();
	}
	else{

		//caso contrario asigna el nombre del boton como actualizar 
		actualizarIntegrante();
	}
}

function recargarPagina(){

	location.href = "../CPresentacion/ventanaDirectorio.php";
}

function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

    	buscarIntegrante();
    }

}

function verificarPagina(paginaLimite,avanza){
    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
		history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= 1");
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
    	document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
    if(avanza == null){
        if(window.location.search.includes(paginaLimite))
            document.getElementById('siguientePagina').style.pointerEvents = "none";    
        else
            document.getElementById('siguientePagina').style.pointerEvents = "auto";
        if(window.location.search.includes(1))
            document.getElementById('primeraPagina').style.pointerEvents = "none";    
        else
            document.getElementById('primeraPagina').style.pointerEvents = "auto";
    }
}

function verificarLimitePaginacion(avanza){
	
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= " + temp);
    verificarLimitePaginacion(true);
    mostrarIntegrantes(temp);


}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= " + temp);
    verificarLimitePaginacion(false);
    mostrarIntegrantes(temp);
}

function crearPaginacion(){
	$("#pagination").empty();
	var resultado = 0;
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("contarFilas=true");

	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			resultado = xhr.responseText;
			resultado = resultado/4;
			resultado = Math.ceil(resultado)-1;
			for(var i = 0; i < resultado; i++){
				var listNode = document.getElementById('pagination'),
					liNode = document.createElement("LI"),
					txtNode = document.createTextNode(i);
				var href = document.createElement("a");
				href.textContent = i+1;
				href.setAttribute('onclick',"cargarTablaPaginacionNumerica('"+(parseInt(i)+1)+"')");
				liNode.appendChild(href);
				listNode.appendChild(liNode);

			}
			verificarPagina(resultado);
		}
	}

}

function cargarTablaPaginacionNumerica(valor){
	history.pushState(null, "", "../CPresentacion/ventanaDirectorio.php?pagina= " + valor);
    verificarLimitePaginacion(null);
	mostrarIntegrantes(valor);

}

function calcularPaginaparaRegistros(){
	var url = window.location.search;
	var puertaRegistro = false;
	var numUrl = 0;
	if(url.length == 12){
		for(var i = 0; i < url.length; i++){
			if(puertaRegistro){
				if(url[i] == "%"){

				}
				else if(i >= 11){
					numUrl += url[i];			
				}
			}
			if(url[i] == "="){
				puertaRegistro = true;
			}
		}
	}
	else if(url.length == 9){
		for(var i = 0; i < url.length; i++){
			if(puertaRegistro){
				numUrl += url[i];			
			}
			if(url[i] == "="){
				puertaRegistro = true;
			}
		}
	}
	return  numUrl;

}

function mostrarIntegrantes(valor){
    
    var pagina = 0;
    if(valor == 0)
        pagina = calcularPaginaparaRegistros();
    if(valor > 0)
        pagina = valor;

    if(pagina == 1)
        pagina = parseInt(pagina)-1;
    else
        pagina = parseInt(pagina)*3;

	document.getElementById('encargado').innerHTML = "Administrado(a) por: Dennis Muñoz Azofeifa";
	var xhr=new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarIntegrantes=true" + "&pagina=" + pagina);
  	xhr.onreadystatechange = function () {

	    if (xhr.readyState == 4 && xhr.status == 200) {

	        var resultado = xhr.responseText;
	        if (resultado != "null") {
	            //Se aplica un split, para separar cada integrante.
	            var cadena = resultado.split("+");
	            $("tr").remove(".cla"); 
	            for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.

	               var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
	               if(datos[7] == "Propietario" && datos[1] != "123456789"){
	               	$("#tablaDirectorio").append(""+
		                	"<tr class = 'cla'>"+
		                    	"<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'cedulaAjax' ID = 'cedulaAjax' VALUE = "+ datos[1] +" MAXLENGTH = '15' SIZE = '15' DISABLED/>" + "</td>" +
		                    	"<td>" + "<INPUT TYPE ='TEXT' NAME = 'nombreAjax' ID = 'nombreAjax' VALUE = '"+ datos[2] +"'' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido1Ajax' ID = 'apellido1Ajax' VALUE = '"+ datos[3] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido2Ajax' ID = 'apellido2Ajax' VALUE = '"+ datos[4] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'telefonoAjax' ID = 'telefonoAjax' VALUE = "+ datos[5] +" MAXLENGTH = '18' SIZE = '18' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'correoAjax' ID = 'correoAjax' VALUE = '"+ datos[9] +"' MAXLENGTH = '150' SIZE = '25' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'institucionAjax' ID = 'institucionAjax' VALUE = '"+ datos[6] +"' MAXLENGTH = '40' SIZE = '40' DISABLED/>" + "</td>" +
			                    "<td hidden>" + 
			                    			"<SELECT ID = 'puestoAjax' NAME = 'puestoAjax' DISABLED>" + 
			                     				"<OPTION VALUE = 'Propietario' SELECTED >Propietario</OPTION>" +
			                     				"<OPTION VALUE = 'Asistente'>Asistente</OPTION>" +
			                     			"</SELECTED>" +
			                    "</td>" +
			                    "<td hidden>" + "<INPUT TYPE = 'TEXT' NAME = 'comiteAjax' ID = 'comiteAjax' VALUE = '"+datos[8]+"' SIZE = '30' MAXLENGTH = '50' DISABLED>" +
			                    "<td hidden>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'verDetalle("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+	
			                    "<td hidden>" + "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED/>" + "</td>" +
			                "</tr>"
			            );


	               }
	               else if(datos[7] == "Asistente" && datos[1] != "123456789"){
	               	$("#tablaDirectorio").append(""+
		                	"<tr class = 'cla'>"+
		                    	"<td  hidden>" + "<INPUT TYPE ='TEXT' NAME = 'cedulaAjax' ID = 'cedulaAjax' VALUE = "+ datos[1] +" MAXLENGTH = '15' SIZE = '15' DISABLED/>" + "</td>" +
		                    	"<td>" + "<INPUT TYPE ='TEXT' NAME = 'nombreAjax' ID = 'nombreAjax' VALUE = '"+ datos[2] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido1Ajax' ID = 'apellido1Ajax' VALUE = '"+ datos[3] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido2Ajax' ID = 'apellido2Ajax' VALUE = '"+ datos[4] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'telefonoAjax' ID = 'telefonoAjax' VALUE = "+ datos[5] +" MAXLENGTH = '18' SIZE = '18' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'correoAjax' ID = 'correoAjax' VALUE = "+ datos[9] +" MAXLENGTH = '150' SIZE = '25' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'institucionAjax' ID = 'institucionAjax' VALUE = '"+ datos[6] +"' MAXLENGTH = '40' SIZE = '40' DISABLED/>" + "</td>" +
			                    "<td hidden>" + 
			                     			"<SELECT ID = 'puestoAjax' NAME = 'puestoAjax' DISABLED>" + 
			                     				"<OPTION VALUE = 'Propietario'>Propietario</OPTION>" +
			                     				"<OPTION VALUE = 'Asistente' SELECTED>Asistente</OPTION>" +
			                     			"</SELECTED>" +
			                    "</td>" +
			               		"<td hidden>" + "<INPUT TYPE = 'TEXT' NAME = 'comiteAjax' ID = 'comiteAjax' VALUE = '"+datos[8]+"' SIZE = '30' MAXLENGTH = '50' DISABLED>" +
			                    "<td hidden>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = verDetalle("+datos[0]+")><i class='far fa-edit'></i></BUTTON>"+	
			                    "<td hidden>" + "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+datos[0]+")'><i class='fas fa-trash-alt'></i></BUTTON>"+
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED/>" + "</td>" +
			                "</tr>"
		            );
	               }
	            } 
	        } 

		}
	} 
}

function eliminarIntegrante(id){

	var xhr = new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("eliminar=" + id);//Envia el id del integrante seleccionar para enviar a eliminar
	
	xhr.onreadystatechange = function(){

		if(xhr.readyState == 4 && xhr.status == 200){

			var resultado = xhr.responseText;
			if(resultado != 0){
				mostrarIntegrantes();//refresca la pagina  con el integrante quitado
				document.getElementById('myModal').style.top = "-500vh";
				setTimeout("verVentanaConfirmacion('eliminar')",500);
			}
			else{
				alert("Error al eliminar");
			}
		}
	}
}
//PRONTO SE ELIMINA SELECCIONAR
function seleccionarIntegrante(id){
	
	var envio = "seleccionar=" + id; 
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send(envio);//envia el id del integrante para rebuscar y colocarlo en los de registro que pasan hacer para actualizar

	xhr.onreadystatechange = function(){

		if(xhr.readyState == 4 && xhr.status == 200){

			var resultado = xhr.responseText;

			if(resultado != 0){
				var cadena = resultado.split(',');//se le hace split a cada atributo del integrante y se asigna a cada campo respectivo del HTML
				document.getElementById('botonregistrar').value = "Actualizar";
				document.getElementById('id').value = cadena[0];

				document.getElementById('nombre').value = cadena[2];
				document.getElementById('primer_apellido').value = cadena[3];
				document.getElementById('segundo_apellido').value = cadena[4];

				if(cadena[5].includes("/")){
					var telefono = cadena[5].split('/');
					document.getElementById('telefono').value = telefono[0];
					
					$("#tabla").append("",
						"<tr>" + 
							"<td></td> <td></td> <td></td> <td></td><td><INPUT TYPE = 'TEXT' NAME = 'telefonoExtraAjax' ID = 'telefonoExtraAjax' value= "+telefono[1]+" SIZE = '8' MAXLENGTH = '8' ONKEYPRESS = 'return validacionNumerico(event)' PLACEHOLDER='87654321'></td>" +
						"</tr>"
					);
					puertaTelefono = false;
				}
				else
					document.getElementById('telefono').value = cadena[5];	 

				document.getElementById('institucionRepresentada').value = cadena[6];
				var temp = document.getElementById('seleccionarPuesto').value = cadena[7];				
				if(temp == "Propietario"){
					document.getElementById('Propietario').selected = true;
				}
				else{
					document.getElementById('Asistente').selected = true;	
				}

				var opciones = document.getElementById('seleccionarComite');
				for(var i = 0; i <= opciones.length; i++){

					if(cadena[8].includes(opciones.options[i].text)){
						
						document.getElementById('seleccionarComite').selectedIndex = i;
					}
				}			
			}
			else if(resultado == 2){
				alert("Error en registro");
			}
			else if(resultado == 0){
				alert("Error de espacios");
			}
		}
	}
}

function actualizarIntegrante(){
	
	if(document.getElementById('actualizarIntegrante').value == "Actualizar"){
		var telefono = "";
		if(document.getElementById('segundoTelefonoActualizar').value != "" && document.getElementById('segundoTelefonoActualizar').value.length == 9){
			telefono = document.getElementById("telefonoActualizar").value;
			if(telefono.includes("-")){
				telefono = telefono.split("-")[0]+""+telefono.split("-")[1];
			}
			var temp = document.getElementById('segundoTelefonoActualizar').value;
			if(temp.includes("-")){
				temp = temp.split("-")[0]+""+temp.split("-")[1];
			}
			telefono = telefono+"/"+temp;
			var puertaActualizar = true;			
		}
		else{
			telefono = document.getElementById("telefonoActualizar").value;
			if(document.getElementById('telefonoActualizar').value.length == 9){
				telefono = telefono.split("-");
				if(document.getElementById('segundoTelefonoActualizar').value != ""){
					var temp = document.getElementById('segundoTelefonoActualizar').value;
					temp = temp.split("-")[0]+""+temp.split("-")[1];
					telefono = telefono[0]+""+"/"+temp.split("u")[0];
				}
				else
					telefono = telefono[0]+""+telefono[1];
				var puertaActualizar = true;
			}
		}
		if(puertaActualizar){
			var id = document.getElementById('idActualizar').value
			var cedula = document.getElementById("cedulaActualizar").value;
			var nombre = document.getElementById("nombreActualizar").value;
			var primer_apellido = document.getElementById("primer_apellidoActualizar").value;
			var segundo_apellido = document.getElementById("segundo_apellidoActualizar").value;
			var institucionRepresentada = document.getElementById('institucionRepresentadaActualizar').value;
			var seleccionarPuesto = document.getElementById('seleccionarPuestoActualizar').value;
			var seleccionarComite = document.getElementById('seleccionarComiteActualizar').value;
			var correo = document.getElementById('actualizarCorreo').value;

			if(cedula == "")
				cedula = 0;

			var envio ="actualizar=true" + 
						"&idActualizar=" +id +
		               	"&cedulaActualizar=" + cedula +
		               	"&nombreActualizar=" + nombre +
		               	"&primer_apellidoActualizar=" + primer_apellido +
		               	"&segundo_apellidoActualizar=" + segundo_apellido +
		               	"&telefonoActualizar=" + telefono +
		               	"&institucionRepresentadaActualizar=" + institucionRepresentada + 
		               	"&seleccionarPuestoActualizar=" + seleccionarPuesto +
		               	"&seleccionarComiteActualizar=" + seleccionarComite +
		               	"&correoActualizar=" + correo; //se envian los datos para actualizar el integrante

			var xhr = new XMLHttpRequest();
			xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
			xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xhr.send(envio);
			xhr.onreadystatechange = function(){

				if(xhr.readyState == 4 && xhr.status == 200){

					var resultado = xhr.responseText;
					if(resultado == 1){
						mostrarIntegrantes();
						limpiarCampos();//limpia los campos de texto y refresca la pagina
						document.getElementById('myModal').style.top = "-500vh";
						setTimeout("verVentanaConfirmacion('actualizar')",500);
					}
					else if(resultado == 2){
						setTimeout("verVentanaConfirmacion('actualizarError')",500);
						//alert("Error en actualizar");
					}
					else if(resultado == 0){
						setTimeout("verVentanaConfirmacion('actualizarEspacios')",500);
						//alert("Error de espacios");
					}
				}
			}	
		}	
	}
	else if(document.getElementById('actualizarIntegrante').value == "Eliminar"){
		eliminarIntegrante(document.getElementById('idActualizar').value);
	}

}

function validarCedula(opcion){
	var contador = 0;
	var filtro = "123456789";
	var respuesta = false;
	if(opcion == "registrarCampo"){
		var cedula = document.getElementById("cedula").value;
		for(var i = 0; i < cedula.length; i++){
			if(!filtro.includes(cedula[i])){
				contador++;
			}
		}

		if(opcion == "registrarCampo"){
			if(contador >= 3 || cedula.length < 9){
				document.getElementById('cedula').style.borderColor = "red";
				$("#lCedula").show();
			}
			else if(contador < 3 || document.getElementById('cedula').value == ""){
				document.getElementById('cedula').style.borderColor = "";
				$("#lCedula").hide();
				respuesta = true;	
			}
		}	
	}

	if(opcion == "actualizarCampo"){
		var cedula = document.getElementById("cedulaActualizar").value;
		for(var i = 0; i < cedula.length; i++){
			if(!filtro.includes(cedula[i])){
				contador++;
			}
		}

		if(opcion == "actualizarCampo"){
			if(contador >= 3 || document.getElementById('cedulaActualizar').value.length < 9){
				document.getElementById('cedulaActualizar').style.borderColor = "red";
				$("#lCedulaActualizar").show();
			}
			else if(contador < 3 ||document.getElementById('cedulaActualizar').value == ""){
				document.getElementById('cedulaActualizar').style.borderColor = "";
				$("#lCedulaActualizar").hide();	
				respuesta = true;	
			}

		}
	}

	return respuesta;
}

function validarCorreo(opcion){

	var respuesta = false;
	if(opcion == "registrarCampo"){

		if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(document.getElementById('correo').value)){
			document.getElementById('correo').style.borderColor = "";
			$("#lCorreo").hide();
			respuesta = true;
		} 
		else{
			document.getElementById('correo').style.borderColor = "red";
			$("#lCorreo").show();	
		}
		
	}

	if(opcion == "actualizar"){

		if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(document.getElementById('actualizarCorreo').value)){
			document.getElementById('actualizarCorreo').style.borderColor = "";
			$("#lActualizarCorreo").hide();
			respuesta = true;
		} 
		else{
			document.getElementById('actualizarCorreo').style.borderColor = "red";
			$("#lActualizarCorreo").show();	
		}
	}

	return respuesta;
}
function validarCampos(opcion, campoTexto){
	var estado = 0;
	if(opcion == "registrarCampo"){

		var nombre = document.getElementById("nombre").value;
		var primer_apellido = document.getElementById("primer_apellido").value;
		var segundo_apellido = document.getElementById("segundo_apellido").value;
		var telefono = document.getElementById("telefono").value;
		var segundoTelefono = document.getElementById('segundoTelefono').value;
		var correo = document.getElementById('correo').value;
		var institucionRepresentada = document.getElementById('institucionRepresentada').value;
		var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
		var seleccionarComite = document.getElementById('seleccionarComite').value;

		if(nombre == "" && campoTexto == "nombre"){
			document.getElementById('nombre').style.borderColor = "red";
			$("#lNombre").show();
		}
		else{
			document.getElementById('nombre').style.borderColor = "";	
			$("#lNombre").hide();	
		}		
		if(primer_apellido == "" && campoTexto == "apellido1"){
			document.getElementById('primer_apellido').style.borderColor = "red";
			$("#lApellido1").show();
		}
		else{
			document.getElementById('primer_apellido').style.borderColor = "";	
			$("#lApellido1").hide();	
		}		
		if(segundo_apellido == "" && campoTexto == "apellido2"){
			document.getElementById('segundo_apellido').style.borderColor = "red";
			$("#lApellido2").show();			
		}
		else{
			document.getElementById('segundo_apellido').style.borderColor = "";	
			$("#lApellido2").hide();				
		}		
		if(telefono == "" && campoTexto == "telefono" && document.getElementById('telefono').value.length < 8){
			document.getElementById('telefono').style.borderColor = "red";
			$("#lTelefono").show();				
		}
		else{
			document.getElementById('telefono').style.borderColor = "";
			$("#lTelefono").hide();	
		}		
		if(segundoTelefono == "" && campoTexto == "segundoTelefono" && document.getElementById('segundoTelefono').value.length < 8){
			document.getElementById('segundoTelefono').style.borderColor = "red";
			$("#lSegundoTelefono").show();				
		}
		else{
			document.getElementById('segundoTelefono').style.borderColor = "";
			$("#lSegundoTelefono").hide();
		}
		if(campoTexto == "correo"){
			validarCorreo("registrarCampo");			
		}		
		if(institucionRepresentada == "" && campoTexto == "institucion"){
			document.getElementById('institucionRepresentada').style.borderColor = "red";
			$("#lInstitucion").show();	
		}
		else{
			document.getElementById('institucionRepresentada').style.borderColor = "";	
			$("#lInstitucion").hide();	
		}		
		if(seleccionarPuesto == "" && campoTexto == "puesto"){
			document.getElementById('seleccionarPuesto').style.borderColor = "red";
			$("#lPuesto").show();	
		}
		else{
			document.getElementById('seleccionarPuesto').style.borderColor = "";	
			$("#lPuesto").hide();	
		}		
		if(seleccionarComite == "" && campoTexto == "comite"){
			document.getElementById('seleccionarComite').style.borderColor = "red";
			$("#lComite").show();	
		}
		else{
			document.getElementById('seleccionarComite').style.borderColor = "";	
			$("#lComite").hide();	
		}		
		
	}

	if(opcion == "registrar"){
		var nombre = document.getElementById("nombre").value;
		var primer_apellido = document.getElementById("primer_apellido").value;
		var segundo_apellido = document.getElementById("segundo_apellido").value;
		var telefono = document.getElementById("telefono").value;
		var segundoTelefono = document.getElementById('segundoTelefono').value;
		var institucionRepresentada = document.getElementById('institucionRepresentada').value;
		var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
		var seleccionarComite = document.getElementById('seleccionarComite').value;
		if(nombre == ""){
			document.getElementById('nombre').style.borderColor = "red";
			$("#lNombre").show();
		}
		else{
			document.getElementById('nombre').style.borderColor = "";	
			$("#lNombre").hide();	
			estado++;
		}		
		if(primer_apellido == ""){
			document.getElementById('primer_apellido').style.borderColor = "red";
			$("#lApellido1").show();
		}
		else{
			document.getElementById('primer_apellido').style.borderColor = "";	
			$("#lApellido1").hide();
			estado++;	
		}		
		if(segundo_apellido == ""){
			document.getElementById('segundo_apellido').style.borderColor = "red";
			$("#lApellido2").show();			
		}
		else{
			document.getElementById('segundo_apellido').style.borderColor = "";	
			$("#lApellido2").hide();
			estado++;				
		}		
		if(telefono == "" || document.getElementById('telefono').value.length < 8){
			document.getElementById('telefono').style.borderColor = "red";
			$("#lTelefono").show();				
		}
		else{
			document.getElementById('telefono').style.borderColor = "";
			$("#lTelefono").hide();
			estado++;	
		}		
		if(segundoTelefono == "" || document.getElementById('segundoTelefono').value.length < 8){
			document.getElementById('segundoTelefono').style.borderColor = "red";
			$("#lSegundoTelefono").show();				
		}
		else{
			document.getElementById('segundoTelefono').style.borderColor = "";
			$("#lSegundoTelefono").hide();
		}
		if(institucionRepresentada == ""){
			document.getElementById('institucionRepresentada').style.borderColor = "red";
			$("#lInstitucion").show();	
		}
		else{
			document.getElementById('institucionRepresentada').style.borderColor = "";	
			$("#lInstitucion").hide();
			estado++;	
		}		
		if(seleccionarPuesto == ""){
			document.getElementById('seleccionarPuesto').style.borderColor = "red";
			$("#lPuesto").show();	
		}
		else{
			document.getElementById('seleccionarPuesto').style.borderColor = "";	
			$("#lPuesto").hide();	
			estado++;
		}		
		if(seleccionarComite == ""){
			document.getElementById('seleccionarComite').style.borderColor = "red";
			$("#lComite").show();	
		}
		else{
			document.getElementById('seleccionarComite').style.borderColor = "";	
			$("#lComite").hide();
			estado++;	
		}		
		if(validarCorreo("registrarCampo") == true){
			estado++;
		}
		return estado;
	}

	if(opcion == "actualizarCampo"){
		var id = document.getElementById('idActualizar').value
		var nombre = document.getElementById("nombreActualizar").value;
		var primer_apellido = document.getElementById("primer_apellidoActualizar").value;
		var segundo_apellido = document.getElementById("segundo_apellidoActualizar").value;
		var institucionRepresentada = document.getElementById('institucionRepresentadaActualizar').value;
		var seleccionarPuesto = document.getElementById('seleccionarPuestoActualizar').value;
		var seleccionarComite = document.getElementById('seleccionarComiteActualizar').value;
		var telefono = document.getElementById("telefonoActualizar").value;
		var segundoTelefono = document.getElementById('segundoTelefonoActualizar').value;
		var correo = document.getElementById('actualizarCorreo').value;

		if(nombre == "" && campoTexto == "nombre"){
			document.getElementById('nombreActualizar').style.borderColor = "red";
			$("#lNombreActualizar").show();
		}
		else{
			document.getElementById('nombreActualizar').style.borderColor = "";	
			$("#lNombreActualizar").hide();
		}		
		if(primer_apellido == "" && campoTexto == "apellido1"){
			document.getElementById('primer_apellidoActualizar').style.borderColor = "red";
			$("#lApellido1Actualizar").show();
		}
		else{
			document.getElementById('primer_apellidoActualizar').style.borderColor = "";	
			$("#lApellido1Actualizar").hide();	
		}		
		if(segundo_apellido == "" && campoTexto == "apellido2"){
			document.getElementById('segundo_apellidoActualizar').style.borderColor = "red";
			$("#lApellido2Actualizar").show();			
		}
		else{
			document.getElementById('segundo_apellidoActualizar').style.borderColor = "";	
			$("#lApellido2Actualizar").hide();				
		}		
		if(telefono == "" && campoTexto == "telefono" || document.getElementById('telefonoActualizar').value.length < 8){
			document.getElementById('telefonoActualizar').style.borderColor = "red";
			$("#lTelefonoActualizar").show();				
		}
		else{
			document.getElementById('telefonoActualizar').style.borderColor = "";
			$("#lTelefonoActualizar").hide();	
		}	
		if(segundoTelefono == "" && campoTexto == "telefonoExtra" || document.getElementById('segundoTelefonoActualizar').value.length < 8){
			document.getElementById('segundoTelefonoActualizar').style.borderColor = "red";
			$("#lTelefonoExtraActualizar").show();				
		}
		else{
			document.getElementById('segundoTelefonoActualizar').style.borderColor = "";
			$("#lTelefonoExtraActualizar").hide();	
		}	
		if(correo == "" && campoTexto == "actualizarCorreo"){
			validarCorreo("actualizar");			
		}
		else{
			validarCorreo("actualizar");
		}		
		if(institucionRepresentada == "" && campoTexto == "institucion"){
			document.getElementById('institucionRepresentadaActualizar').style.borderColor = "red";
			$("#lInstitucionActualizar").show();	
		}
		else{
			document.getElementById('institucionRepresentadaActualizar').style.borderColor = "";	
			$("#lInstitucionActualizar").hide();	
		}		
		if(seleccionarPuesto == "" && campoTexto == "puesto"){
			document.getElementById('seleccionarPuestoActualizar').style.borderColor = "red";
			$("#lPuestoActualizar").show();	
		}
		else{
			document.getElementById('seleccionarPuestoActualizar').style.borderColor = "";	
			$("#lPuestoActualizar").hide();	
		}		
		if(seleccionarComite == "" && campoTexto == "comite"){
			document.getElementById('seleccionarComiteActualizar').style.borderColor = "red";
			$("#lComiteActualizar").show();	
		}
		else{
			document.getElementById('seleccionarComiteActualizar').style.borderColor = "";	
			$("#lComiteActualizar").hide();	
		}
	}

	if(opcion == "actualizar"){
		var id = document.getElementById('idActualizar').value
		var nombre = document.getElementById("nombreActualizar").value;
		var primer_apellido = document.getElementById("primer_apellidoActualizar").value;
		var segundo_apellido = document.getElementById("segundo_apellidoActualizar").value;
		var institucionRepresentada = document.getElementById('institucionRepresentadaActualizar').value;
		var seleccionarPuesto = document.getElementById('seleccionarPuestoActualizar').value;
		var seleccionarComite = document.getElementById('seleccionarComiteActualizar').value;
		var telefono = document.getElementById("telefonoActualizar").value;
		var segundoTelefono = document.getElementById('segundoTelefonoActualizar').value;

		if(nombre == ""){
			document.getElementById('nombreActualizar').style.borderColor = "red";
			$("#lNombreActualizar").show();
		}
		else{
			document.getElementById('nombreActualizar').style.borderColor = "";	
			$("#lNombreActualizar").hide();
			estado++;	
		}		
		if(primer_apellido == ""){
			document.getElementById('primer_apellidoActualizar').style.borderColor = "red";
			$("#lApellido1Actualizar").show();
		}
		else{
			document.getElementById('primer_apellidoActualizar').style.borderColor = "";	
			$("#lApellido1Actualizar").hide();	
			estado++;
		}		
		if(segundo_apellido == ""){
			document.getElementById('segundo_apellidoActualizar').style.borderColor = "red";
			$("#lApellido2Actualizar").show();			
		}
		else{
			document.getElementById('segundo_apellidoActualizar').style.borderColor = "";	
			$("#lApellido2Actualizar").hide();				
			estado++;
		}		
		if(telefono == "" || document.getElementById('telefonoActualizar').value.length < 8){
			document.getElementById('telefonoActualizar').style.borderColor = "red";
			$("#lTelefonoActualizar").show();				
		}
		else{
			document.getElementById('telefonoActualizar').style.borderColor = "";
			$("#lTelefonoActualizar").hide();	
			estado++;
		}	
		if(segundoTelefono == "" || document.getElementById('segundoTelefonoActualizar').value.length < 8){
			document.getElementById('segundoTelefonoActualizar').style.borderColor = "red";
			$("#lTelefonoExtraActualizar").show();				
		}
		else{
			document.getElementById('segundoTelefonoActualizar').style.borderColor = "";
			$("#lTelefonoExtraActualizar").hide();	
			estado++;
		}			
		if(institucionRepresentada == ""){
			document.getElementById('institucionRepresentadaActualizar').style.borderColor = "red";
			$("#lInstitucionActualizar").show();	
		}
		else{
			document.getElementById('institucionRepresentadaActualizar').style.borderColor = "";	
			$("#lInstitucionActualizar").hide();	
			estado++;
		}		
		if(seleccionarPuesto == ""){
			document.getElementById('seleccionarPuestoActualizar').style.borderColor = "red";
			$("#lPuestoActualizar").show();	
		}
		else{
			document.getElementById('seleccionarPuestoActualizar').style.borderColor = "";	
			$("#lPuestoActualizar").hide();	
			estado++;
		}		
		if(seleccionarComite == ""){
			document.getElementById('seleccionarComiteActualizar').style.borderColor = "red";
			$("#lComiteActualizar").show();	
		}
		else{
			document.getElementById('seleccionarComiteActualizar').style.borderColor = "";	
			$("#lComiteActualizar").hide();	
			estado++;
		}		

		if(validarCorreo("actualizar") == true)
			estado++;

		return estado;
	}

}

function registrarIntegrante(){
	if(validarCampos("registrar") == 8){
		var telefono = "";
		if(!puertaTelefono && document.getElementById('segundoTelefono').value != "" && document.getElementById('telefono').value.length == 9 && document.getElementById('segundoTelefono').value.length == 9){
			telefono = document.getElementById("telefono").value.split("-");
			telefono = telefono[0]+""+telefono[1]+"/";
			var temp = document.getElementById('segundoTelefono').value.split("-");			
			telefono = telefono + temp[0]+""+temp[1];
			var puertaRegistro = true;
		}
		else{
			telefono = document.getElementById("telefono").value.split("-");
			telefono = telefono[0]+""+telefono[1];
			if(document.getElementById('telefono').value.length == 9){
				var puertaRegistro = true;
			}
		}
		if(puertaRegistro){
			var cedula = document.getElementById("cedula").value;
			var nombre = document.getElementById("nombre").value;
			var primer_apellido = document.getElementById("primer_apellido").value;
			var segundo_apellido = document.getElementById("segundo_apellido").value;
			var institucionRepresentada = document.getElementById('institucionRepresentada').value;
			var seleccionarPuesto = document.getElementById('seleccionarPuesto').value;
			var seleccionarComite = document.getElementById('seleccionarComite').value;
			var correo = document.getElementById('correo').value;

			//EN CASO QUE NO SE INGRESE CEDULA, EL REGISTRO DE CEDULA SERA 0
			if(cedula == "")
				cedula = "0";
			//

			var enviar ="registrar=true" +
		               "&cedula=" + cedula +
		               "&nombre=" + nombre +
		               "&primer_apellido=" + primer_apellido +
		               "&segundo_apellido=" + segundo_apellido +
		               "&telefono=" + telefono +
		               "&institucionRepresentada=" + institucionRepresentada + 
		               "&seleccionarPuesto=" + seleccionarPuesto +
		               "&seleccionarComite=" + seleccionarComite +
		               "&correo=" + correo; //envia los datos para registrar el integrante


			var xhr = new XMLHttpRequest();
		  	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
		   	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		   	xhr.send(enviar);

			xhr.onreadystatechange = function () {

			    if (xhr.readyState == 4 && xhr.status == 200) {

			        var resultado = xhr.responseText;

			        if (resultado == 1) {

			        	document.getElementById('modalRegistrar').style.top = "-500vh";
			        	mostrarIntegrantes();//refresca la pagina para actualizar la busqueda de cada integrante
			        	limpiarCampos();//limpia los campos de texto
						setTimeout("verVentanaConfirmacion('registro')",500);

			        } 

			        else{
						setTimeout("verVentanaConfirmacion('registroError')",500);
			           //alert("Error en la consulta" + resultado);

			        } // Fin else.
			    } // Fin if.
			}// Fin onreadystatechange
		}
	}
}

//metodo para limpiar todos los campos de texto
function limpiarCampos(){
	document.getElementById('cedula').value = "";
	document.getElementById('nombre').value = "";
	document.getElementById('primer_apellido').value = "";
	document.getElementById('segundo_apellido').value = "";
	document.getElementById('telefono').value = "";
	document.getElementById('segundoTelefono').value = "";
	document.getElementById('institucionRepresentada').value = "";
	document.getElementById('seleccionarPuesto').value = "";
	document.getElementById('seleccionarComite').value = "";
	document.getElementById('telefonoExtraRegistrar').style.display = 'none';
	document.getElementById('telefonoExtra').style.display = 'none';
	document.getElementById('correo').value = "";
	document.getElementById('buscar').value = "";
	ocultarError();
}

function resultadoBusqueda(texto){	
  	if(texto == "Sin resultado"){
  		document.getElementById('buscar').value = "Sin resultados";
  		document.getElementById('buscar').style.color = "red";
  		document.getElementById('tablaDirectorio').style.display = "";
  	}
  	else if(document.getElementById('buscar').value == ""){
  		document.getElementById('buscar').style.color = "black";
  		mostrarIntegrantes();
  	}
}


function buscarIntegrante(){
	var buscar = document.getElementById('buscar').value;
	var tablaDirectorio = document.getElementById('tablaDirectorio');
	var td = "";
	var tr = "";
	var temp = "";
	var dato = "Sin resultado";
	var i = 0; 
	if(buscar != ""){
		while(i < tablaDirectorio.rows.length){
			tr = tablaDirectorio.rows[i];
			for(var j = 0; j < tr.cells.length-3; j++){
				if(j == 7){
					td = tr.cells[j].getElementsByTagName('select')[0].value;
					if(buscar.toUpperCase().includes(td.toUpperCase())){
						temp += tr.cells[0].getElementsByTagName('input')[0].value + ",";
						if(!dato.includes(temp)){
							dato += temp;
						}
					}
				}
				else if(j < 7 || j > 7){
					td = tr.cells[j].getElementsByTagName('input')[0].value;
					if(buscar.toUpperCase() == td.toUpperCase()){
						temp = tr.cells[0].getElementsByTagName('input')[0].value + ",";
						if(!dato.includes(temp)){
							dato += temp;
						}
					}	
					
				}
			}
			i++;
		}
		if(dato != "Sin resultado"){
			for(var i = 0 ; i < tablaDirectorio.rows.length; i++){
				if(dato.includes(tablaDirectorio.rows[i].getElementsByTagName('input')[0].value)){
					tablaDirectorio.rows[i].style.display = "";
				}
				else{
					tablaDirectorio.rows[i].style.display = "none";
				}
			}
		}
		else{
			if(dato == "Sin resultado"){

				buscar_en_baseD(buscar);

			}
			else
				resultadoBusqueda(dato);
		}
		
	}
	if(buscar == ""){
		resultadoBusqueda(buscar);
	}

}

function buscar_en_baseD(variable){

	var tablaDirectorio = document.getElementById("tablaDirectorio");
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "../CNegocio/controladoraDirectorio.php");
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("buscarIntegranteEspecifico=" + variable);

	xhr.onreadystatechange = function () {

		if (xhr.readyState == 4 && xhr.status == 200) {

			var resultado = xhr.responseText;
			if (resultado != "") {
	            //Se aplica un split, para separar cada integrante.
	            var cadena = resultado.split("+");
	            $("tr").remove(".cla"); 
	            for (var i = 0; i < cadena.length -1; i++) { //Ciclo para mostrar cada integrante.

	               var datos = cadena[i].split(","); //Se aplica split, para dividir cada atributo del integrante.
	               if(datos[7] == "Propietario" && datos[1] != "123456789"){
	               	$("#tablaDirectorio").append(""+
		                	"<tr class = 'cla'>"+
		                    	"<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'cedulaAjax' ID = 'cedulaAjax' VALUE = "+ datos[1] +" MAXLENGTH = '15' SIZE = '15' DISABLED/>" + "</td>" +
		                    	"<td>" + "<INPUT TYPE ='TEXT' NAME = 'nombreAjax' ID = 'nombreAjax' VALUE = '"+ datos[2] +"'' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido1Ajax' ID = 'apellido1Ajax' VALUE = '"+ datos[3] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido2Ajax' ID = 'apellido2Ajax' VALUE = '"+ datos[4] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'telefonoAjax' ID = 'telefonoAjax' VALUE = "+ datos[5] +" MAXLENGTH = '18' SIZE = '18' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'correoAjax' ID = 'correoAjax' VALUE = '"+ datos[9] +"' MAXLENGTH = '150' SIZE = '25' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'institucionAjax' ID = 'institucionAjax' VALUE = '"+ datos[6] +"' MAXLENGTH = '40' SIZE = '40' DISABLED/>" + "</td>" +
			                    "<td hidden>" + 
			                    			"<SELECT ID = 'puestoAjax' NAME = 'puestoAjax' DISABLED>" + 
			                     				"<OPTION VALUE = 'Propietario' SELECTED >Propietario</OPTION>" +
			                     				"<OPTION VALUE = 'Asistente'>Asistente</OPTION>" +
			                     			"</SELECTED>" +
			                    "</td>" +
			                    "<td hidden>" + "<INPUT TYPE = 'TEXT' NAME = 'comiteAjax' ID = 'comiteAjax' VALUE = '"+datos[8]+"' SIZE = '30' MAXLENGTH = '50' DISABLED>" +
			                    "<td hidden>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = 'verDetalle("+datos[0]+")'><i class='far fa-edit'></i></BUTTON>"+	
			                    "<td hidden>" + "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+ datos[0] + ")'><i class='fas fa-trash-alt'></i></BUTTON>"+
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED/>" + "</td>" +
			                "</tr>"
			            );


	               }
	               else if(datos[7] == "Asistente" && datos[1] != "123456789"){
	               	$("#tablaDirectorio").append(""+
		                	"<tr class = 'cla'>"+
		                    	"<td  hidden>" + "<INPUT TYPE ='TEXT' NAME = 'cedulaAjax' ID = 'cedulaAjax' VALUE = "+ datos[1] +" MAXLENGTH = '15' SIZE = '15' DISABLED/>" + "</td>" +
		                    	"<td>" + "<INPUT TYPE ='TEXT' NAME = 'nombreAjax' ID = 'nombreAjax' VALUE = '"+ datos[2] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido1Ajax' ID = 'apellido1Ajax' VALUE = '"+ datos[3] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'apellido2Ajax' ID = 'apellido2Ajax' VALUE = '"+ datos[4] +"' MAXLENGTH = '30' SIZE = '30' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'telefonoAjax' ID = 'telefonoAjax' VALUE = "+ datos[5] +" MAXLENGTH = '18' SIZE = '18' DISABLED/>" + "</td>" +
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'correoAjax' ID = 'correoAjax' VALUE = "+ datos[9] +" MAXLENGTH = '150' SIZE = '25' DISABLED/>" + "</td>" +
			                    "<td>" + "<INPUT TYPE ='TEXT' NAME = 'institucionAjax' ID = 'institucionAjax' VALUE = '"+ datos[6] +"' MAXLENGTH = '40' SIZE = '40' DISABLED/>" + "</td>" +
			                    "<td hidden>" + 
			                     			"<SELECT ID = 'puestoAjax' NAME = 'puestoAjax' DISABLED>" + 
			                     				"<OPTION VALUE = 'Propietario'>Propietario</OPTION>" +
			                     				"<OPTION VALUE = 'Asistente' SELECTED>Asistente</OPTION>" +
			                     			"</SELECTED>" +
			                    "</td>" +
			               		"<td hidden>" + "<INPUT TYPE = 'TEXT' NAME = 'comiteAjax' ID = 'comiteAjax' VALUE = '"+datos[8]+"' SIZE = '30' MAXLENGTH = '50' DISABLED>" +
			                    "<td hidden>" + "<BUTTON ID = 'actualizar' NAME = 'actualizar' ONCLICK = verDetalle("+datos[0]+")><i class='far fa-edit'></i></BUTTON>"+	
			                    "<td hidden>" + "<BUTTON ID = 'eliminar' NAME = 'eliminar' ONCLICK = 'verDetalleEliminar("+datos[0]+")'><i class='fas fa-trash-alt'></i></BUTTON>"+
			                    "<td hidden>" + "<INPUT TYPE ='TEXT' NAME = 'idAjax' ID = 'idAjax' VALUE = "+ datos[0] +" MAXLENGTH = '1' SIZE = '1' DISABLED/>" + "</td>" +
			                "</tr>"
		            );
	               }
	            } 
	        } 
	        else if(resultado == ""){
	        	resultadoBusqueda("Sin resultado");
	        }
	    }

	}

}
function campoVacio(){
	if(document.getElementById('buscar').value == ""){
		resultadoBusqueda("");
	}	
}

function verDetalle(txt){
	document.getElementById('myModal').style.top = "0px";
	var datos = "";
	var datosxMostrar = "";
	var i = 0;
	var encontrado = false;
	while(!encontrado && i < tablaDirectorio.rows.length){	
		if(tablaDirectorio.rows[i].cells[11].getElementsByTagName('input')[0].value == txt){
			var td = tablaDirectorio.rows[i];
			for(var j = 0; j < td.cells.length-2; j++){
				if(j == 7){
					datos += td.cells[j].getElementsByTagName('select')[0].value + ",";
				}
				else if(j < 7 || j == 8){
					datos += td.cells[j].getElementsByTagName('input')[0].value + ",";
				}
				if(j == 8){
					encontrado = true;
				}
			}
		}
		i++;
	}
	datosxMostrar = datos.split(",");
	//LLAMA EL MODAL
	var modal = document.getElementById('myModal');

	//LLAMA EL OBJETO QUE PERMITE CERRAR EL MODAL
	var span = document.getElementsByClassName("close")[0];

	//CUANDO ES EJECUTADO EL BOTON PARA LLAMAR EL MODAL HARA LO QUE CONTINUA ABAJO
	if(datos != ""){
		bloquearCampos("actualizar");
		document.getElementById('idActualizar').value = txt;
		document.getElementById('nombreActualizar').value = datosxMostrar[1];
		document.getElementById('primer_apellidoActualizar').value = datosxMostrar[2];
		document.getElementById('segundo_apellidoActualizar').value = datosxMostrar[3];
		document.getElementById('institucionRepresentadaActualizar').value = datosxMostrar[6];	
		document.getElementById('actualizarCorreo').value = datosxMostrar[5];
		verificarIntegranteComiteSeleccinado(datosxMostrar[8]);

		//VERIFICAR SI LA CEDULA ES DISTINTA A 0 PARA LA CABECERA DE: CEDULA:#
		if(datosxMostrar[0] != 0){
			document.getElementById('cedulaDetalle').innerHTML = "Actualizar: " + datosxMostrar[0];
			document.getElementById('cedulaActualizar').value = datosxMostrar[0];
		}
		if(datosxMostrar[0] == 0){
			document.getElementById('cedulaDetalle').innerHTML = "Actualizar";
			document.getElementById('cedulaActualizar').value = "";
		}
		//

		//EN CASO QUE EL NUMERO DE TELEFONO CONTENGA DOS NUMEROS DIVIDOS POR / ACTIVA EL SEGUNDO CAMPO
		//E INHABILITA EL BOTON, COLOCANDO EN EL SEGUNDO CAMPO EL SEGUNDO NUMERO 
		if(datosxMostrar[4].includes("/")){
			document.getElementById('botonTelefonoExtra').disabled = true;
			document.getElementById("telefonoExtra").style.display = "block";
			document.getElementById("telefonoActualizar").value = datosxMostrar[4].split("/")[0];
			document.getElementById("segundoTelefonoActualizar").value = datosxMostrar[4].split("/")[1];
			puertaTelefono = false;
		}
		//

		//CASO CONTRARIO SI NO CONTIENE EL /
		//MANTIENE HABILITADO EL BOTON DE AGREGAR OTRO NUMERO
		if(!datosxMostrar[4].includes("/")){
			document.getElementById('telefonoActualizar').value = datosxMostrar[4];
			document.getElementById('botonTelefonoExtra').disabled = false;
		}
		//

		//VERIFICA SI ES ASISTENTE O PROPIETARIO PARA EL PUESTO EN EL CAMPO DEL SELECT
		if(datosxMostrar[6] == "Asistente"){
			document.getElementById('seleccionarPuestoActualizar').selectedIndex = 2;	
		}
		else{
			document.getElementById('seleccionarPuestoActualizar').selectedIndex = 1;	
		}
		//
	}

	//CUANDO EL USUARIO DA CLICK EN LA X DEL MODAL HARA QUE SE CIERRE
	span.onclick = function() {
	    modal.style.top = "-500vh";
	    cancelarActualizacion();
	}

	//CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL HARA QUE SE CIERRE
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.top = "-500vh";
	        cancelarActualizacion();
	    }
	}
	
}

function verDetalleEliminar(txt){
	document.getElementById('myModal').style.top = "0px";
	var datos = "";
	var datosxMostrar = "";
	var i = 0;
	var encontrado = false;
	while(!encontrado && i < tablaDirectorio.rows.length){	
		if(tablaDirectorio.rows[i].cells[11].getElementsByTagName('input')[0].value == txt){
			var td = tablaDirectorio.rows[i];

			for(var j = 0; j < td.cells.length-2; j++){
				if(j == 7){
					datos += td.cells[j].getElementsByTagName('select')[0].value + ",";
				}
				else if(j < 7 || j == 8){
					datos += td.cells[j].getElementsByTagName('input')[0].value + ",";
				}
				if(j == 8){
					encontrado = true;
				}
			}
		}
		i++;
	}

	datosxMostrar = datos.split(",");
	//LLAMA EL MODAL DE ELIMINAR
	var modal = document.getElementById('myModal');

	//OBTIENE EL OBJETO QUE PERMITE CERRAR EL MODAL
	var span = document.getElementsByClassName("close")[0];

	//CUANDO ES LLAMADA EL MODAL CARGA TODOS LOS DATOS QUE VIENEN ABAJO 
	if(datos != ""){

		document.getElementById('idActualizar').value = txt;
		document.getElementById('nombreActualizar').value = datosxMostrar[1];
		document.getElementById('primer_apellidoActualizar').value = datosxMostrar[2];
		document.getElementById('segundo_apellidoActualizar').value = datosxMostrar[3];
		document.getElementById('telefonoActualizar').value = datosxMostrar[4];
		document.getElementById('institucionRepresentadaActualizar').value = datosxMostrar[6];
		document.getElementById('actualizarCorreo').value = datosxMostrar[5];
		verificarIntegranteComiteSeleccinado(datosxMostrar[8]);
		bloquearCampos("eliminar");

		document.getElementById('actualizarIntegrante').value = "Eliminar";

		//EN CASO QUE LA CEDULA SEA 0 LA CABECERA DICE ELIMINAR
		if(datosxMostrar[0] == 0){
			document.getElementById('cedulaDetalle').innerHTML = "Eliminar";
			document.getElementById('cedulaActualizar').value = "";
		}
		//

		//SI LA CEDULA ES DISTINTA A 0 LA CABECERA DICE: ELIMINAR:#
		if(datosxMostrar[0] != 0){
			document.getElementById('cedulaActualizar').value = datosxMostrar[0];
			document.getElementById('cedulaDetalle').innerHTML = "Eliminar: " + datosxMostrar[0];
		}
		//

		//VERIFICA SI EL PUESTO EN EL COMITE ES ASISTENTE O PROPIETARIO Y LO SELECCIONA EN EL SELECT
		if(datosxMostrar[6] == "Asistente"){
			document.getElementById('seleccionarPuestoActualizar').selectedIndex = 2;	
		}
		else{
			document.getElementById('seleccionarPuestoActualizar').selectedIndex = 1;	
		}
		//
	}

	//CUANDO EL USUARIO DA CLICK EN LA X CIERRA EL MODAL
	span.onclick = function() {
	        modal.style.top = "-500vh";
	}

	//CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL CIERRA EL MODAL
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.top = "-500vh";
	    }
	}
}

function bloquearCampos(opcion){

	if(opcion == "eliminar"){
		document.getElementById('idActualizar').disabled = true;
		document.getElementById("cedulaActualizar").disabled = true;
		document.getElementById("nombreActualizar").disabled = true;
		document.getElementById("primer_apellidoActualizar").disabled = true;
		document.getElementById("segundo_apellidoActualizar").disabled = true;
		document.getElementById('institucionRepresentadaActualizar').disabled = true;
		document.getElementById('seleccionarPuestoActualizar').disabled = true;
		document.getElementById('seleccionarComiteActualizar').disabled = true;			
		document.getElementById('telefonoActualizar').disabled = true;	
		document.getElementById('actualizarCorreo').disabled = true;
		document.getElementById('botonTelefonoExtra').disabled = true;
	}
	if(opcion == "actualizar"){
		document.getElementById('idActualizar').disabled = false;
		document.getElementById("cedulaActualizar").disabled = false;
		document.getElementById("nombreActualizar").disabled = false;
		document.getElementById("primer_apellidoActualizar").disabled = false;
		document.getElementById("segundo_apellidoActualizar").disabled = false;
		document.getElementById('institucionRepresentadaActualizar').disabled = false;
		document.getElementById('seleccionarPuestoActualizar').disabled = false;
		document.getElementById('seleccionarComiteActualizar').disabled = false;			
		document.getElementById('telefonoActualizar').disabled = false;	
		document.getElementById('actualizarCorreo').disabled = false;
	}
}
function verificarIntegranteComiteSeleccinado(comitexBuscar){
	for(var i = 0; i < seleccionarComiteActualizar.length; i++){
		if(seleccionarComiteActualizar.options[i].value.includes(comitexBuscar)){
			document.getElementById('seleccionarComiteActualizar').selectedIndex = i;
		}
	}
}

function cancelarRegistro(){
	document.getElementById('modalRegistrar').style.top = "-500vh";
	document.getElementById('telefonoExtraRegistrar').value = "";
	document.getElementById('telefonoExtraRegistrar').style.display = 'none';
	limpiarCampos();

}
function cancelarActualizacion(){
	document.getElementById('myModal').style.top = "-500vh";
	document.getElementById('telefonoExtra').style.display = 'none';
	limpiarCampos();
	ocultarError();
}
function verVentanaRegistrar(){
	document.getElementById('modalRegistrar').style.top = "0px";

	var modal = document.getElementById('modalRegistrar');

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("closeRegistrar")[0];

		// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	    modal.style.top = "-500vh";
	    ocultarError();
	    cancelarRegistro();
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.top = "-500vh";
	        ocultarError();
	        cancelarRegistro();
	    }
	}

}

function botonConfirmacion(){

	document.getElementById('modalConfirmacion').style.top = "-500vh";
}

function verVentanaConfirmacion(opcion){
	if(opcion == "registro"){
		crearPaginacion();
		document.getElementById('modalConfirmacion').style.top= "0px";
		document.getElementById('confirmacion').innerHTML = "¡Registro Exitoso!";
		var modal = document.getElementById('modalConfirmacion');

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("closeConfirmacion")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.top = "-500vh";
		    crearPaginacion();
		    verificarLimitePaginacion(false);
		    mostrarIntegrantes();
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
			    modal.style.top = "-500vh";
			}
		}
	}
	else if(opcion == "registroError"){
		document.getElementById('modalConfirmacion').style.top= "0px";
		document.getElementById('confirmacion').style.backgroundColor = "#f44336";
		document.getElementById('botonConfirmacion').style.background = "#f44336";
		document.getElementById('confirmacion').innerHTML = "¡Problema de Registro!";
		var modal = document.getElementById('modalConfirmacion');

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("closeConfirmacion")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.top = "-500vh";
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
			    modal.style.top = "-500vh";
			}
		}
	}
	else if(opcion == "actualizar"){
		document.getElementById('modalConfirmacion').style.top= "0px";
		document.getElementById('confirmacion').innerHTML = "¡Actualización Exitosa!";
		var modal = document.getElementById('modalConfirmacion');

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("closeConfirmacion")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.top = "-500vh";
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
			    modal.style.top = "-500vh";
			}
		}
	}
	else if(opcion == "actualizarError"){
		document.getElementById('modalConfirmacion').style.top= "0px";
		document.getElementById('confirmacion').style.backgroundColor = "#f44336";
		document.getElementById('botonConfirmacion').style.background = "#f44336";
		document.getElementById('confirmacion').innerHTML = "¡Problema de Actualización!";
		var modal = document.getElementById('modalConfirmacion');

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("closeConfirmacion")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.top = "-500vh";
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
			    modal.style.top = "-500vh";
			}
		}
	}
	else if(opcion == "actualizarEspacios"){
		document.getElementById('modalConfirmacion').style.top= "0px";
		document.getElementById('confirmacion').style.backgroundColor = "#f44336";
		document.getElementById('botonConfirmacion').style.background = "#f44336";
		document.getElementById('confirmacion').innerHTML = "¡NO deje espacios en blanco!";
		var modal = document.getElementById('modalConfirmacion');

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("closeConfirmacion")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.top = "-500vh";
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
			    modal.style.top = "-500vh";
			}
		}		
	}
	else if(opcion == "eliminar"){
		crearPaginacion();
		document.getElementById('modalConfirmacion').style.top= "0px";
		document.getElementById('confirmacion').innerHTML = "¡Registro Eliminado!";
		var modal = document.getElementById('modalConfirmacion');

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("closeConfirmacion")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.top = "-500vh";
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
			    modal.style.top = "-500vh";
			}
		}		
	}
}