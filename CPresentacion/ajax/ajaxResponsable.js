window.onload = function(event) {

    $("#telefono").mask("99-99-99-99");
    $("#cedula").mask("9-9999-9999");
    LimpiarCampos();
    Consultar(calcularPaginaparaRegistros());
    crearPaginacion();
    verificarPagina();
}

$(document).ready(function(){
    
   $('#btn_registrar').click(function(){

        document.getElementById("modal").style.top = "0px";
        LimpiarCampos();
   });
   $('#cancelar').click(function(){

        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
   });
   $('.btnEliminar').click(function(){

        document.getElementById("div_modal_confirmacion").style.top = "0px";
        LimpiarCampos();
   });
   $('#btn_cancelar_confirmacion').click(function(){

        document.getElementById("modalEliminar").style.top = "-500vh";
        LimpiarCampos();
   });
    //CUANDO EL USUARIO DA CLICK EN LA X CIERRA EL MODAL
    document.getElementsByClassName("closeEliminar")[0].onclick = function() {
            document.getElementById('modalEliminar').style.top = "-500vh";
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL CIERRA EL MODAL
    window.onclick = function(event) {
        if (event.target == document.getElementById('modalEliminar')) {
            document.getElementById('modalEliminar').style.top = "-500vh";
        }
    }
});

function LimpiarCampos() {

    OcultarMensajes();

    document.getElementById("btn").value = "Registrar";
    document.getElementById("cedula").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("apellido1").value = "";
    document.getElementById("apellido2").value = "";
    document.getElementById("telefono").value = "";
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() { // Metodo que oculta las etiquetas de mensajes.

    $(".mensaje").hide();
    $(".asterisco").hide();
}

function BTN() { // Metodo que verifica si se preciono 'Registrar' o 'Actualizar'.

    OcultarMensajes();

    if (document.getElementById("btn").value == "Registrar") {

        Registrar();
    } else {

        Actualizar();
    }
}

function SoloAceptarNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite.
    if (tecla == 8 || tecla == 0) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros.
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function SoloAceptarLetrasYNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8 || tecla == 0) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function Registrar() { // Metodo que registra un responsable.

    try {
        var nombre = document.getElementById("nombre").value;
        var telefono = document.getElementById("telefono").value;

        // Si el campo nombre o telefono estan vacio, se indicaran con un asterisco rojo.
        if (nombre == "") {

            $("#lNombre").show();
        }
        if (telefono == "") {

            $("#lTelefono").show();
        }

        // Si los datos requeridos son correctos.
        if (nombre != "" && telefono != "") {

            // Se crea el STRING que se enviara por post a la controladora.
            var enviar = "registrar=true" + "&cedula=" + document.getElementById("cedula").value + "&nombre=" + nombre + "&apellido1=" + document.getElementById("apellido1").value + "&apellido2=" + document.getElementById("apellido2").value + "&telefono=" + telefono;

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraResponsable.php");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(enviar);

            xhr.onreadystatechange = function() {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;

                    //alert(resultado);
                    if (resultado != "") {

                        //alert(resultado);
                        if (resultado == 1) {

                            LimpiarCampos();
                            $("#mensaje2").show();
                            Consultar();
                        } else if (resultado == 2) {

                            $("#lCedula").show();
                            $("#mensaje7").show();
                        }
                    } else {

                        $("#mensaje1").show();
                        //alert("Error en la registrar!.\n" + resultado);
                    }
                }
            }
        } else { // Error!

            // Datos incorrectos.
            $("#mensaje5").show();
        }
    } catch (error) {

        console.error(error);
    }
}

function Consultar(valor) {

    try {

        var pagina = 0;
        if(valor == 0)
            pagina = calcularPaginaparaRegistros();
        if(valor > 0)
            pagina = valor;

        if(pagina == 1)
            pagina = parseInt(pagina)-1;
        else
            pagina = parseInt(pagina)*3;

        // Enviamos los datos a la controladora.
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraResponsable.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("consultar=true" + "&pagina=" + pagina);

        xhr.onreadystatechange = function() {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != "null") {

                    //alert(resultado);
                    $("tr").remove(".cla");
                    $("#tabla").append(resultado);
                }
            }
        } // Metodo que consulta la lista de responsables.
    } catch (error) {

        console.error(error);
    }
}

function ConfirmarEliminacion(id) {

    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_responsable").innerHTML = id;
}

function Eliminar() {

    try {

        var temp = document.getElementById("label_id_responsable").innerHTML;
        OcultarMensajes();

        document.getElementById("modalEliminar").style.top = "-500vh";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraResponsable.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("eliminar=" + temp);

        xhr.onreadystatechange = function() {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != 0) {

                    if (resultado == 2) {

                        $("#mensaje6").show();
                    } else {

                        Consultar();
                    }
                } else {

                    $("#mensaje1").show();
                    //alert("Error en eliminar.\n" + resultado);
                }
            }
        } // Metodo que elimina un responsable.
    } catch(error) {

        console.eror(error);
    }
}

function Seleccionar(id) {

    OcultarMensajes();

    document.getElementById("modal").style.top = "0px";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraResponsable.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function() {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                if (resultado != 0) {

                    var cadena = resultado.split(",");

                    document.getElementById("id_id").value = cadena[0];
                    document.getElementById("cedula").value = cadena[1];
                    document.getElementById("nombre").value = cadena[2];
                    document.getElementById("apellido1").value = cadena[3];
                    document.getElementById("apellido2").value = cadena[4];
                    document.getElementById("telefono").value = cadena[5];
                    document.getElementById("btn").value = "Actualizar";
                } else {

                    $("#mensaje1").show();
                    //alert("Error en seleccionar.\n" + resultado);
                }
            }
        } // Metodo que Selecciona un responsable.
}

function Actualizar() {

    var nombre = document.getElementById("nombre").value;
    var telefono = document.getElementById("telefono").value;

    if (nombre == "") {

        $("#lNombre").show();
    }
    if (telefono == "") {

        $("#lTelefono").show();
    }

    if (nombre != "" && telefono != "") {

        var enviar = "actualizar=true" + "&id=" + document.getElementById("id_id").value + "&cedula=" + document.getElementById("cedula").value + "&nombre=" + nombre + "&apellido1=" + document.getElementById("apellido1").value + "&apellido2=" + document.getElementById("apellido2").value + "&telefono=" + telefono;

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraResponsable.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function() {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != "") {

                    //alert(resultado);
                    if (resultado == 1) {

                        //LimpiarCampos();
                        $("#mensaje4").show();
                        Consultar();
                    }
                } else {

                    $("#mensaje1").show();
                    //alert("Error en actualizar.\n" + resultado);
                }
            }
        }
    } else { // Error!.

        $("#mensaje5").show();
    }
}

function CancelarModal() {

    document.getElementById("modal").style.top = "-500vh";
    LimpiarCampos();
}

function detectarTeclaEnter_enBusqueda(e) {

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        buscarResponsable();
    }
}

function buscarResponsable() {

    var buscar = document.getElementById('buscar').value;
    var tablaResponsable = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;

    if (buscar != "") {

        while (i < tablaResponsable.rows.length) {

            tr = tablaResponsable.rows[i];
            for (var j = 1; j < tr.cells.length-1; j++) {

                td = tr.cells[j].innerHTML;

                if (td.toUpperCase().includes(buscar.toUpperCase())) {

                    temp = tr.cells[0].innerHTML + ",";

                    if (!puerta) {

                        puerta = true;
                        dato = "";
                    }
                    if (!dato.includes(temp)) {

                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if (dato != "Sin resultado") {

            for (var i = 1 ; i < tablaResponsable.rows.length; i++) {

                if (dato.includes(tablaResponsable.rows[i].cells[0].innerHTML)) {

                    tablaResponsable.rows[i].style.display = "";
                }
                else {
                    tablaResponsable.rows[i].style.display = "none";
                }
            }
        }
        else{
            if (dato == "Sin resultado") {

                buscar_en_baseD(buscar);
            }
            else {

                resultadoBusqueda(dato);
            }
        }
    }
    if (buscar == "") {

        resultadoBusqueda(buscar);
    }
}

function buscar_en_baseD(variable) {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraResponsable.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarResponsableEspecifico=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else{
                resultadoBusqueda("Sin resultado");
            }
        }
    }
}

function campoVacio() {

    if (document.getElementById('buscar').value == "") {

        resultadoBusqueda("");
    }   
}

function resultadoBusqueda(texto) {

    if (texto == "Sin resultado") {

        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if (document.getElementById('buscar').value == "") {

        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}


function verificarPagina(paginaLimite,avanza){
    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
        history.pushState(null, "", "../CPresentacion/ventanaResponsable.php?pagina= 1");
    }
    if(window.location.search.includes(paginaLimite)){
        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
    if(avanza)
        document.getElementById('primeraPagina').style.pointerEvents = "auto";
    if(!avanza)
        document.getElementById('siguientePagina').style.pointerEvents = "auto";
    if(avanza == null){
        if(window.location.search.includes(paginaLimite))
            document.getElementById('siguientePagina').style.pointerEvents = "none";    
        else
            document.getElementById('siguientePagina').style.pointerEvents = "auto";
        if(window.location.search.includes(1))
            document.getElementById('primeraPagina').style.pointerEvents = "none";    
        else
            document.getElementById('primeraPagina').style.pointerEvents = "auto";
    }
}

function verificarLimitePaginacion(avanza){
    
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraResponsable.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            verificarPagina(resultado,avanza);

        }
    }
}

function cargarSiguienteInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)+1;
    history.pushState(null, "", "../CPresentacion/ventanaResponsable.php?pagina= " + temp);
    verificarLimitePaginacion(true);
    Consultar(temp);


}
function cargarPreviaInformacion(){
    var temp = calcularPaginaparaRegistros();
    temp = parseInt(temp)-1;
    history.pushState(null, "", "../CPresentacion/ventanaResponsable.php?pagina= " + temp);
    verificarLimitePaginacion(false);
    Consultar(temp);
}

function crearPaginacion() {

    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraResponsable.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4 && xhr.status == 200) {

            resultado = xhr.responseText;
            //alert(resultado);
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;

            for (var i = 0; i < resultado; i++) {

                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('onclick',"cargarTablaPaginacionNumerica('"+(parseInt(i)+1)+"')");
                liNode.appendChild(href);
                listNode.appendChild(liNode);
            }
            verificarPagina(resultado);
        }
    }
}

function cargarTablaPaginacionNumerica(valor){
    history.pushState(null, "", "../CPresentacion/ventanaResponsable.php?pagina= " + valor);
    verificarLimitePaginacion(null);
    Consultar(valor);

}



function calcularPaginaparaRegistros() {

    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;

    if (url.length == 12) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                if (url[i] == "%") {
                }
                else if (i >= 11) {

                    numUrl += url[i];           
                }
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    else if (url.length == 9) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                numUrl += url[i];           
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    return  numUrl;
}