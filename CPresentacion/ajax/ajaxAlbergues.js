window.onload = function(event) {
   
  LimpiarCampos();
  Consultar();
  crearPaginacion();
  verificarPagina();
}

$(document).ready(function(){
    
   $('#btn_registrar').click(function(){

        document.getElementById("modal").style.top = "0px";
        LimpiarCampos();
        CargarDatosResponsable("Sin definir");
   });
   $('#cancelar').click(function(){

        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
   });
   $('.btnEliminar').click(function(){

        document.getElementById("div_modal_confirmacion").style.top = "0px";
        LimpiarCampos();
   });
   $('#btn_cancelar_confirmacion').click(function(){

        document.getElementById("modalEliminar").style.top = "-500vh";
        LimpiarCampos();
   });
    //CUANDO EL USUARIO DA CLICK EN LA X CIERRA EL MODAL
    document.getElementsByClassName("closeEliminar")[0].onclick = function() {
            document.getElementById('modalEliminar').style.top = "-500vh";
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL CIERRA EL MODAL
    window.onclick = function(event) {
        if (event.target == document.getElementById('modalEliminar')) {
            document.getElementById('modalEliminar').style.top = "-500vh";
        }
    }
});

function OcultarMensajes() {

    $(".mensaje").hide();
    $(".asterisco").hide();
}

function ValidarTexto(e) {

   tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    // tecla = 8 -> es de retroceso.
    // tecla = 0 -> es de tabulador.
    // tecla = 32 -> es del espacio.
    // tecla = 44 -> es la d ela ',' "coma".
    if (tecla == 8 || tecla == 0 || tecla == 32 || tecla == 44) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    OcultarMensajes();

    return patron.test(tecla_final);
}

function ValidarNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite.
    if (tecla == 8 || tecla == 0) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros.
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function LimpiarCampos() { // Metodo que limpia los inputs de la pagina.

    OcultarMensajes();

    document.getElementById("nombre").value = "";
    document.getElementById("capacidad").value = "";
    document.getElementById("localidad").value = "";
    document.getElementById("luz").checked = false;
    document.getElementById("agua").checked = false;
    document.getElementById("telefono").value = "";
    document.getElementById("btn").value = "Registrar";
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() { // Metodo que oculta (visiblemente), los mensajes (DIV).

    $(".mensaje").hide();  
    $(".asterisco").hide();
}

function CargarDatosResponsable(responsable) { // Metodo que carga los datos de responssable.

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("cargarDatosEncargado=true");

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                var res = resultado.split("+");
                var select = document.getElementById("responsable");

                for (var i = 0; i < res.length - 1; i++) {

                    select.options[i+1] = new Option(res[i]);

                    if (responsable == res[i]) {
                        
                        select.options[i+1].selected = true;
                    }
                }
            } else{

                alert("Error en cargar datos de encargados!\n" + resultado);
            }
        }
    }
}

function BTN() { // Metodo que verifica si el boton es para registrar o actualizar.

    OcultarMensajes();

    if (document.getElementById("nombre").value != "") {

        if (document.getElementById("btn").value == "Registrar") {

            Registrar();
        } else {

            Actualizar();
        }
    }
}

function Registrar() { // Metodo que registra un albergue.
   
    try {
        var nombre    = document.getElementById("nombre").value;
        var encargado = document.getElementById("responsable").value;
        var servicios = 0;
       
        if (document.getElementById("luz").checked == 1) {

            servicios++;
        }
        if (document.getElementById("agua").checked == 1) {

            servicios+=2;
        }
        if (nombre != "") {

            var enviar="registrar=true" +
                        "&nombre="    + nombre +
                        "&capacidad=" + document.getElementById("capacidad").value +
                        "&encargado=" + encargado +
                        "&localidad=" + document.getElementById("localidad").value +
                        "&servicios=" + servicios +
                        "&telefono="  + document.getElementById("telefono").value;
       
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send(enviar);
          
            xhr.onreadystatechange = function () {

                if (xhr.readyState == 4 && xhr.status == 200) {

                    var resultado = xhr.responseText;
                
                    //alert(resultado);
                    if (resultado != 0) {

                        Consultar();
                        LimpiarCampos();
                    } else{

                        alert("Error en registrar albergue!.\n" + resultado);
                    }
                }
            }
        } else {

          $("#lNombre").show();
          alert("Campo nombre vacio!");
        }
    } catch (error) {

        console.error(error);
    }
}

function Consultar() { // Metodo que consulta la lista de albergures en la base de datos.

    var pagina = calcularPaginaparaRegistros();

    if (pagina == 1) {

        pagina = parseInt(pagina-1);
    }
    else {

        pagina = parseInt(pagina*3);
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("consultar=true" + "&pagina=" + pagina);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {
         	
                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
        }
    }
}

function ConfirmarEliminacion(id) {

    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_albergue").innerHTML = id;
}

function Eliminar() { // Metodo que elimina un voluntario.

    var temp = document.getElementById("label_id_albergue").innerHTML;
    OcultarMensajes();

    document.getElementById("modalEliminar").style.top = "-500vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + temp);
    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                if (resultado == 1) { // Exitoso.

                    Consultar();
                    document.getElementById("label_id_albergue").innerHTML = "";
                }
            } else {

                alert("Error en eliminar albergue.\n" + resultado);
            }
        }
    }
    if (document.getElementById("btn").value == "Actualizar") {

        document.getElementById("btn").value = "Registrar";
    }
}

function Cancelar() {

    document.getElementById("label_id_albergue").innerHTML = "";
}

function Seleccionar(id) {
   
    OcultarMensajes();

    document.getElementById("modal").style.top = "0px";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                var cadena = resultado.split("_");
                document.getElementById("id_id").value = cadena[0];
                document.getElementById("nombre").value = cadena[1];
                document.getElementById("capacidad").value = cadena[2];
                CargarDatosResponsable(cadena[3]);
                document.getElementById("localidad").value = cadena[4];
                
                if (cadena[5] == 1) {
                   
                    document.getElementById("luz").checked = true;
                } else if (cadena[5] == 2) {
                   
                    document.getElementById("agua").checked = true;
                } else if (cadena[5] == 3) {

                    document.getElementById("luz").checked = true;
                    document.getElementById("agua").checked = true;
                }
                document.getElementById("telefono").value  = cadena[6];
                document.getElementById("btn").value       = "Actualizar";
            } else {

                $("#mensaje1").show();
                //alert("Error en seleccionar albergue!\n" + resultado);
            }
        }
    }
}

function Actualizar() {

    var id = document.getElementById("id_id").value;
    var nombre    = document.getElementById("nombre").value;
    var capaciad = document.getElementById("capacidad").value;
    var encargado = document.getElementById("responsable").value;
    var localiad = document.getElementById("localidad").value;
    var servicios = 0;
    if (document.getElementById("luz").checked == 1) {
        servicios++;
    }
    if (document.getElementById("agua").checked == 1) {
        servicios+=2;
    }    
    var telefono  = document.getElementById("telefono").value;

    var enviar ="actualizar=true" +
                "&id="        + id +
                "&nombre="    + nombre    +
                "&capacidad=" + capacidad +
                "&encargado=" + encargado +
                "&localidad=" + localidad +
                "&servicios=" + servicios +
                "&telefono="  + telefono;
   
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(enviar);
   
    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                $("#mensaje4").show();
                LimpiarCampos();
                Consultar();
            } else{

                $("#mensaje1").show();
                //alert("Error en actualizar albergue.\n" + resultado);
            }
        }
    }
}

function CancelarModal() {

    document.getElementById("modal").style.top = "-500vh";
    LimpiarCampos();
}

function detectarTeclaEnter_enBusqueda(e) {

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if (key == 13) {

        buscarAlbergue();
    }
}

function buscarAlbergue() {

    var buscar = document.getElementById('buscar').value;
    var tablaAlbergue = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;

    if (buscar != "") {

        while (i < tablaAlbergue.rows.length) {

            tr = tablaAlbergue.rows[i];

            for (var j = 1; j < tr.cells.length-1; j++) {

                td = tr.cells[j].innerHTML;

                if (td.toUpperCase().includes(buscar.toUpperCase())) {

                    temp = tr.cells[0].innerHTML + ",";

                    if (!puerta) {

                        puerta = true;
                        dato = "";
                    }
                    if (!dato.includes(temp)) {

                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if (dato != "Sin resultado") {

            for (var i = 1 ; i < tablaAlbergue.rows.length; i++) {

                if (dato.includes(tablaAlbergue.rows[i].cells[0].innerHTML)) {

                    tablaAlbergue.rows[i].style.display = "";
                }
                else {

                    tablaAlbergue.rows[i].style.display = "none";
                }
            }
        }
        else {
            if (dato == "Sin resultado") {

                buscar_en_baseD(buscar);
            }
            else {

                resultadoBusqueda(dato);
            }
        }
    }
    if (buscar == "") {

        resultadoBusqueda(buscar);
    }
}

function buscar_en_baseD(variable) {

    alert(variable);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarAlbergueEspecifico=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else {

                resultadoBusqueda("Sin resultado");
            }
        }
    }
}

function campoVacio() {

    if (document.getElementById('buscar').value == "") {

        resultadoBusqueda("");
    }   
}

function resultadoBusqueda(texto) { 

    if (texto == "Sin resultado") {

        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if (document.getElementById('buscar').value == "") {

        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}

function verificarPagina(paginaLimite) {

    if (window.location.search == "") {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
        location.href = "../CPresentacion/ventanaAlbergues.php?pagina=1";
    }
    if (window.location.search.includes(paginaLimite)) {

        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if (window.location.search.includes(1)) {

        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
}

function crearPaginacion() {

    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraAlbergues.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4 && xhr.status == 200) {

            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;

            for (var i = 0; i < resultado; i++) {

                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('href',"../CPresentacion/ventanaAlbergues.php?pagina="+(i+1));
                liNode.appendChild(href);
                listNode.appendChild(liNode);
            }
            verificarPagina(resultado);
        }
    }
}

function calcularPaginaparaRegistros() {

    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if (url.length == 12) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                if (url[i] == "%") {
                }
                else if (i >= 11) {

                    numUrl += url[i];           
                }
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    else if (url.length == 9) {

        for (var i = 0; i < url.length; i++) {

            if (puertaRegistro) {

                numUrl += url[i];           
            }
            if (url[i] == "=") {

                puertaRegistro = true;
            }
        }
    }
    return  numUrl;
}