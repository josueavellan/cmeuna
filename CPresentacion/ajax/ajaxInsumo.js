window.onload = function (event) {

    Consultar();
    LimpiarCampos();
    Cargar();
    crearPaginacion();
    verificarPagina();
}

$(document).ready(function(){
    
   $('#btn_registrar').click(function(){

        document.getElementById("modal").style.top = "0px";
        LimpiarCampos();
   });
   $('#cancelar').click(function(){

        document.getElementById("modal").style.top = "-500vh";
        LimpiarCampos();
   });
    //CUANDO EL USUARIO DA CLICK EN LA X CIERRA EL MODAL
    document.getElementsByClassName("closeEliminar")[0].onclick = function() {
            document.getElementById('modalEliminar').style.top = "-500vh";
    }

    //CUANDO EL USUARIO DA CLICK EN CUALQUIER LUGAR FUERA DEL MODAL CIERRA EL MODAL
    window.onclick = function(event) {
        if (event.target == document.getElementById('modalEliminar')) {
            document.getElementById('modalEliminar').style.top = "-500vh";
        }
    }
});

function LimpiarCampos() { // Metodo que limpia los inputs de la pagina.

    OcultarMensajes();

    document.getElementById("articulo").value = "";
    document.getElementById("institucion").value = "";
    document.getElementById("cantidad").value = "";
    document.getElementById("observacion").value = "";
    document.getElementById("btn").value = "Registrar";
    document.getElementById('buscar').value = "";
}

function OcultarMensajes() { // Metodo que oculta (visiblemente), los mensajes (DIV).

    $(".mensaje").hide();
    $(".asterisco").hide();
}

function Cargar() { // Metodo que carga los datos de la bodega.

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraInsumo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("cargar=true");

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                var res = resultado.split("+");
                var select = document.getElementById("bodega");
                select.options[0].selected = true;

                for (var i = 1; i < res.length; i++) {

                    select.options[i] = new Option(res[i - 1]);
                }
            } else {

                alert("Error en cargar datos de la bodega!\n" + resultado);
            }
        }
    }
}

function BTN() { // Metodo que verifica si el boton es para registrar o actualizar.

    OcultarMensajes();

    if (document.getElementById("btn").value == "Registrar") {

        Registrar();
    } else {

        Actualizar();
    }
}

function AceptarNumeros(e) {

    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite.
    if (tecla == 8 || tecla == 0) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros.
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function AceptarTexto(e) {

    tecla = (document.all) ? e.keyCode : e.which;
    
    /* 8 -> Borrar.
     * 0 -> Tabulador.
     * 32-> Espacio.
     */
    if (tecla == 8 || tecla == 0 || tecla == 32 || tecla == 44) {

        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(tecla);

    return patron.test(tecla_final);
}

function Registrar() { // Metodo que registra un insumo.

    // Se obtiene los datos de los inputs.
    var articulo = document.getElementById("articulo").value;
    var cantidad = document.getElementById("cantidad").value;
    var bodega = document.getElementById("bodega").value;

    // Se verifica si algun dato requerido no esta escrito.
    if (articulo == "") {

        $("#lArticulo").show();
    }
    if (cantidad == "") {

        $("#lCantidad").show();
    }
    if (bodega == "") {

        $("#lBodega").show();
    }

    // Si todos los datos estan correctos.
    if (articulo != "" && cantidad != "" && bodega != "") {

        // Se crea e STRING que se enviara a la controladora.
        var enviar = "registrar=true" + "&articulo=" + articulo + "&institucion=" + document.getElementById("institucion").value + "&cantidad=" + cantidad + "&observacion=" + document.getElementById("observacion").value + "&bodega=" + bodega;

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraInsumo.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;

                //alert(resultado);
                if (resultado != "") {

                    //alert(resultado);
                    Consultar();
                    LimpiarCampos();
                    $("#mensaje2").show();
                } else {

                    $("#mensaje1").show();
                    //alert("Error en registrar insumo!.\n" + resultado);
                }
            }
        }
    } else {

        $("#mensaje5").show();
    }
}

function Consultar() { // Metodo que consulta la lista de insumos en la base de datos.
    var pagina = calcularPaginaparaRegistros();

    if(pagina == 1)
        pagina = parseInt(pagina-1);
    else
        pagina = parseInt(pagina*3);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraInsumo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("consultar=true" + "&pagina=" + pagina);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
        }
    }
}

function ConfirmarEliminacion(id) {

    document.getElementById("modalEliminar").style.top = "0px";
    document.getElementById("label_id_insumo").innerHTML = id;
}

function Eliminar() { // Metodo que elimina un insumo en la base de datos.
    
    var temp = document.getElementById("label_id_insumo").innerHTML;
    OcultarMensajes();
    document.getElementById("modalEliminar").style.top = "-500vh";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraInsumo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("eliminar=" + temp);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            if (resultado != 0) {

                //alert(resultado);
                Consultar();
                $("#mensaje3").show();
            } else {
                $("#mensaje1").show();
                //alert("Error en eliminar.\n" + resultado);
            }
        }
    }
}

function Cancelar() {

    document.getElementById("label_id_insumo").innerHTML = "";
    document.getElementById("modalEliminar").style.top = "-500vh";
}

function Seleccionar(id) {

    OcultarMensajes();
    

    document.getElementById("modal").style.top = "0px";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraInsumo.php");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("seleccionar=" + id);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;

            //alert(resultado);
            if (resultado != 0) {

                //alert(resultado);
                var cadena = resultado.split("-");
                document.getElementById("id_id").value = cadena[0];
                document.getElementById("articulo").value = cadena[1];
                document.getElementById("institucion").value = cadena[2];
                document.getElementById("cantidad").value = cadena[3];
                document.getElementById("observacion").value = cadena[4];
                document.getElementById("bodega").value = cadena[5];
                document.getElementById("btn").value = "Actualizar";
            } else {

                $("#mensaje1").show();
                //alert("Error en seleccionar insumo!\n" + resultado);
            }
        }
    }
}

function Actualizar() {

    var articulo = document.getElementById("articulo").value;
    var bodega = document.getElementById("bodega").value;
    var cantidad = document.getElementById("cantidad").value;

    // Se verifica si algun dato requerido no esta escrito.
    if (articulo == "") {

        $("#lArticulo").show();
    }
    if (cantidad == "") {

        $("#lCantidad").show();
    }
    if (bodega == "") {

        $("#lBodega").show();
    }

    // Si todos los datos estan correctos.
    if (articulo != "" && cantidad != "" && bodega != "") {

        var enviar = "actualizar=true" + "&id=" + document.getElementById("id_id").value + "&articulo=" + articulo + "&bodega=" + bodega + "&institucion=" + document.getElementById("institucion").value + "&cantidad=" + cantidad + "&observacion=" + document.getElementById("observacion").value;

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../CNegocio/controladoraInsumo.php");
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(enviar);

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {

                var resultado = xhr.responseText;
                //alert(resultado);
                if (resultado != 0) {

                    //alert(resultado);
                    //LimpiarCampos();
                    Consultar();
                    $("#mensaje4").show();                
                } else {

                    $("#mensaje4").show();
                    //alert("Error en actualizar insumo.\n" + resultado);
                }
            }
        }
    } else {

        $("#mensaje5").show();
    }
}

function Mas(i){
    
    if (document.getElementById(i).style.display == "none" || document.getElementById(i).style.display == "") {

        document.getElementById(i).style.display = "block";
        document.getElementById("btn" + i).value = "Menos...";
    } else {
        document.getElementById(i).style.display = "none";
        document.getElementById("btn" + i).value = "Mas...";
    }
}

function CancelarModal() {

    document.getElementById("modal").style.top = "-500vh";
    LimpiarCampos();
}

function detectarTeclaEnter_enBusqueda(e){

    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if(key == 13){

        buscarInsumo();
    }

}

function buscarInsumo(){
    var buscar = document.getElementById('buscar').value;
    var tablaAlbergue = document.getElementById('tabla');
    var td = "";
    var tr = "";
    var temp = "";
    var dato = "Sin resultado";
    var i = 1; 
    var puerta = false;
    if(buscar != ""){
        while(i < tablaAlbergue.rows.length){
            tr = tablaAlbergue.rows[i];
            for(var j = 1; j < tr.cells.length-1; j++){
                td = tr.cells[j].innerHTML;

                if(td.toUpperCase().includes(buscar.toUpperCase())){
                    temp = tr.cells[0].innerHTML + ",";
                    if(!puerta){
                        puerta = true;
                        dato = "";
                    }
                    if(!dato.includes(temp)){
                        dato += temp;
                    }                    
                }
            }
            i++;
        }
        if(dato != "Sin resultado"){
            for(var i = 1 ; i < tablaAlbergue.rows.length; i++){

                if(dato.includes(tablaAlbergue.rows[i].cells[0].innerHTML)){
                    tablaAlbergue.rows[i].style.display = "";
                }
                else{
                    tablaAlbergue.rows[i].style.display = "none";
                }
            }
        }
        else{
            if(dato == "Sin resultado"){

                buscar_en_baseD(buscar);

            }
            else
                resultadoBusqueda(dato);
        }
        
    }
    if(buscar == ""){
        resultadoBusqueda(buscar);
    }

}

function buscar_en_baseD(variable){
    var tablaVoluntario = document.getElementById("tabla");
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraInsumo.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("buscarInsumoEspecifico=" + variable);

    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4 && xhr.status == 200) {

            var resultado = xhr.responseText;
            if (resultado != 0) {

                $("tr").remove(".cla");
                $("#tabla").append(resultado);
            }
            else{
                resultadoBusqueda("Sin resultado");
            }
        }

    }

}

function campoVacio(){
    if(document.getElementById('buscar').value == ""){
        resultadoBusqueda("");
    }   
}
function resultadoBusqueda(texto){  
    if(texto == "Sin resultado"){
        document.getElementById('buscar').value = "Sin resultados";
        document.getElementById('buscar').style.color = "red";
        document.getElementById('tabla').style.display = "";
    }
    else if(document.getElementById('buscar').value == ""){
        document.getElementById('buscar').style.color = "black";
        Consultar();
    }
}


function verificarPagina(paginaLimite){

    if(window.location.search == ""){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
        location.href = "../CPresentacion/ventanaInsumo.php?pagina=1";
    }
    if(window.location.search.includes(paginaLimite)){

        document.getElementById('siguientePagina').style.pointerEvents = "none";
    }
    if(window.location.search.includes(1)){
        document.getElementById('primeraPagina').style.pointerEvents = "none";
    }
}
function crearPaginacion(){
    $("#pagination").empty();

    var resultado = 0;
    var xhr=new XMLHttpRequest();
    xhr.open("POST", "../CNegocio/controladoraInsumo.php");
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("contarFilas=true");

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            resultado = xhr.responseText;
            resultado = resultado/4;
            resultado = Math.ceil(resultado)-1;
            for(var i = 0; i < resultado; i++){
                var listNode = document.getElementById('pagination'),
                    liNode = document.createElement("LI"),
                    txtNode = document.createTextNode(i);
                var href = document.createElement("a");
                href.textContent = i+1;
                href.setAttribute('href',"../CPresentacion/ventanaInsumo.php?pagina="+(i+1));
                liNode.appendChild(href);
                listNode.appendChild(liNode);

            }
            verificarPagina(resultado);
        }
    }

}

function calcularPaginaparaRegistros(){
    var url = window.location.search;
    var puertaRegistro = false;
    var numUrl = 0;
    if(url.length == 12){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                if(url[i] == "%"){

                }
                else if(i >= 11){
                    numUrl += url[i];           
                }
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    else if(url.length == 9){
        for(var i = 0; i < url.length; i++){
            if(puertaRegistro){
                numUrl += url[i];           
            }
            if(url[i] == "="){
                puertaRegistro = true;
            }
        }
    }
    return  numUrl;

}
