<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
    <title>Responsable</title>
    <link href="../CPresentacion/css/responsableCss.css" rel="stylesheet" type="text/css"/>    
    <LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous""></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->
    <link rel="stylesheet" href="../CPresentacion/css/estilos.css" href="">

    <script src="ajax/jquery-3.3.1.min.js"></script>
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/ajaxResponsable.js" type="text/javascript"></script>
</head>
<body>
    <!-- HEADER -->
    <?php include("includes/generic-header.html"); ?>
    <!----------- MENU ---------------->


    <div id="page" class="container">

        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de Responsable de Albergue</p> 
            <p id = "encargado">Administrado(a) por: Michael Salas</p>
            <!-- Linea divisora -->
            <table id="opcionesMenuDirectorio">
                <thead>
                    <tr>
                        <td>
                            <button id="btn_registrar">Registrar Responsable</button>                
                        </td>
                    </tr>
                </thead>
            </table>
        </div>

        <INPUT TYPE = 'TEXT' ID = 'buscar' MAXLENGTH = '40' SIZE = '40' TITLE = "Nombre, Apellidos es su forma de b&uacute;squeda" ONKEYUP = "campoVacio()" ONKEYDOWN = "detectarTeclaEnter_enBusqueda(event);" PLACEHOLDER = "Buscar responsable espec&iacute;fico    "></INPUT>
        
        <!--FORMUlARIO----------------------------->
        <div id="modal">
            <!-- El cuadro visible -->
            <div id="modal-content">
                <!-- Cabecera -->
                <div class="modal-header">
                    <span class="close" onclick = "CancelarModal();">&times;</span>
                    <h2><p id="cedulaDetalle">Registrar Responsable</p></h2>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-body">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr id = "id_registro">
                            <td>
                                <font color=red><div id="mensaje1" class="mensaje">Error!</div></font>
                                <font color=green ><div id="mensaje2" class="mensaje">Registrado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje4" class="mensaje">Actualizado Correctamente!</div></font>
                                <font color=red ><div id="mensaje5" class="mensaje">Datos incorrectos! Llene los campos.</div></font>
                                <font color=red ><div id="mensaje6" class="mensaje">No se puede eliminar!</div></font>
                                <label id="id_id"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="cedula">C&eacute;dula</label>
                                <font color=red /><label id="lCedula" class="asterisco"> *</label></font>
                                <input type="text" name="cedula" id="cedula" size="10" maxlength="9" placeholder="1-1595-0328" onkeypress="return SoloAceptarNumeros(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="nombre">Nombre</label>
                                <font color=red /><label id="lNombre" class="asterisco"> *</label></font>
                                <input type="text" name="nombre" id="nombre" size="20" maxlength="30" placeholder="Michael" onkeypress="return SoloAceptarLetrasYNumeros(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="apellido1">Primer Apellido</label>
                                <input type="text" name="apellido1" id="apellido1" size="20" maxlength="30" placeholder="Salas" onkeypress="return SoloAceptarLetrasYNumeros(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="apellido2">Segundo Apellido</label>
                                <input type="text" name="apellido2" id="apellido2" size="20" maxlength="30" placeholder="Granados" onkeypress="return SoloAceptarLetrasYNumeros(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="telefono">Tel&eacute;fono</label>
                                <font color=red /><label id="lTelefono" class="asterisco"> *</label></font>
                                <input type="text" name="telefono" id="telefono" size="10" maxlength="8" placeholder="88-77-66-55" onkeypress="return SoloAceptarLetrasYNumeros(event)" />
                                </td>
                        </tr>
                        <tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footer">
                    <input type="button" value="Registrar" name="btn" id="btn" class="btn" onclick="BTN();" />
                    <input type="button" value="Cancelar" name="cancelar" id="cancelar" class="btn" />
                </div>
            </div>
        </div>
        <BR/>
        <!--------------TABLA DE CONSULTAS--------------->
        <div id="div_Tabla">
            <table id="tabla">
                <tr>
                    <th id="id_numero-fila">Fila</th>
                    <th class="class_th">C&eacute;dula </th>
                    <th class="class_th">Nombre</th>
                    <th class="class_th">Primer Apellido</th>
                    <th class="class_th">Segundo Apellido</th>
                    <th class="class_th">Tel&eacute;fono</th>
                    <th class="class_th">Acci&oacute;n</th>
                </tr>
            </table>
        </div>


            <NAV>
                <UL ID= "paginacion" CLASS= "paginacion" style='list-style-type: none;'>
                    <LI CLASS = "page-item" ID = 'paginaAnterior'>
                        <A CLASS = "page-link" ID = 'primeraPagina' ONCLICK = "cargarPreviaInformacion()">Anterior
                        </A>
                    </LI>

                    <LI ID = 'pagination' CLASS = "page-item">
                    </LI>
                    <LI CLASS = "page-item" ID = 'siguientePagina'>
                        <A CLASS = "page-link " ONCLICK = "cargarSiguienteInformacion()">Siguiente
                        </A>
                    </LI>
                </UL>
            </NAV>

    <!-- Modal confirmacion -->

            <div id="modalEliminar">
                <!-- El cuadro visible -->
                <div id="modal-contentEliminar">
                    <!-- Cabecera -->
                    <div class="modal-headerEliminar">
                        <span class="closeEliminar" onclick = "CancelarModal();">&times;</span>
                        <h2><p id="cedulaDetalle">Eliminar Responsable</p></h2>
                    </div>
                    <!-- Cuerpo del modal Formulario! -->
                    <div class="modal-bodyEliminar">
                        <!-- Tabla del Formulario -->
                        <table id = "tablaxinformacionDetallada">
                            <tr>
                                <td id = "label_confirmacion">¿Desea realmente eliminarlo?</td>                                
                            </tr>
                            <tr>
                                <td><label id = "label_id_responsable" value = ""></td>
                            </tr>
                        </table>
                    </div>
                    <!-- Pie del formulario -->
                    <div class="modal-footerEliminar">
                        <input type="button" value="Eliminar" id="btn_confirmar" class="btn_confirmacion" onclick="Eliminar();" />
                        <input type="button" value="Cancelar" id="btn_cancelar_confirmacion" class="btn_confirmacion " onclick="Cancelar();"/>
                    </div>
                </div>
            </div>

        </div>

    <div id="footer">   
    <p>Pagina en desarrollo para el Comite Municipal de Emergencias de Sarapiqui</p>
    <p>Elaborado en el curso de Ingenieria de Software II UNA</p>
    </div>

    <?php include("includes/generic-footer.html"); ?>      

</body>
</html>