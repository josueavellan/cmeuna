<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
    <title>Administrador(a) P.I</title>
    <link href="../CPresentacion/css/adminPlanInforCss.css" rel="stylesheet" type="text/css"/>    
    <LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>
    <LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/default.css"/>
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
    <script src="../CPresentacion/ajax/jquery-3.3.1.min.js"></script>
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/ajaxAdminPlanInfor.js" type="text/javascript"></script>
</head>
<body>
    <!-- HEADER -->
    <div id="wrapper">
        <div id="header-wrapper">
            <div id="header" class="container">
                <div id="logo">         
                    <img src="imagenes/logo.png" width="100" alt=""/>
                <h1>
                    <a href="index.html">Comit&eacute; Municipal de Emergencias</a>
                </h1>
                </div>
            </div>
            <div id="menu" class="container">
                <ul>
                    <li class="current_page_item"><a href="index.html" accesskey="1" title="">Inicio</a></li>
                    <li><a href="#" accesskey="1" title="">Servicios</a></li>
                    <li><a href="#" accesskey="2" title="">Misi&oacute;n y Visi&oacute;n</a></li>
                    <li><a href="#" accesskey="3" title="">Nosotros</a></li>
                    <li><a href="#" accesskey="4" title="">CME</a></li>
                    <li><a href="#" accesskey="5" title="">Contactenos</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Menu 2 -->
    <div id="nav_Menu" class= "contenedor">
        <ul>
           <li class="pagina_de_acceso">
                <a href="#Alertas" id = "link" onclick="desplegarInformacion('1');">Alerta</a>
            </li>
            <li class="pagina_de_acceso">
                <a href="#Anuncios" id = "link" onclick="desplegarInformacion('2');">Anuncio</a>
            </li>
            <li class="pagina_de_acceso">
                <a id = "link" onclick = "desplegarInformacion('3')">Directorio</a>
            </li>
            <li class="pagina_de_acceso">
                <a id = "link" onclick="desplegarInformacion('4');">Voluntario</a>
            </li>
        </ul>
    </div>
   
    <div id="page" class="container">

        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de </p> 
            <p id = "encargado">Administrado(a) por: </p>
            <!-- Linea divisora -->
            <table id="opcionesMenu">
                <thead>
                    <tr>
                        <td>
                            <!-- Boton Registrar nuevo Valor -->
                            <INPUT TYPE = 'button' id="btn_registrar" hidden ONCLICK = "verModalRegistrar()"></INPUT>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>

        <INPUT TYPE = 'TEXT' ID = 'buscar' MAXLENGTH = '40' SIZE = '40' TITLE = "" ONKEYUP = "campoVacio()" ONKEYDOWN = "detectarTeclaEnter_enBusqueda(event)" PLACEHOLDER = "" hidden></INPUT>

        <div style = "position: relative;overflow: auto;">
            <table id="tabla" hidden>
                <tbody id = 'tablaDirectorio'></tbody>
            </table>
        </div>

        <NAV>
            <UL ID= "paginacion" CLASS= "paginacion" style='list-style-type: none;'>
                <LI CLASS = "page-item" ID = 'paginaAnterior'>
                    <A CLASS = "page-link" ID = 'primeraPagina' ONCLICK = "cargarPreviaInformacion()">
                        Anterior
                    </A>
                </LI>
                <LI ID = 'pagination' CLASS = "page-item">
                </LI>
                <LI CLASS = "page-item" ID = 'siguientePagina'>
                    <A CLASS = "page-link " ONCLICK = "cargarSiguienteInformacion()">
                        Siguiente
                    </A>
                </LI>
            </UL>
        </NAV>

        <DIV id="myModal" class="modal">
            <DIV class="modal-content">
                <DIV class="modal-header">
                    <SPAN class="close">&times;</SPAN>
                        <H2><P ID="encabezadoModal"></P></H2>
                </DIV>
                <DIV class="modal-body">
                    <TABLE id = 'tablaxinformacionDetallada'>
                                   
                    </TABLE>
                </DIV>
                <DIV class="modal-footer">
                    <INPUT TYPE = 'BUTTON' ID = 'aceptarDatos' NAME = 'aceptarDatos' VALUE = 'Actualizar' ONCLICK = "aceptarDatos()">
                    <INPUT TYPE = 'BUTTON' ID = 'cancelarDatos' NAME = 'cancelarDatos' VALUE = 'Cancelar' ONCLICK = 'cancelarDato()'>
                </DIV>
            </DIV>
        </DIV>

    </div>
    <div id="footer">   
        <p>Pagina en desarrollo para el Comite Municipal de Emergencias de Sarapiqui</p>
        <p>Elaborado en el curso de Ingenieria de Software II UNA</p>
    </div>

    <DIV id="modalConfirmacion" class="modalConfirmacion">

        <!-- Modal content -->
        <DIV class="modal-contentConfirmacion">
            <DIV class="modal-headerConfirmacion">
                <SPAN class="closeConfirmacion">&times;</SPAN>
            </DIV>
            <DIV class="modal-bodyConfirmacion">            
                <P ID = "confirmacion"></P>
            </DIV>
            <DIV class="modal-footerConfirmacion">
                <INPUT TYPE = 'BUTTON' ID = 'botonConfirmacion' NAME = 'botonConfirmacion' VALUE = 'Cerrar' ONCLICK = 'botonConfirmacion()'>
            </DIV>
        </DIV>
    </DIV>
</body>
</html>