<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Zona de Riesgo</title>
    
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
    <link rel="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="../CPresentacion/css/zonaRiesgoCss.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení <responsive--></responsive-->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous""></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="../CPresentacion/ajax/jquery-3.3.1.min.js"></script>
    <script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../CPresentacion/ajax/ajaxZonaRiesgo.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyDNZCK5NCo8SO8f14wMWGAn2W2oT6nSGcE"></script>
</head>
<body>
    <!-- HEADER -->
    <?php include("includes/generic-header.html"); ?>
    <div id="page" class="container">
        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de Zona de Riesgo</p> 
            <p id = "encargado">Administrado(a) por: Michael Salas</p>
            <!-- Linea divisora -->
            <table id="opcionesMenuDirectorio">
                <thead>
                    <tr>
                        <td>
                            <!-- Boton Registrar nuevo Bodega -->
                            <button id="btn_registrar">Registrar Zona de Riesgo</button>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>
        <Input type = 'text' id = 'buscar' maxlength = '40' size = '40' title = "Lugar, tipo de riesgo es su forma de b&uacute;squeda" onkeyup = "campoVacio()" onkeydown = "detectarTeclaEnter_enBusqueda(event);" placeholder = "Buscar bodega espec&iacute;fica"></input>
        <!-- Formulario -->
        <!-- Se encuntra oculta cuando se carga la pagina al precionar "Registrar" se mostrara -->
        <div id="modal">
            <!-- El cuadro visible -->
            <div id="modal-content">
                <!-- Cabecera -->
                <div class="modal-header">
                    <span class="close" onclick = "CancelarModal();">&times;</span>
                    <h2><p id="cedulaDetalle">Registro Zona de riesgo</p></h2>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-body">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr id = "id_registro">
                            <td>
                                <font color=red><div id="mensaje1" class="mensaje">Error!</div></font>
                                <font color=green ><div id="mensaje2" class="mensaje">Registrado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje3" class="mensaje">Eliminado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje4" class="mensaje">Actualizado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje5" class="mensaje">Ingrese los datos correctamente!</div></font>
                                <label id="id_id"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="lugar">Lugar</label><br/>
                                <font color=red ><label id="lLugar" class="asterisco"> *</label></font>
                                <input type="text" name="lugar" id="lugar" size="30" maxlength="30" placeholder="Horquetas, Sarapiqui" onkeypress="return AceptarTexto(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="tipo">Tipo Riesgo:</label><br/>
                                <font color=red ><label id="lTipo" class="asterisco"> *</label></font>
                                <input type="text" name="tipo" id="tipo" size="30" maxlength="30" onkeypress="return AceptarTexto(event)" placeholder="Desbordamiento" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="albergue">Albergue m&aacute;s Cercano:</label><br/>
                                <select name="albergue" id="albergue">
                                    <option value="">Sin definir</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td id="td_mapa">
                                <button onclick="GenerarCoordenadas();">Ubicaci&oacute;n Actual</button>
                                <!--<input type="text" id="coords" />-->
                                <div id="map"></div>
                                <br/>
                                <Label>Latitud: </Label>
                                <Label id = "label_latitud"></Label>
                                <br/>
                                <label>Longitud: </label>
                                <Label id = "label_longitud"></Label>
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footer">
                    <input type="button" value="Registrar" id="btn" class="btn" onclick="BTN();" />
                    <input type="button" value="Cancelar" id="cancelar" class="btn" />
                </div>
            </div>
        </div>
        <!-- Tabla -->
        <div id="div_Tabla">
            <table id="tabla">
                <tr>
                    <th id="id_numero-fila">Fila</th> <!-- id -->
                    <th class="class_th">Lugar</th>
                    <th class="class_th">Tipo de riesgo</th>
                    <th class="class_th">Albergue m&aacute;s cercano</th>
                    <th class="class_th">Acción</th>
                </tr>
            </table>
        </div>
        <!-- Mensaje de que no hay datos -->
        <div id="divNoHaydatos">
            No hay datos en la DB.
        </div>
        <div id="div_mapa">
            <table id="tabla_mapa">
                <tr>
                    <th id="id_mapa"></th>
                    <th class="class_th_mapa">Lugar</th>
                    <th class="class_th_mapa">Tipo de riesgo</th>
                    <th class="class_th_mapa">Albergue m&aacute;s cercano</th>
                    <th class="class_th_mapa">Acción</th>
                </tr>
            </table>
            <div id="mapa"></div>
        </div>
           <nav>
                <ul id= "paginacion" class= "paginacion" style='list-style-type: none;'>
                    <li class = "page-item" id = 'paginaAnterior'>
                        <a class = "page-link" id = 'primeraPagina' href = "../CPresentacion/ventanaZonaRiesgo.php?pagina=<?php echo $_GET['pagina']-1?>">Anterior
                        </a>
                    </li>

                    <li id = 'pagination' class = "page-item">
                    </li>
                    <li class = "page-item" id = 'siguientePagina'>
                        <a class = "page-link " href = "../CPresentacion/ventanaZonaRiesgo.php?pagina=<?php echo $_GET['pagina']+1?>">Siguiente
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- Modal confirmacion -->
            <div id="modalEliminar">
                <!-- El cuadro visible -->
                <div id="modal-contentEliminar">
                    <!-- Cabecera -->
                    <div class="modal-headerEliminar">
                        <span class="closeEliminar" onclick = "CancelarModal();">&times;</span>
                        <h2><p id="cedulaDetalle">Eliminar Punto de Riesgo</p></h2>
                    </div>
                    <!-- Cuerpo del modal Formulario! -->
                    <div class="modal-bodyEliminar">
                        <!-- Tabla del Formulario -->
                        <table id = "tablaxinformacionDetallada">
                            <tr>
                                <td id = "label_confirmacion">¿Desea realmente eliminarlo?</td>                                
                            </tr>
                            <tr>
                                <td><label id = "label_id_puntoriesgo" value = ""></td>
                            </tr>
                        </table>
                    </div>
                    <!-- Pie del formulario -->
                    <div class="modal-footerEliminar">
                        <input type="button" value="Eliminar" id="btn_confirmar" class="btn_confirmacion" onclick="Eliminar();" />
                        <input type="button" value="Cancelar" id="btn_cancelar_confirmacion" class="btn_confirmacion " onclick="Cancelar();"/>
                    </div>
                </div>
            </div>
    </div>
    <!-- Pie de pagina -->
    <div id="footer">   
        <p>Pagina en desarrollo para el Comite Municipal de Emergencias de Sarapiqui</p>
        <p>Elaborado en el curso de Ingenieria de Software II UNA</p>
    </div>
    <?php include("includes/generic-footer.html"); ?>      
</body>
</html>