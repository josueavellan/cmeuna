<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
    <title>Insumos</title>
    <link href="../CPresentacion/css/insumoCss.css" rel="stylesheet" type="text/css"/>    
    <LINK REL="stylesheet" type="text/css" href="CPresentacion/css/fonts.css"/>
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous""></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->
    <link rel="stylesheet" href="../CPresentacion/css/estilos.css" href="">

    <script src="../CPresentacion/ajax/jquery-3.3.1.min.js"></script>
    <script src="../CPresentacion/ajax/ajaxInsumo.js" type="text/javascript"></script>
</head>
<body>
    <!-- HEADER -->
    <?php include("includes/generic-header.html"); ?>
<!----------- MENU ---------------->

    <div id="page" class="container">

        <!-- Titulo de pagina -->
        <div id = "cabeceraPrincipal">
            <p id = "tituloCabecera">Secci&oacute;n de Insumos</p> 
            <p id = "encargado">Administrado(a) por: Michael Salas</p>
            <!-- Linea divisora -->
            <table id="opcionesMenuDirectorio">
                <thead>
                    <tr>
                        <td>
                            <button id="btn_registrar">Registrar Insumo</button>                
                        </td>
                    </tr>
                </thead>
            </table>
        </div>

        <INPUT TYPE = 'TEXT' ID = 'buscar' MAXLENGTH = '40' SIZE = '40' TITLE = "Art&iacute;culo, cantidad, institucion es la forma de b&uacute;squeda" ONKEYUP = "campoVacio()" ONKEYDOWN = "detectarTeclaEnter_enBusqueda(event);" PLACEHOLDER = "Buscar insumo espec&iacute;fico    "></INPUT>
  
        <!-- Formulario -->
        <div id="modal">
            <!-- El cuadro visible -->
            <div id="modal-content">
                <!-- Cabecera -->
                <div class="modal-header">
                    <span class="close"  onclick = "CancelarModal();">&times;</span>
                    <h2><p id="cedulaDetalle">Registro Insumo</p></h2>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-body">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr id = "id_registro">
                            <td>
                                <font color=red><div id="mensaje1" class="mensaje">Error!</div></font>
                                <font color=green ><div id="mensaje2" class="mensaje">Registrado Correctamente!</div></font>
                                <font color=blue ><div id="mensaje4" class="mensaje">Actualizado Correctamente!</div></font>
                                <font color=red ><div id="mensaje5" class="mensaje">Datos incorrectos!</div></font>
                                <label id="id_id"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="articulo">Art&iacute;culo:</label>
                                <font color=red ><label id="lArticulo" class="asterisco"> *</label></font>
                                <input type="text" name="articulo" id="articulo" size="30" maxlength="30" placeholder="Colchones" onkeypress="return AceptarTexto(event)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="institucion">Instituci&oacute;n:</label>
                                <input type="text" name="institucion" id="institucion" size="30" maxlength="30" onkeypress="return AceptarTexto(event)" placeholder="Campus sarapiqui" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="cantidad">Cantidad:</label>
                                <font color=red /><label id="lCantidad" class="asterisco"> *</label></font>
                                <input type="text" name="cantidad" id="cantidad" size="30" maxlength="10" onkeypress="return AceptarNumeros(event)" placeholder="100" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="observacion">Observaci&oacute;n:</label>
                                <textarea id="observacion" rows="5" cols="52" maxlength="300" onkeypress="return AceptarTexto(event)" placeholder="Escribe aqui la descripcion del insumo"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="bodega">Bodega:</label>
                                <font color=red /><label id="lBodega" class="asterisco"> *</label></font>
                                <select id="bodega">
                                    <option value="">Sin definir</option>
                                    </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footer">
                    <input type="button" value="Registrar" id="btn" class="btn" onclick="BTN();" />
                    <input type="button" value="Cancelar" id="cancelar" class="btn"" />
                    <!--<input type = 'BUTTON' id = 'actualizarIntegrante' name = 'actualizarIntegrante' value = 'Actualizar' onclick = "actualizarIntegrante()">
                    <input type = 'BUTTON' id = 'cancelarActualizacion' name = 'cancelarActualizacion' value = 'Cancelar' onclick = 'cancelarActualizacion()'>-->
                </div>
            </div>
        </div>
        <div id="div_Tabla">
            <table id="tabla">
                <tr>
                    <th  id="id_numero-fila">Fila</th>
                    <th class="class_th">Art&iacute;culo</th>
                    <th class="class_th">Instituci&oacute;n donante</th>
                    <th class="class_th">Cantidad</th>
                    <th class="class_th">Observaci&oacute;n</th>
                    <th class="class_th">Bodega</th>
                    <th class="class_th">Acci&oacute;n</th>
                </tr>
            </table>
        </div>

        <NAV>
            <UL ID= "paginacion" CLASS= "paginacion" style='list-style-type: none;'>
                <LI CLASS = "page-item" ID = 'paginaAnterior'>
                    <A CLASS = "page-link" ID = 'primeraPagina' HREF = "../CPresentacion/ventanaInsumo.php?pagina=<?php echo $_GET['pagina']-1?>">Anterior
                    </A>
                </LI>

                <LI ID = 'pagination' CLASS = "page-item"></LI>
                <LI CLASS = "page-item" ID = 'siguientePagina'>
                    <A CLASS = "page-link " HREF = "../CPresentacion/ventanaInsumo.php?pagina=<?php echo $_GET['pagina']+1?>">Siguiente</A>
                </LI>
            </UL>
        </NAV>

        <div id="modalEliminar">
            <!-- El cuadro visible -->
            <div id="modal-contentEliminar">
                <!-- Cabecera -->
                <div class="modal-headerEliminar">
                    <span class="closeEliminar" onclick = "CancelarModal();">&times;</span>
                    <h2><p id="cedulaDetalle">Eliminar Insumo</p></h2>
                </div>
                <!-- Cuerpo del modal Formulario! -->
                <div class="modal-bodyEliminar">
                    <!-- Tabla del Formulario -->
                    <table id = "tablaxinformacionDetallada">
                        <tr>
                            <td id = "label_confirmacion">¿Desea realmente eliminarlo?</td>                                
                        </tr>
                        <tr>
                            <td><label id = "label_id_insumo" value = ""></td>
                        </tr>
                    </table>
                </div>
                <!-- Pie del formulario -->
                <div class="modal-footerEliminar">
                    <input type="button" value="Eliminar" id="btn_confirmar" class="btn_confirmacion" onclick="Eliminar();" />
                    <input type="button" value="Cancelar" id="btn_cancelar_confirmacion" class="btn_confirmacion " onclick="Cancelar();"/>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">   
        <p>Pagina en desarrollo para el Comite Municipal de Emergencias de Sarapiqui</p>
        <p>Elaborado en el curso de Ingenieria de Software II UNA</p>
    </div>

    <?php include("includes/generic-footer.html"); ?>      

</body>
</HTML>