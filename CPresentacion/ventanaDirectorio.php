<!DOCTYPE html>
<HTML>
	<HEAD>
		<link rel="shortcut icon" href="../CPresentacion/imagenes/crc.png" />
		<TITLE>Directorio</TITLE>
		<SCRIPT SRC="../CPresentacion/ajax/jquery-3.3.1.min.js"></SCRIPT>
		<SCRIPT SRC="../CPresentacion/ajax/ajaxDirectorio.js" TYPE="text/javascript"></SCRIPT>
   		<script src="../CPresentacion/ajax/jquery.maskedinput-master/src/jquery.maskedinput.js" type="text/javascript"></script>

		<META http-equiv="Content-Type" CONTENT="text/html; charset=utf-8"/>

		<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->
		<link rel="stylesheet" href="../CPresentacion/css/estilos.css" href="">

		<LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/directorioCss.css"/>
		<LINK REL="stylesheet" type="text/css" href="../CPresentacion/css/fonts.css"/>

		<META NAME="viewport" CONTENT="width=device-width, initial-scale=1, maximum-scale=1"/>

	</HEAD>
	<BODY>
		
		<?php include("includes/generic-header.html"); ?>
			

			<div id="page" class="container">
				
				<!-- Aqui adentro deberan ir todos los elementos-->
				<DIV id = 'cabeceraPrincipal'>

					<P ID = 'tituloCabecera'>Secci&oacute;n de Directorio</P>	
					<P ID = 'encargado' CLASS = 'encargad'>Encargado</P>	
					
					<TABLE ID = 'opcionesMenuDirectorio'>
						<THEAD>
							<TR>
						 		<TH ID = 'agregar'><INPUT TYPE = 'BUTTON' ID = 'ventanaRegistrar' NAME = 'ventanaRegistrar' VALUE = 'Registrar' ONCLICK = "verVentanaRegistrar();"></TH>
							</TR>
						</THEAD>
					</TABLE>
				</DIV>


				<INPUT TYPE = 'TEXT' ID = 'buscar' MAXLENGTH = '40' SIZE = '40' TITLE = "Nombre, Apellido, Instituci&oacute;n es lo que busca" ONKEYUP = "campoVacio()" ONKEYDOWN = "detectarTeclaEnter_enBusqueda(event);" PLACEHOLDER = "Buscar integrante"></INPUT>

				<DIV style = "position: relative;overflow: auto;">
						
						<TABLE ID = "tabla">
							
							<THEAD>
								<TR>
						            <TH>Nombre</TH>
						            <TH>Primer Apellido:</TH>
						            <TH>Segundo Apellido:</TH>
						            <!--<TH>Telefono</TH>-->
						            <TH>Instituci&oacute;n Representada:</TH>	
						            <!--<TH >Opciones</TH>-->
								</TR>	
							</THEAD>

					        <TR>

					        <TBODY ID = 'tablaDirectorio'></TBODY>


						</TABLE>	

					</DIV>
					        <NAV>
					        	<UL ID= "paginacion" CLASS= "paginacion" style='list-style-type: none;'>
					        		<LI CLASS = "page-item" ID = 'paginaAnterior'>
					        			<A CLASS = "page-link" ID = 'primeraPagina' ONCLICK = "cargarPreviaInformacion()">
					        				Anterior
					        			</A>
					        		</LI>

					        		<LI ID = 'pagination' CLASS = "page-item">
					        		</LI>
					        		<LI CLASS = "page-item" ID = 'siguientePagina'>
					        			<A CLASS = "page-link " ONCLICK = "cargarSiguienteInformacion()">
					        				Siguiente
					        			</A>
					        		</LI>
					        	</UL>

					        </NAV>

							<!-- The Modal -->
					<DIV id="myModal" class="modal">

					  <!-- Modal content -->
					 	<DIV class="modal-content">
						    <DIV class="modal-header">
						      <SPAN class="close">&times;</SPAN>
					            <H2><P ID="cedulaDetalle"></P></H2>
						    </DIV>
						    <DIV class="modal-body">
						    	<TABLE id = 'tablaxinformacionDetallada'>
						    		<TR>
						    			<TD>C&eacute;dula:
						    				<INPUT TYPE="text" NAME="cedulaActualizar" ID="cedulaActualizar" SIZE = "18" MAXLENGTH = "15" ONBLUR = "validarCedula('actualizarCampo')"PLACEHOLDER = "Campo no obligatorio"/>
						    				<FONT color=red /><LABEL id="lCedulaActualizar" class="asterisco"> *</LABEL></TH></TD>
						    		</TR>

						    		<TR>
						    			<TD>Nombre: 
						    				<INPUT TYPE="text" NAME="nombreActualizar" ID="nombreActualizar" SIZE = "30" MAXLENGTH = "30" ONBLUR = "validarCampos('actualizarCampo','nombre')" PLACEHOLDER = "Escriba su nombre"/>
						    				<FONT color=red /><LABEL id="lNombreActualizar" class="asterisco">*</LABEL></TH></TD>
						    		</TR>

									<TR>
										<TD>Apellidos:
											<INPUT TYPE="text" NAME="primer_apellidoActualizar" ID="primer_apellidoActualizar" SIZE = "30" MAXLENGTH = "30" ONBLUR = "validarCampos('actualizarCampo','apellido1')" PLACEHOLDER = "Escriba su primer apellido"/>
											<FONT color=red /><LABEL id="lApellido1Actualizar" class="asterisco">*</LABEL></TH>
											<INPUT TYPE="text" NAME="segundo_apellidoActualizar" ID="segundo_apellidoActualizar" SIZE = "30" MAXLENGTH = "30" ONBLUR = "validarCampos('actualizarCampo','apellido2')" PLACEHOLDER = "Escriba su segundo apellido"/>
											<FONT color=red /><LABEL id="lApellido2Actualizar" class="asterisco">*</LABEL></TH>
										</TD>
									</TR>

									<TR>
						    			<TD>Tel&eacute;fono: 
						    				<INPUT TYPE="text" NAME="telefonoActualizar" ID="telefonoActualizar" SIZE = "18" MAXLENGTH = "8" ONKEYPRESS = "return validacionNumerico(event)" ONBLUR = "validarCampos('actualizarCampo','telefono')" PLACEHOLDER = "Escriba su teléfono"/>
						    				<FONT color=red /><LABEL id="lTelefonoActualizar" class="asterisco">*</LABEL></TH>
						    				<INPUT TYPE ="button" NAME="botonTelefonoExtra" ID="botonTelefonoExtra" VALUE = "+" ONCLICK = "insertarTelefonoExtra()"></TD>	
						    		</TR>

						    		<TR ID = 'telefonoExtra' ALIGN = 'center' style = 'display: none;'>
						    			<TD>Tel&eacute;fono Extra:
						    				<INPUT TYPE="text" NAME="segundoTelefonoActualizar" ID="segundoTelefonoActualizar" SIZE = "8" MAXLENGTH = "8" ONKEYPRESS = "return validacionNumerico(event)" ONBLUR = "validarCampos('actualizarCampo','telefonoExtra')"/>
						    				<FONT color=red /><LABEL id="lTelefonoExtraActualizar" class="asterisco">*</LABEL></TH>
						    			</TD>
						    		</TR>

						    		<TR>
						    			<TD>Correo electr&oacute;nico:
						    				<INPUT TYPE="text" NAME="actualizarCorreo" ID="actualizarCorreo" SIZE = "25" MAXLENGTH = "30" ONBLUR = "validarCampos('actualizarCampo','actualizarCorreo')" PLACEHOLDER = "Escriba su correo electrónico"/>
						    				<FONT color=red /><LABEL id="lActualizarCorreo" class="asterisco"> *</LABEL></TH>
						    			</TD>
						    		</TR>

						    		<TR>
						    			<TD>Instituci&oacute;n:<INPUT TYPE="text" NAME="institucionRepresentadaActualizar" ID="institucionRepresentadaActualizar" MAXLENGTH = "40" SIZE = "40" ONBLUR = "validarCampos('actualizarCampo','institucion')" PLACEHOLDER = "Escriba su institución o empresa"/>
						    			<FONT color=red /><LABEL id="lInstitucionActualizar" class="asterisco">*</LABEL></TH></TD>
						    		</TR>

						    		<TR>
						    			<TD>Puesto:
						    				 <SELECT ID = "seleccionarPuestoActualizar" NAME = "seleccionarPuestoActualizar" ONBLUR = "validarCampos('actualizarCampo','puesto')">
												<OPTION VALUE = "" SELECTED DISABLED>Seleccionar puesto</OPTION>
												<OPTION VALUE = "Propietario" ID = "Propietario" NAME  = "Propietario">Propietario</OPTION>
												<OPTION VALUE = "Asistente" ID = "Asistente" NAME = "Asistente">Asistente</OPTION>
											</SELECT>
											<FONT color=red /><LABEL id="lPuestoActualizar" class="asterisco">*</LABEL></TH>
						    			</TD>
						    		</TR>

						    		<TR>
						    			<TD>Comit&eacute;:
						    				<SELECT ID = "seleccionarComiteActualizar" NAME = "seleccionarComiteActualizar" ONCLICK = "agregarNuevoComite()" ONCHANGE = "agregarNuevoComite()" ONBLUR = "validarCampos('actualizarCampo','comite')">
											</SELECT>
											<FONT color=red /><LABEL id="lComiteActualizar" class="asterisco">*</LABEL></TH>
						    			</TD>
						    		</TR>

						    		<TR hidden><TD><INPUT TYPE = 'text' ID = 'idActualizar' NAME = 'idActualizar'></TD></TR>

						    	</TABLE>

						    </DIV>

						    <DIV class="modal-footer">
						      <INPUT TYPE = 'BUTTON' ID = 'actualizarIntegrante' NAME = 'actualizarIntegrante' VALUE = 'Actualizar' ONCLICK = "actualizarIntegrante()">
						      <INPUT TYPE = 'BUTTON' ID = 'cancelarActualizacion' NAME = 'cancelarActualizacion' VALUE = 'Cancelar' ONCLICK = 'cancelarActualizacion()'>
						    </DIV>
					  	</DIV>
					</DIV>

					<DIV id="modalRegistrar" class="modalRegistro">

					  <!-- Modal content -->
					 	<DIV class="modal-contentRegistrar">
						    <DIV class="modal-headerRegistrar">
						      	<SPAN class="closeRegistrar">&times;</SPAN>
					            <H2><P ID = "titutloRegistrar">Registrar Integrante</P></H2>
						    </DIV>
						    <DIV class="modal-bodyRegistar">
						    	<TABLE id = 'tablaxRegistrar'>

						    		<TR>
						    			<TD>C&eacute;dula:
						    				<INPUT TYPE="text" NAME="cedula" ID="cedula" SIZE = "18" MAXLENGTH = "15" ONBLUR = "validarCedula('registrarCampo')" PLACEHOLDER = "Campo no obligatorio"/>
						    				<FONT color=red /><LABEL id="lCedula" class="asterisco"> *</LABEL></TH></TD>
						    		</TR>

						    		<TR>
						    			<TD>Nombre: 
						    				<INPUT TYPE="text" NAME="nombre" ID="nombre" SIZE = "30" MAXLENGTH = "30" ONBLUR = "validarCampos('registrarCampo','nombre')" PLACEHOLDER = "Escriba su nombre"/>
						    				<FONT color=red /><LABEL id="lNombre" class="asterisco"> *</LABEL></TH></TD>
						    		</TR>

									<TR>
										<TD>Apellidos:
											<INPUT TYPE="text" NAME="primer_apellido" ID="primer_apellido" SIZE = "30" MAXLENGTH = "30" ONBLUR = "validarCampos('registrarCampo','apellido1')" PLACEHOLDER = "Escriba su primer apellido"/>
											<FONT color=red /><LABEL id="lApellido1" class="asterisco"> *</LABEL></TH>
											<INPUT TYPE="text" NAME="segundo_apellido" ID="segundo_apellido" SIZE = "30" MAXLENGTH = "30" ONBLUR = "validarCampos('registrarCampo','apellido2')" PLACEHOLDER = "Escriba su segundo apellido"/>
											<FONT color=red /><LABEL id="lApellido2" class="asterisco"> *</LABEL></TH>
										</TD>
									</TR>

									<TR>
						    			<TD>Tel&eacute;fono: 
						    				<INPUT TYPE="text" NAME="telefono" ID="telefono" SIZE = "15" MAXLENGTH = "8" ONKEYPRESS = "return validacionNumerico(event)" ONBLUR = "validarCampos('registrarCampo','telefono')" PLACEHOLDER = "Escriba su teléfono"/>
						    				<FONT color=red /><LABEL id="lTelefono" class="asterisco"> *</LABEL></TH>
						    				<INPUT TYPE ="button" NAME="botonTelefonoExtra" ID="botonTelefonoExtra" VALUE = "+" ONCLICK = "insertarTelefonoExtra('registrar')"></TD>	
						    		</TR>

						    		<TR ID = 'telefonoExtraRegistrar' style = 'display: none;' ALIGN = 'center'>
						    			<TD>Tel&eacute;fono Extra:
						    				<INPUT TYPE="text" NAME="segundoTelefono" ID="segundoTelefono" SIZE = "15" MAXLENGTH = "8" ONKEYPRESS = "return validacionNumerico(event)" ONBLUR = "validarCampos('registrarCampo','segundoTelefono')"/>
						    				<FONT color=red /><LABEL id="lSegundoTelefono" class="asterisco"> *</LABEL></TH>
						    			</TD>
						    		</TR>

						    		<TR>
						    			<TD>Correo electr&oacute;nico:
						    				<INPUT TYPE="text" NAME="correo" ID="correo" SIZE = "25" MAXLENGTH = "30" ONBLUR = "validarCampos('registrarCampo','correo')" PLACEHOLDER = "Escriba su correo electrónico"/>
						    				<FONT color=red /><LABEL id="lCorreo" class="asterisco"> *</LABEL></TH>
						    			</TD>
						    		</TR>

						    		<TR>
						    			<TD>Instituci&oacute;n:<INPUT TYPE="text" NAME="institucionRepresentada" ID="institucionRepresentada" MAXLENGTH = "40" SIZE = "40" ONBLUR = "validarCampos('registrarCampo','institucion')" PLACEHOLDER = "Escriba su institución o empresa"/>
						    			<FONT color=red /><LABEL id="lInstitucion" class="asterisco"> *</LABEL></TH></TD>
						    		</TR>

						    		<TR>
						    			<TD>Puesto:
						    				 <SELECT ID = "seleccionarPuesto" NAME = "seleccionarPuesto" ONBLUR = "validarCampos('registrarCampo','puesto')">
												<OPTION VALUE = "" SELECTED DISABLED>Seleccionar puesto</OPTION>
												<OPTION VALUE = "Propietario" ID = "Propietario" NAME  = "Propietario">Propietario</OPTION>
												<OPTION VALUE = "Asistente" ID = "Asistente" NAME = "Asistente">Asistente</OPTION>
											</SELECT>
											<FONT color=red /><LABEL id="lPuesto" class="asterisco"> *</LABEL></TH></TD>
						    			</TD>
						    		</TR>

						    		<TR>
						    			<TD>Comit&eacute;:
						    				<SELECT ID = "seleccionarComite" NAME = "seleccionarComite" ONCLICK = "agregarNuevoComite()" ONCHANGE = "agregarNuevoComite()" ONBLUR = "validarCampos('registrarCampo','comite')">
											</SELECT>
											<FONT color=red /><LABEL id="lComite" class="asterisco"> *</LABEL></TH>
						    			</TD>
						    		</TR>

						    	</TABLE>

						    </DIV>
						    <DIV class="modal-footerRegistrar">
						     	<INPUT TYPE = 'BUTTON' ID = 'registrarIntegrante' NAME = 'registrarIntegrante' VALUE = 'Registrar' ONCLICK = 'registrarIntegrante()'>
						      	<INPUT TYPE = 'BUTTON' ID = 'cancelarRegistro' NAME = 'cancelarRegistro' VALUE = 'Cancelar' ONCLICK = 'cancelarRegistro()'>
						    </DIV>
					  	</DIV>
					</DIV>

			</div>

		<div id="footer">	
			<p>Pagina en desarrollo para el Comite Municipal de Emergencias de Sarapiqui</p>
			<p>Elaborado en el curso de Ingenieria de Software II UNA</p>
		</div>


		<DIV id="modalConfirmacion" class="modalConfirmacion">

		  <!-- Modal content -->
		 	<DIV class="modal-contentConfirmacion">
			    <DIV class="modal-headerConfirmacion">
			      	<SPAN class="closeConfirmacion">&times;</SPAN>
			    </DIV>
		    	<DIV class="modal-bodyConfirmacion">		    
			    	<P ID = "confirmacion"></P>
			    </DIV>
			    <DIV class="modal-footerConfirmacion">
  		    	 	<INPUT TYPE = 'BUTTON' ID = 'botonConfirmacion' NAME = 'botonConfirmacion' VALUE = 'Cerrar' ONCLICK = 'botonConfirmacion()'>
		    	</DIV>
			</DIV>
		</DIV>

	<?php include("includes/generic-footer.html"); ?>		

	</BODY>
</HTML>