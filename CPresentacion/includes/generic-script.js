/* Alternar entre colocar y quitar la clase "responsive" al navbar cuando el usuario hace click en ella */

function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";  /*  Dejar siempre ese espacio en blanco, as�: " responsive"   */
  } else {
    x.className = "topnav";
  }
}