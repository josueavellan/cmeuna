	<?php
	include '../CDatos/consultasBodega.php';  // Donde estan las consultas.
	require '../CDominio/bodega.php';  // Objeto bodega.

	if (isset($_POST['registrar'])) {

		// Se obtienen los datos.
		$nombre 	 = $_POST['nombre'];
		$descripcion = $_POST['nombre'];
		$direccion 	 = $_POST['direccion'];
		$responsable = $_POST['responsable'];

		// Se crea el objeto bodega.
		$bodega =new bodega(0, 
							$nombre, 
							$descripcion, 
							$direccion, 
							$responsable);

		// Se envia a la BD.
		$consulta = new consultasBodega();
		//$bodega->setResponsable($consulta->ObtenerIdResponablePorNombre($bodega->getResponsable()));

		$resultado = $consulta->Registrar($bodega);

		echo $resultado;
	} 

	if (isset($_POST['cargar'])) {

		$consultas = new consultasBodega();
		$resultado = $consultas->Cargar();

		echo $resultado;
	}

	if (isset($_POST['consultar'])) {

		$pagina    = $_POST['pagina'];
		$consultas = new consultasBodega(); // Instanciamos la clase consultas.
		$resultado = $consultas->Consultar($pagina);
		
		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
				
				$parrafo = "";

				if (strlen($resultado[$i]->getDescripcion()) < 50) {
					
					$parrafo = "<p>" . $resultado[$i]->getDescripcion() . "</p>";
				} else {

					$parrafo = "<p>" . substr($resultado[$i]->getDescripcion(), 0, 50) . "</p><p id='".($i+1)."' class='p'>" . 
					substr($resultado[$i]->getDescripcion(), 50, -1) . "</p><input type='button' id='btn".($i+1)."' value='Mas...' onclick='Mas(".($i+1).");' />";
				}

				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $parrafo . "</td>" .
		                "<td>" . $resultado[$i]->getDireccion() . "</td>" .
		                "<td>" . $resultado[$i]->getResponsable() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo 0;
	}

	if (isset($_POST['eliminar'])) {

		$consultas = new consultasBodega();
		$resultado = $consultas->Eliminar($_POST['eliminar']);

		echo $resultado;
	}

	if (isset($_POST['seleccionar'])) {

		$consultas = new consultasBodega();
		$resultado = $consultas->Seleccionar($_POST['seleccionar']);

		echo $resultado->toString();
	}

	if (isset($_POST['actualizar'])) {

		// Obtenemos los datos.
		$id = $_POST['id'];
		$nombre = $_POST['nombre'];
		$descripcion = $_POST['descripcion'];
		$direccion = $_POST['direccion'];
		$responsable = $_POST['responsable'];

		// Se crea el objeto bodega.
		$bodega =new bodega($id, 
							$nombre, 
							$descripcion, 
							$direccion, 
							$responsable);

		// Si todo esta correcto, se envia a la BD.
		$consulta = new consultasBodega();
		$bodega->setResponsable($consulta->ObtenerIdResponablePorNombre($bodega->getResponsable()));

		echo $consulta->Actualizar($bodega);
	}

	if (isset($_POST['buscarBodegaEspecifica'])) {

		$variable  = $_POST['buscarBodegaEspecifica'];
		$consultas = new consultasBodega(); // Instanciamos la clase consultas.
		$resultado = $consultas->buscarBodegaEspecifica($variable);
		
		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
				
				$parrafo = "";

				if (strlen($resultado[$i]->getDescripcion()) < 50) {
					
					$parrafo = "<p>" . $resultado[$i]->getDescripcion() . "</p>";
				} else {

					$parrafo = "<p>" . substr($resultado[$i]->getDescripcion(), 0, 50) . "</p><p id='".($i+1)."' class='p'>" . 
					substr($resultado[$i]->getDescripcion(), 50, -1) . "</p><input type='button' id='btn".($i+1)."' value='Mas...' onclick='Mas(".($i+1).");' />";
				}

				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $parrafo . "</td>" .
		                "<td>" . $resultado[$i]->getDireccion() . "</td>" .
		                "<td>" . $resultado[$i]->getResponsable() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}
			echo $datos;
		}
		echo $resultado;
	}

	if (isset($_POST['contarFilas'])) {
		
		$consulta = new consultasBodega();
		echo $consulta->contarFilas();
	}
?>