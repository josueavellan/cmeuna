<?php
	include '../CDatos/consultasAlbergues.php';  // Donde estan las consultas.
	require '../CDominio/albergue.php';  // Objeto albergue.

    if (isset($_POST['cargarDatosEncargado'])) {

		$consultas = new consultasAlbergues();

		echo $consultas->CargarDatosEncargados();

	} else if (isset($_POST['registrar'])) {

		$consulta = new consultasAlbergues();
		
		echo $consulta->Registrar(new albergue(0, $_POST['nombre'], $_POST['capacidad'], $_POST['encargado'], $_POST['localidad'], $_POST['servicios'], $_POST['telefono']));

	} else if (isset($_POST['consultar'])) {
		$pagina = $_POST['pagina'];
		$consultas = new consultasAlbergues();
		$resultado = $consultas->Consultar($pagina); 

		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
					
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getCapacidad() . "</td>" .
		                "<td>" . $resultado[$i]->getEncargado() . "</td>" .
		                "<td>" . $resultado[$i]->getLocalidad() . "</td>" .
		                "<td>" . $resultado[$i]->getServicios() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}
		}

		echo $datos; 

	} else if (isset($_POST['eliminar'])) {
		
		$consultas = new consultasAlbergues();

		echo $consultas->Eliminar($_POST['eliminar']);

	} else if (isset($_POST['seleccionar'])) {

		$consultas = new consultasAlbergues();
		$resultado = $consultas->Seleccionar($_POST['seleccionar']);
		echo $resultado->toString();

	} else if (isset($_POST['actualizar'])) {

		$consulta = new consultasAlbergues();

		echo $consulta->Actualizar(new albergue($_POST['id'], $_POST['nombre'], $_POST['capacidad'], $_POST['encargado'], $_POST['localidad'], $_POST['servicios'], $_POST['telefono']));

	} else if(isset($_POST['buscarAlbergueEspecifico'])){
		$consultas = new consultasAlbergues();
		$variable = $_POST['buscarAlbergueEspecifico'];
		$resultado = $consultas->buscarAlbergueEspecifico($variable);

		if ($resultado != 0) {

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) {
					
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getNombre() . "</td>" .
		                "<td>" . $resultado[$i]->getCapacidad() . "</td>" .
		                "<td>" . $resultado[$i]->getEncargado() . "</td>" .
		                "<td>" . $resultado[$i]->getLocalidad() . "</td>" .
		                "<td>" . $resultado[$i]->getServicios() . "</td>" .
		                "<td>" . $resultado[$i]->getTelefono() . "</td>" .
		                "<td>" .
		                "<button class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}
			echo $datos; 
		}
		echo $resultado;

	}
	else if(isset($_POST['contarFilas'])){
		$consulta = new consultasAlbergues();
		echo $consulta->contarFilas();
	}
?>