<?php
	include '../CDatos/consultasZonasRiesgo.php';  // Donde estan las consultas.
	require '../CDominio/zonaRiesgo.php';  // Objeto bodega.

	// Consultar informacion de la base de datos.
	if (isset($_POST['consultar'])) {
		$pagina = $_POST['pagina'];
		$consultas = new consultasZonasRiesgo(); // Instanciamos la clase consultas.
		$resultado = $consultas->Consultar($pagina); // Obtenemos todos los datos.

		if ($resultado != 0) { // Preguntamos si en la BD hay información.

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) { // Sea crea todas las filas de la tabla.
				
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getLugar() . "</td>" .
		                "<td>" . $resultado[$i]->getTipo() . "</td>" .
		                "<td>" . $resultado[$i]->getAlbergue() . "</td>" .
		                "<td>" .
		                "<button title='Editar información' class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button title='Eliminar' class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "<button title='Ver Mapa' class='btnMostrar' onclick = 'Mostrar(" . $resultado[$i]->getId() . ")'><i class='fas fa-map-marker-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo $resultado;

	}

	// Se obtiene los datos de Albergue para mostrar en el modal de reistrar.
	else if (isset($_POST['cargar'])) {

		$consultas = new consultasZonasRiesgo(); // Instanciamos la clase consulta.
		$resultado = $consultas->Cargar(); // Obtenemos todos los datos que vienen de la clase consulta.

        echo $resultado;

	}

	// Se registrara una neuva zona de riesgo.
	else if (isset($_POST['registrar'])) {

		// Se crea el objeto voluntario.
		$lugar    = $_POST['lugar'];
		$tipo     = $_POST['tipo'];
		$albergue = $_POST['albergue'];
		$latitud  = $_POST['latitud'];
		$longitud = $_POST['longitud'];

		// Creamos el objeto Zona de Riesgo.
		$zonaRiesgo = new zonaRiesgo(0, $lugar, $tipo, $albergue, $latitud, $longitud);

		// Si todo esta correcto, se envia a la BD.
		$consulta = new consultasZonasRiesgo();
		$resultado = $consulta->Registrar($zonaRiesgo); // Enviamos el objeto Zona de riesgo a consulta para que lo registre.

		echo $resultado; // Retornamos el resultado de la consulta, si se efectuo correctamente o no.

	} 
	
	//
	else if (isset($_POST['eliminar'])) {

		$consultas = new consultasZonasRiesgo(); // Instanciamos la clase consula.
		$resultado = $consultas->Eliminar($_POST['eliminar']); // Envimos por parametro el id y lo eliminamos de la base de datos.

        echo $resultado;

	} 
	
	// Seleccionamos la Zona de Riesgo que queremos modificar.
	else if (isset($_POST['seleccionar'])) {

		$consultas = new consultasZonasRiesgo(); // Instanciamos la clase consultas.
		$resultado = $consultas->Seleccionar($_POST['seleccionar']); // Enviamos por parametro el id.

		echo $resultado->toString(); // Retornamos toda la información de la Zona de Riesgo.

	}
	
	// Despues de seleccionar se actualiza la informacion.
	else if (isset($_POST['actualizar'])) {

		// Se crea el objeto bodega.
		$id 	  = $_POST['id'];
		$lugar 	  = $_POST['lugar'];
		$tipo 	  = $_POST['tipo'];
		$albergue = $_POST['albergue'];

		$zonaRiesgo = new zonaRiesgo($id, $lugar, $tipo, $albergue, 0, 0);

		// Si todo esta correcto, se envia a la BD.
		$consulta = new consultasZonasRiesgo(); // Se instancia la clase consuta.
		$resultado = $consulta->Actualizar($zonaRiesgo); // Se envia para ser modificado.

		echo $resultado;

	}

	else if(isset($_POST['buscarPuntoRiesgoEspecifico'])){
		$variable = $_POST['buscarPuntoRiesgoEspecifico'];
		$consultas = new consultasZonasRiesgo(); // Instanciamos la clase consultas.
		$resultado = $consultas->buscarPuntoRiesgoEspecifico($variable); // Obtenemos todos los datos.

		if ($resultado != 0) { // Preguntamos si en la BD hay información.

			$datos = "";

			for ($i = 0; $i < count($resultado); $i++) { // Sea crea todas las filas de la tabla.
				
				$datos.="<tr class = 'cla'>" .
		                "<td>" . ($i+1) . "</td>" .
		                "<td>" . $resultado[$i]->getLugar() . "</td>" .
		                "<td>" . $resultado[$i]->getTipo() . "</td>" .
		                "<td>" . $resultado[$i]->getAlbergue() . "</td>" .
		                "<td>" .
		                "<button title='Editar información' class='btnActualizar' onclick = 'Seleccionar(" . $resultado[$i]->getId() . ")'><i class='far fa-edit'></i></button>" .
		                "<button title='Eliminar' class='btnEliminar' onclick = 'ConfirmarEliminacion(" . $resultado[$i]->getId() . ")'><i class='fas fa-trash-alt'></i></button>" .
		                "<button title='Ver Mapa' class='btnMostrar' onclick = 'Mostrar(" . $resultado[$i]->getId() . ")'><i class='fas fa-map-marker-alt'></i></button>" .
		                "</td>" .
		                "</tr>";
			}

			echo $datos;
		}

		echo $resultado;
	}
	else if(isset($_POST['contarFilas'])){
		$consulta = new consultasZonasRiesgo();
		echo $consulta->contarFilas();
	}
?>