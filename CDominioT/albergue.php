<?php
namespace CDominioT;

	class albergue {

		private $id;
		private $nombre;
		private $capacidad;
		private $encargado;
		private $localidad;
		private $servicios;
		private $telefono;

		public function albergue($id, $nombre, $capacidad, $encargado, $localidad, $servicios, $telefono) {

			$this->id = $id;
			$this->nombre = $nombre;
			$this->capacidad = $capacidad;
			$this->encargado = $encargado;
			$this->localidad = $localidad;
			$this->servicios = $servicios;
			$this->telefono = $telefono;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}

		public function setNombre($nombre) {

			$this->nombre = $nombre;
		}

		public function setCapacidad($capacidad) {

			$this->capacidad = $capacidad;
		}

		public function setEncargado($encargado) {

			$this->encargado = $encargado;
		}

		public function setLocalidad($localidad) {

			$this->localidad = $localidad;
		}

		public function setServicios($servicios) {

			$this->servicios = $servicios;
		}

		public function setTelefono($telefono) {

			$this->telefono = $telefono;
		}


		// Gets...
		public function getId() {

			return $this->id;
		}

		public function getNombre() {

			return $this->nombre;
		}

		public function getCapacidad() {

			return $this->capacidad;
		}

		public function getEncargado() {

			return $this->encargado;
		}

		public function getLocalidad() {

			return $this->localidad;
		}

		public function getServicios() {

			return $this->servicios;
		}

		public function getTelefono() {

			return $this->telefono;
		}

		// toString...
		public function toString() {

			return $this->id."_".$this->nombre."_".$this->capacidad."_".$this->encargado."_".$this->localidad."_".$this->servicios."_".$this->telefono;
		}
	}
?>