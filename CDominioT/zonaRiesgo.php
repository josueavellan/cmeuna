<?php
namespace CDominioT;

	class zonaRiesgo {

		private $id;
		private $lugar;
		private $tipo;
		private $albergue;

		public function zonaRiesgo($id, $lugar, $tipo, $albergue) {

			$this->id = $id;
			$this->lugar = $lugar;
			$this->tipo = $tipo;
			$this->albergue = $albergue;
		}

		// Sets...
		public function setId($id) {

			$this->id = $id;
		}

		public function setLugar($lugar) {

			$this->lugar = $lugar;
		}

		public function setTipo($tipo) {

			$this->tipo = $tipo;
		}

		public function setAlbergue($albergue) {

			$this->albergue = $albergue;
		}

		// Gets...
		public function getId() {

			return $this->id;
		}

		public function getLugar() {

			return $this->lugar;
		}

		public function getTipo() {

			return $this->tipo;
		}

		public function getAlbergue() {

			return $this->albergue;
		}

		// toString...
		public function toString() {

			return $this->id."_".$this->lugar."_".$this->tipo."_".$this->albergue;
		}
	}
?>