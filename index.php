<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">

	<link rel="shortcut icon" href="CPresentacion/imagenes/crc.png" />
	<title>cme sarapiqu&iacute</title>
	<meta name: "viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<!--Bootstrap agregado mediante CDN -->

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous""></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--agregar el icono de barras del mení responsive-->


	<link rel="stylesheet" href="CPresentacion/css/estilos.css" href="">
	<script type="text/javascript" src="CPresentacion/ajax/theScript.js"></script>
	

</head>



<body>

	<?php include("includes/generic-header.html"); ?>




	<div class="container">  <!-- class="container-fluid"   con esto podemos hacer que abarque todo el contenido de la pantalla pero casi no se usa -->
		<section class="main row">

			<div id="carouselExampleIndicators" class="col-xs-12 col-sm-8 col-md-9 carousel slide" data-ride="carousel">
			  
			  <ol class="carousel-indicators">
			    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			  </ol>
			  
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img class="d-block w-100" src="CPresentacion/imagenes/carousel/1.jpg" alt="Primer slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="CPresentacion/imagenes/carousel/2.jpg" alt="Segundo slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="CPresentacion/imagenes/carousel/3.jpg" alt="Tercer slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="CPresentacion/imagenes/carousel/4.jpg" alt="Cuarto slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="CPresentacion/imagenes/carousel/5.jpg" alt="Quinto slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="CPresentacion/imagenes/carousel/6.jpg" alt="Sexto slide">
			    </div>
			  </div>
			  
			  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>



			

			<aside class="col-xs-12 col-sm-4 col-md-3">			
			    <div class="contain">
				    <div id="myCarousel" class="carousel slide" data-ride="carousel">
			            <!-- Indicators -->
			            <ol class="carousel-indicators">
			                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			                <li data-target="#myCarousel" data-slide-to="1"></li>
			                <li data-target="#myCarousel" data-slide-to="2"></li>
			                <li data-target="#myCarousel" data-slide-to="3"></li>
			            </ol>

			            <!-- Wrapper for slides -->
			            <div class="carousel-inner">
			                <div class="carousel-item active">
			                    <!--div class="fill" style=" background-color:#00FF00;">1</div-->
			                    <img class="d-block w-100" src="CPresentacion/imagenes/carousel/anuncio-1.jpg" alt="primer anuncio">
			                </div>
			                <div class="carousel-item">
			                    <div class="fill" style=" background-color:#ffff00;">2</div>
			                </div>
			                <div class="carousel-item">
			                    <div class="fill" style=" background-color:#FF0000;">3
			                     <br><p>Comisi&oacute;n Nacional de Emergencias para m&aacute;s info: <a href="https://cne.go.cr/" rel="nofollow">CNE 	Costa Rica</a>
			                    </div>
			                </div>
			                <div class="carousel-item">
			                    <div class="fill" style=" background-color:#C0C0C0;">4</div>
			                </div>
			            </div>
			        </div>
			    </div>		
			</aside>

			



		</section>

		<div class="row">

			<div class="color4 col-xs-12 col-sm-9 col-md-6">
				<p>
					<h3>Rese&ntilde;a CME</h3>
						<h6>COMIT&Eacute;S REGIONALES, MUNICIPALES Y COMUNALES DE EMERGENCIA</h6>
							
							<div class="color5">
								Comit&eacute;s regionales, municipales y comunales de emergencia son instancias permanentes de coordinaci&oacute;n en los niveles regional, municipal y comunal. Por medio de ellos, la CNE cumple su funci&oacute;n de coordinaci&oacute;n de las instituciones p&uacute;blicas, privadas, organismos no gubernamentales y la sociedad civil, que trabajan en la atenci&oacute;n de emergencias o desastres. Se integran con la representaci&oacute;n institucional o sectorial de los funcionarios con mayor autoridad en el nivel correspondiente. Las organizaciones no gubernamentales, las privadas, las municipales y comunales, definir&aacute;n su representaci&oacute;n por medio de la autoridad interna de cada una de ellas. 
							</div>						
				</p>

				<!--a href="#" class="boton">Leer m&aacute;s</a-->
				<button type="button" class="btn btn-info btn-sm">Leer m&aacute;s</button>

			</div>

			<div class="col-xs-12 col-sm-6 col-md-3">
				<p>
					<h3>&Aacute;rea de voluntariado</h3>
					<div>
						<img src="CPresentacion/imagenes/pic01.jpg" alt="" width="300">
					</div>
					<p class="color4">
						<br>Te gustaria formar parte de nuestros voluntarios?
					</p>

				</p>
			</div>

			<!--div class="clearfix visible-sm-block"></div-->  <!-- para ajustar las columnas pero ya lo hace por defecto solo que se alarga demasiado-->

			<div class="color2 col-xs-12 col-sm-6 col-md-3">   <!-- col-md-offset-3 SUPUESTAMENTE movia las columnas pero al parecer ya no lo hace-->
				<p>
					<h3>Consultar zonas de Riesgo</h3>
					<img src="CPresentacion/imagenes/pic02.jpg" alt="" width="250">
					<p class="color4">
						<br>Clasificaci&oacute;n de las mayores zonas de riesgo del cant&oacute;n.
					</p>
				</p>
			</div>

			
		
		</div>

		

		<div class="row">
			<div class="color3 col-xs-12">
				<p>
					<h3>Informaci&oacute;n General</h3>
					Lorem ipsum s simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
				</p>
			</div>
		</div>
	</div>
		


	<?php include("includes/generic-footer.html"); ?>

</body>


</html>