<?php 

	class conexion{

		private $servidor;
		private $usuario;
		private $contrasenia;
		private $db;
		private $conexion;	

		public function conexion(){
			$this->servidor = "localhost";
			$this->usuario = "root";
			$this->contrasenia = "";
			$this->db = "bdcme";
		}

		public function abrirConexion(){
			
			$this->conexion = new mysqli($this->servidor,$this->usuario,$this->contrasenia,$this->db);
			$this->conexion->set_charset("utf8");
			if($this->conexion->connect_error){

				die("error");

			}
			else{

				return $this->conexion;

			}

		}

		public function cerrarConexion(){

			mysqli_close($this->conexion);
		}

	}
?>