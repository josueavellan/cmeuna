<?php

	include 'conexion.php';

	class consultasInsumo {

		public function consultasInsumo() {}

		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "SELECT COUNT(idinsumo) FROM tbinsumo";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarInsumoEspecifico($dato){

			$conexion = new conexion();
			$resultado = $this->Query("SELECT * FROM tbinsumo WHERE articuloinsumo = '".$dato."' 
																	OR institucioninsumo = '".$dato."' 
																	OR cantidadinsumo = '".$dato."'
																	OR bodegainsumo = '".$dato."'
																	");
			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$id          = $row[0];
					$articulo    = $row[1];
					$institucion = $row[2];
					$cantidad    = $row[3];
					$observacion = $row[4];
					$bodega      = $row[5];

					$array[$i] = new insumo($id, $articulo, $institucion, $cantidad, $observacion, $bodega);
					$i++;
				}

				return $array;
			}

			return 0;
		}		

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();

			return $resultado;
		}

		function CargarDatosBodega() {
			
			$query = "SELECT nombrebodega FROM tbbodega";
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0]."+";
				}

				return $datos;
			} else {

				return 0;
			}
		}

		function OntenerIdBodegaPorNombre($nombreBodega) {

			$query = "SELECT idbodega FROM tbbodega WHERE nombrebodega='".$nombreBodega."'";
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return $row[0];
			} else {

				return 0;
			}
		}

		function Registrar($insumo) {

			$query ="INSERT INTO tbinsumo VALUES ('".$insumo->getId()."','"
													.$insumo->getArticulo()."','"
													.$insumo->getInstitucion()."','"
													.$insumo->getCantidad()."','"
													.$insumo->getObservacion()."','"
													.$insumo->getBodega()."')";

			$resultado = $this->Query($query);

			if ($resultado > 0) {

				return 1;
			} else {

				return 0;
			}
		}

		function ConsultarInsumos($pagina) {
			
			$resultado = $this->Query("SELECT idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega FROM tbinsumo INNER JOIN tbbodega ON tbinsumo.bodegainsumo = tbbodega.idbodega 
				ORDER BY idinsumo DESC LIMIT $pagina,6");

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$id          = $row[0];
					$articulo    = $row[1];
					$institucion = $row[2];
					$cantidad    = $row[3];
					$observacion = $row[4];
					$bodega      = $row[5];

					$array[$i] = new insumo($id, $articulo, $institucion, $cantidad, $observacion, $bodega);
					$i++;
				}

				return $array;
			}

			return 0;
		}

		function EliminarInsumo($id) {

			$query = "DELETE FROM tbinsumo WHERE idinsumo='".$id."'";
			$resultado = $this->Query($query);
			
			return $resultado;
		}

		function SeleccionarInsumo($id) {

			$query = "SELECT idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega FROM tbinsumo 
				INNER JOIN tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
				WHERE idinsumo='".$id."'";
			
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return $insumo = new insumo($row[0], $row[1],$row[2], $row[3], $row[4], $row[5]);
			} else {

				return 0;
			}
		}

		function Actualizar($insumo) {

			$query="UPDATE tbinsumo SET articuloinsumo='".$insumo->getArticulo()."',
										institucioninsumo='".$insumo->getInstitucion()."',
										cantidadinsumo='".$insumo->getCantidad()."',
										observacioninsumo='".$insumo->getObservacion()."',
										bodegainsumo='".$insumo->getBodega()."'
										WHERE idinsumo='".$insumo->getId()."'";

			$resultado = $this->Query($query);

			return $resultado; 
		}

		function BuscarInsumoPorBodega($idBodega) {

			$query = "SELECT idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
			FROM tbinsumo 
			INNER JOIN tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
			WHERE bodegainsumo = '".$idBodega."'";

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$datos = "";

			if (mysqli_num_rows($resultado) > 0) {

				while ($row = mysqli_fetch_row($resultado)) {

					$id          = $row[0];
					$articulo    = $row[1];
					$institucion = $row[2];
					$cantidad    = $row[3];
					$observacion = $row[4];
					$bodega      = $row[5];

					$datos.=$id."-".
							$articulo."-".
							$institucion."-".
							$cantidad."-".
							$observacion."-".
							$bodega."+";
				}
			} else {

				$datos = "null";
			}
			return $datos;
		}
	}
?>