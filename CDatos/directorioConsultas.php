<?php
	
	include '../CDatos/conexion.php';
	include  '../CDominio/directorio.php';

	class directorioConsultas{

		public function directorioConsultas(){}


		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "SELECT COUNT(idIntegrante) FROM tbdirectorio";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		//REALIZA LA BUSQUEDA DE LOS COMITES REGISTRADOS PARA EL SELECT
		function buscarComite(){

			$conexion = new conexion();
			$respuesta = "";

			$buscarComite = "SELECT DISTINCT comiteIntegrante FROM tbdirectorio";
			$resultado = mysqli_query($conexion->abrirConexion(), $buscarComite);

			if(mysqli_num_rows($resultado) > 0){
				while($columna = mysqli_fetch_row($resultado)){
					$respuesta.= $columna[0].",";
				}
			}
			$conexion->cerrarConexion();
			return $respuesta;
		}

		//REALIZA LA BUSQUEDA CON DATOS COMO : NOMBRE,APELLIDO1,APELLIDO2,INSTITUCION
		function buscarIntegranteEspecifico($dato){

			$respuesta = "";
			$conexion = new conexion();

			//SI ES UN DATO COMO: DENNIS,MUNOZ, EL DETECTA QUE SON DOS VARIABLES QUE DEBE BUSCAR PARA FILTRAR
			if(strpos($dato, ",") == true){
				$datoxBuscar = explode(",",$dato);
				$datoxBuscar[1] = trim($datoxBuscar[1],'*');
				$tamaniodebusqueda = count($datoxBuscar);
				
				//REALIZA BUSQUEDAS ENTRE DOS VARIABLES POSIBLES
				if($tamaniodebusqueda == 2){


					$buscarIntegranteEspecifico = "SELECT * FROM tbdirectorio WHERE nombreIntegrante = '".$datoxBuscar[0]."' AND apellido1Integrante = '".$datoxBuscar[1]."' OR 
																					nombreIntegrante= '".$datoxBuscar[0]."' AND apellido2Integrante = '".$datoxBuscar[1]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND institucionrepresentadaIntegrante = '".$datoxBuscar[1]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND comiteIntegrante = '".$datoxBuscar[1]."' OR
																					apellido1Integrante= '".$datoxBuscar[0]."' AND apellido2Integrante = '".$datoxBuscar[1]."' OR
																					institucionrepresentadaIntegrante= '".$datoxBuscar[0]."' AND comiteIntegrante= '".$datoxBuscar[1]."'";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarIntegranteEspecifico);

					if(mysqli_num_rows($resultado) > 0){
						while($row = mysqli_fetch_row($resultado)){
							$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."+";
						}
					}
					$conexion->cerrarConexion();
					
				}
				//REALIZA BUSQUEDAS ENTRE TRES VARIABLES POSIBLES
				else if($tamaniodebusqueda == 3){
					$buscarIntegranteEspecifico = "SELECT * FROM tbdirectorio WHERE nombreIntegrante= '".$datoxBuscar[0]."' AND apellido1Integrante = '".$datoxBuscar[1]."' AND 																																	apellido2Integrante = '".$datoxBuscar[2]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND apellido2Integrante = '".$datoxBuscar[1]."' AND
																															apellido1Integrante = '".$datoxBuscar[2]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND apellido1Integrante = '".$datoxBuscar[1]."' 																		AND institucionrepresentadaIntegrante = '".$datoxBuscar[2]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND apellido2Integrante = '".$datoxBuscar[1]."' 																		AND institucionrepresentadaIntegrante = '".$datoxBuscar[2]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND apellido1Integrante= '".$datoxBuscar[1]."' 
																															AND comiteIntegrante= '".$datoxBuscar[2]."' OR
																					apellido1Integrante= '".$datoxBuscar[0]."' AND apellido2Integrante = '".$datoxBuscar[1]."' AND 
																															nombreIntegrante = '".$datoxBuscar[2]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND institucionrepresentadaIntegrante= '".$datoxBuscar[1]."'
																															AND comiteIntegrante= '".$datoxBuscar[2]."'";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarIntegranteEspecifico);

					if(mysqli_num_rows($resultado) > 0){
						while($row = mysqli_fetch_row($resultado)){
							$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."+";
						}
					}					
					$conexion->cerrarConexion();
				}
				//REALIZA BUSQUEDAS ENTRE 4 VARIABLES POSIBLES
				else if($tamaniodebusqueda == 4){
					$buscarIntegranteEspecifico = "SELECT * FROM tbdirectorio WHERE nombreIntegrante= '".$datoxBuscar[0]."' AND apellido1Integrante = '".$datoxBuscar[1]."' AND 																							apellido2Integrante = '".$datoxBuscar[2]."' AND institucionrepresentadaIntegrante = '".$datoxBuscar[3]."' OR
																					nombreIntegrante= '".$datoxBuscar[0]."' AND apellido1Integrante= '".$datoxBuscar[1]."' AND
																						institucionrepresentadaIntegrante= '".$datoxBuscar[2]."' AND comiteIntegrante= '".$datoxBuscar[3]."'";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarIntegranteEspecifico);

					if(mysqli_num_rows($resultado) > 0){
						while($row = mysqli_fetch_row($resultado)){
							$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8]."+";
						}
					}					
					$conexion->cerrarConexion();

				}
			}
			//SI EL DATO NO CONTIENE LA O LAS COMA(S) REALIZA UNA BUSQUEDA CON UNA UNICA VARIABLE
			else{
				$buscarIntegranteEspecifico = "SELECT * FROM tbdirectorio WHERE idIntegrante = '".$dato."' OR
																				nombreIntegrante = '".$dato."' OR
																				apellido1Integrante = '".$dato."' OR
																				apellido2Integrante = '".$dato."' OR
																				institucionrepresentadaIntegrante = '".$dato."' OR
																				cedulaIntegrante = '".$dato."' OR
																				comiteIntegrante = '".$dato."'";

				$resultado = mysqli_query($conexion->abrirConexion(), $buscarIntegranteEspecifico);

				if(mysqli_num_rows($resultado) > 0){
					while($row = mysqli_fetch_row($resultado)){
						$respuesta .= $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8].",".$row[9];
					}
				}

				$conexion->cerrarConexion();

			}

			return $respuesta;
		}

		function eliminarIntegrante($id){

			$respuesta = "";
			$conexion = new conexion();

			$actualizarIntegrante = "DELETE FROM tbdirectorio WHERE idIntegrante = '".$id."'";

			$resultado = mysqli_query($conexion->abrirConexion(), $actualizarIntegrante);
			if($resultado != 0){
				$respuesta = 1;
			}
			$conexion->cerrarConexion();

			return $respuesta;

		}

		function devolverIntegrante($id){

			$conexion = new conexion();
			$respuesta = "";

			$buscarIntegrante = "SELECT * FROM tbdirectorio WHERE  idIntegrante = '".$id."'";

			$resultado = mysqli_query($conexion->abrirConexion(), $buscarIntegrante);
			$conexion->cerrarConexion();

			if(mysqli_num_rows($resultado) > 0){

				while($row = mysqli_fetch_row($resultado)){

					$id = $row[0];
					$cedula = $row[1];
					$nombre = $row[2];
					$apellido1 = $row[3];
					$apellido2 = $row[4];
					$telefono = $row[5];
					$institucion = $row[6];
					$puesto = $row[7];
					$comite = $row[8];

					$respuesta .= $id.",".
								  $cedula.",".
								  $nombre.",".
								  $apellido1.",".
								  $apellido2.",".
								  $telefono.",".
								  $institucion.",".
								  $puesto.",".
								  $comite."+";

				}
			}

			return $respuesta;

		}

		function actualizarIntegrante($id,$directorio){

			$respuesta = "";
			$conexion = new conexion();

			$actualizarIntegrante = "UPDATE tbdirectorio SET 
															cedulaIntegrante = '".$directorio->getCedulaIntegranteD()."',
															nombreIntegrante = '".$directorio->getNombreIntegranteD()."',
															apellido1Integrante = '".$directorio->getApellido1IntegranteD()."',
															apellido2Integrante = '".$directorio->getApellido2IntegranteD()."',
															telefonoIntegrante = '".$directorio->getTelefonoIntegranteD()."',
															institucionrepresentadaIntegrante = '".$directorio->getInstitucionIntegranteD()."',
															puestoIntegrante = '".$directorio->getPuestoIntegranteD()."',
															comiteIntegrante = '".$directorio->getComiteIntegranteD()."',
															correoIntegrante = '".$directorio->getCorreoIntegranteD()."'
															WHERE  idIntegrante = '".$id."'";

			$resultado = mysqli_query($conexion->abrirConexion(), $actualizarIntegrante);
			if($resultado != 0){
				$respuesta = 1;
			}
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarIntegrantes($pagina){

			$conexion = new conexion();	
			$respuesta = "";

			$buscarIntegrante = "SELECT * FROM tbdirectorio ORDER BY idIntegrante DESC LIMIT $pagina,6";

			$resultado = mysqli_query($conexion->abrirConexion(), $buscarIntegrante);
			$conexion->cerrarConexion();

			if(mysqli_num_rows($resultado) > 0){

				while($row = mysqli_fetch_row($resultado)){

					$id = $row[0];
					$cedula = $row[1];
					$nombre = $row[2];
					$apellido1 = $row[3];
					$apellido2 = $row[4];
					$telefono = $row[5];
					$institucion = $row[6];
					$puesto = $row[7];
					$comite = $row[8];
					$correo = $row[9];

					$respuesta .= $id.",".
								  $cedula.",".
								  $nombre.",".
								  $apellido1.",".
								  $apellido2.",".
								  $telefono.",".
								  $institucion.",".
								  $puesto.",".
								  $comite.",".
								  $correo."+";

				}
			}

			return $respuesta;

		}

		function insertarIntegrante($cedula,$nombre,$apellido1,$apellido2,$telefono,$institucion,$puesto, $comite, $correo){

			$conexion = new conexion();
			$respuesta = 0;

			$buscarUltimoIntegrante = "SELECT MAX(idIntegrante) FROM tbdirectorio";

			$resultado = mysqli_query($conexion->abrirConexion(),$buscarUltimoIntegrante);
			$conexion->cerrarConexion();
	       
	        if ($row = mysqli_fetch_row($resultado)) {
	            $respuesta = trim($row[0]) + 1;
	        }   

			$registrarIntegrante = "INSERT INTO tbdirectorio VALUES (
			'".$respuesta."',
			'".$cedula."',
			'".$nombre."',
			'".$apellido1."',
			'".$apellido2."',
			'".$telefono."',
			'".$institucion."',
			'".$puesto."',
			'".$comite."',
			'".$correo."')";

			$resultado2 = mysqli_query($conexion->abrirConexion(), $registrarIntegrante);	
			$conexion->cerrarConexion();

			return $resultado2;
		}

	}

?>