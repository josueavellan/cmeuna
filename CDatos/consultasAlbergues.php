<?php

	include 'conexion.php';

	class consultasAlbergues {

		public function consultasAlbergues() {}

		//REALIZA EL CONTENIDO DE LAS FILAS
		function contarFilas(){
			$conexion = new conexion();
			$respuesta = "";

			$contarFilas = "SELECT COUNT(idalbergue) FROM tbalbergues";
			$resultado = mysqli_query($conexion->abrirConexion(),$contarFilas);
			if(mysqli_num_rows($resultado) > 0)
				while($row = mysqli_fetch_row($resultado))
					$respuesta = $row[0];
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarAlbergueEspecifico($dato){

			$resultado = $this->Query("SELECT * FROM tbalbergues WHERE nombrealbergue = '".$dato."' 
																	OR capacidadalbergue = '".$dato."' 
																	OR localidadalbergue = '".$dato."'
																	");

			if(mysqli_num_rows($resultado) > 0){

				$i = 0;
				$array = array();

				while($row = mysqli_fetch_row($resultado)){
					$array[$i] = new albergue($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
					$i++;
				}
				return $array;
			}
			

			return 0;
		}

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();

			return $resultado;
		}

		function CargarDatosEncargados() {
			
			$resultado = $this->Query("SELECT nombreresponsable FROM tbresponsable");

			if (mysqli_num_rows($resultado) > 0) {

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0]."+";
				}

				return $datos;
			}

			return 0;
		}

		function OntenerIdResponsablePorNombre($nombreresponsable) {

			$resultado = $this->Query("SELECT idresponsable FROM tbresponsable WHERE nombreresponsable='".$nombreresponsable."'");
			
			if (mysqli_num_rows($resultado) > 0) {

				$idresponsable = 0;

				while ($row = mysqli_fetch_row($resultado)) {

					$idresponsable = $row[0];
				}

				return $idresponsable;
			}

			return 0;
		}

		function Registrar($albergue) {

			$idresponsable = $this->OntenerIdResponsablePorNombre($albergue->getEncargado());
			return $this->Query("INSERT INTO tbalbergues VALUES ('".$albergue->getId()."','"
																		.$albergue->getNombre()."','"
																		.$albergue->getCapacidad()."','"
																		.$idresponsable."','"
																		.$albergue->getLocalidad()."','"
																		.$albergue->getServicios()."','"
																		.$albergue->getTelefono()."')");
		}

		function Consultar($pagina) {
			
			$resultado = $this->Query("SELECT idalbergue, nombrealbergue, capacidadalbergue, nombreresponsable, localidadalbergue, serviciosalbergue, telefonoalbergue FROM tbalbergues 
				INNER JOIN tbresponsable ON tbalbergues.encargadoalbergue = tbresponsable.idresponsable
				ORDER BY idalbergue DESC LIMIT $pagina,6");
			

			if (mysqli_num_rows($resultado) > 0) {

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) {

					$array[$i] = new albergue($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
					$i++;
				}

				return $array;
			}

			return 0;
		}

		function Eliminar($id) {

			return $this->Query("DELETE FROM tbalbergues WHERE idalbergue='".$id."'");
		}

		function Seleccionar($id) {

			$resultado = $this->Query("SELECT idalbergue, nombrealbergue, capacidadalbergue, nombreresponsable, localidadalbergue, serviciosalbergue, telefonoalbergue FROM tbalbergues 
				INNER JOIN tbresponsable on tbalbergues.encargadoalbergue = tbresponsable.idresponsable
				WHERE idalbergue='".$id."'");

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				return new albergue($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
			}

			return $datos;
		}

		function Actualizar($albergue) {
			
			$idEncargado = $this->OntenerIdResponsablePorNombre($albergue->getEncargado());

			return $this->Query("UPDATE tbalbergues SET nombrealbergue='".$albergue->getNombre()."',
													capacidadalbergue='".$albergue->getCapacidad()."',
													encargadoalbergue='".$idEncargado."',
													localidadalbergue='".$albergue->getLocalidad()."',
													serviciosalbergue='".$albergue->getServicios()."',
													telefonoalbergue='".$albergue->getTelefono()."'
													WHERE idalbergue='".$albergue->getId()."'");
		}
	}
?>