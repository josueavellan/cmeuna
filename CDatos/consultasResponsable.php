<?php

	include 'conexion.php';

	class consultasResponsable{

		public function consultasResponsable() {}

		function contarFilas() {

			// Creamos la conexion.
			$conexion = new conexion();

			// Creamos la consulta SQL.
			$contarFilas = "SELECT COUNT(idresponsable) FROM tbresponsable";

			// Ejecutamos la peticion.
			$resultado = mysqli_query($conexion->abrirConexion(), $contarFilas);

			if (mysqli_num_rows($resultado) > 0) {

				while ($row = mysqli_fetch_row($resultado)) {

					$respuesta = $row[0];
				}
			}
			// Cerramos la conexion.
			$conexion->cerrarConexion();

			return $respuesta;
		}

		function buscarResponsableEspecifico($dato) {

			$respuesta = "";
			$conexion = new conexion();

			//SI ES UN DATO COMO: DENNIS,MUNOZ, EL DETECTA QUE SON DOS VARIABLES QUE DEBE BUSCAR PARA FILTRAR
			if (strpos($dato, ",") == true) {

				$datoxBuscar = explode(",",$dato);
				$datoxBuscar[1] = trim($datoxBuscar[1],'*');
				$tamaniodebusqueda = count($datoxBuscar);
				
				//REALIZA BUSQUEDAS ENTRE DOS VARIABLES POSIBLES
				if ($tamaniodebusqueda == 2) {

					// Se crea la consulta MySQL.
					$buscarResponsableEspecifico = "SELECT * FROM tbresponsable WHERE 
						nombreresponsable = '".$datoxBuscar[0]."' AND apellido1responsable = '".$datoxBuscar[1]."' OR 
						nombreresponsable= '".$datoxBuscar[0]."' AND apellido2responsable = '".$datoxBuscar[1]."' OR
						apellido1responsable= '".$datoxBuscar[0]."' AND apellido2responsable = '".$datoxBuscar[1]."'";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarResponsableEspecifico);

					if (mysqli_num_rows($resultado) > 0) {

						$i = 0;
						$array = array();

						while ($row = mysqli_fetch_row($resultado)) {

							$array[$i] = new responsable($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
							$i++;
						}

						return $array;
					}

					return 0;
					
				}
				//REALIZA BUSQUEDAS ENTRE TRES VARIABLES POSIBLES
				else if ($tamaniodebusqueda == 3) {

					$buscarResponsableEspecifico = "SELECT * FROM tbresponsable WHERE 
					nombreresponsable= '".$datoxBuscar[0]."' AND apellido1responsable = '".$datoxBuscar[1]."' AND		apellido2responsable = '".$datoxBuscar[2]."' OR
					nombreresponsable= '".$datoxBuscar[0]."' AND apellido2responsable = '".$datoxBuscar[1]."' AND
					apellido1responsable = '".$datoxBuscar[2]."' OR
					apellido1responsable= '".$datoxBuscar[0]."' AND apellido2responsable = '".$datoxBuscar[1]."' AND 
					nombreresponsable = '".$datoxBuscar[2]."'";
					$resultado = mysqli_query($conexion->abrirConexion(),$buscarResponsableEspecifico);

					if (mysqli_num_rows($resultado) > 0) {

						$i = 0;
						$array = array();

						while ($row = mysqli_fetch_row($resultado)) {

							$array[$i] = new responsable($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
							$i++;
						}

						return $array;
					}

					return 0;
				}
			}
			//SI EL DATO NO CONTIENE LA O LAS COMA(S) REALIZA UNA BUSQUEDA CON UNA UNICA VARIABLE
			else {
				$buscarResponsableEspecifico = "SELECT * FROM tbresponsable WHERE nombreresponsable = '".$dato."' OR
																				apellido1responsable = '".$dato."' OR
																				apellido2responsable = '".$dato."' OR
																				cedularesponsable = '".$dato."'";
				$resultado = mysqli_query($conexion->abrirConexion(),$buscarResponsableEspecifico);
				if (mysqli_num_rows($resultado) > 0) {

					$i = 0;
					$array = array();

					while ($row = mysqli_fetch_row($resultado)) {

						$array[$i] = new responsable($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
						$i++;
					}

					return $array;
				}

				return 0;
			}
		}

		function Query($query) {

			$conexion = new conexion();
			$resultado = mysqli_query($conexion->abrirConexion(), $query);
			$conexion->cerrarConexion();
			
			return $resultado;
		}

		function VerificarCedulaRepetida($id, $cedula) {

			// Creamos la consulta MySQL.
			$query = "SELECT idresponsable, cedularesponsable FROM tbresponsable WHERE cedularesponsable='".$cedula."'";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$row = mysqli_fetch_row($resultado);

				if ($row[0] == $id) {

					return 1; // Cedula repetidad. Pero es la misma de el.
				}

				return 0; // Ya hay una cedula repetida.
			} 

			return 1; // No hay cedula repetidad.
		}

		function RegistrarResponsable($responsable) {

			// Primero se registra que no este repetida la cedula.
			if ($responsable->getCedula() != "") {

				if ($this->VerificarCedulaRepetida($responsable->getId(), $responsable->getCedula()) == 0) {

					return 2; // Cedula repetida.
				}
			}

			echo $this->Query("INSERT INTO tbresponsable VALUES ('".$responsable->getId()."','".$responsable->getCedula()."','".$responsable->getNombre()."','".$responsable->getApellido1()."','".$responsable->getApellido2()."','".$responsable->getTelefono()."')");
		}

		function ConsultarResponsables($pagina) {
			
			// Creamos la consulta MySQL.
			//$query = "SELECT * FROM tbresponsable ORDER BY idresponsable DESC LIMIT $pagina, 6";
			$query = "call consultar_tabla_Responsable('".$pagina."');";
			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) { // Exito!

				$i = 0;
				$array = array();

				while ($row = mysqli_fetch_row($resultado)) { // Recorrido por c/fila.

					// Obtenemos los datos.
					$id = $row[0];
					$cedula = $row[1];
					$nombre = $row[2];
					$apellido1 = $row[3];
					$apellido2 = $row[4];
					$telefono = $row[5];

					// Creamos el responsable.
					$responsable = new responsable($id, 
												   $cedula, 
												   $nombre, 
												   $apellido1, 
												   $apellido2, 
												   $telefono);

					// Añadimos el responsable al vector.
					$array[$i] = $responsable;
					$i++;
				}

				return $array;
			}

			return 0;
		}

		function EliminarResponsable($id) {

			// Creamos la Consulta MySQL.
			//$query = "DELETE FROM tbresponsable WHERE idresponsable='".$id."'";
			$query = "call eliminar_un_Responsable('".$id."');";
			
			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			return $resultado;
		}

		function Seleccionar($id) {

			// Creamos la consulta MySQL.
			$query = "SELECT * FROM tbresponsable WHERE idresponsable='".$id."'";

			// Ejecutar la peticion.
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				$datos = "";

				while ($row = mysqli_fetch_row($resultado)) {

					$datos.=$row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5];
				}

				return $datos;
			}

			return 0;
		}

		function ActualizarResponsable($responsable) {

			// Primero se registra que no este repetida la cedula.
			if ($responsable->getCedula() != "") {

				if ($this->VerificarCedulaRepetida($responsable->getId(), $responsable->getCedula()) == 0) {

					return 2; // Cedula repetida.
				}
			}

			return $this->Query("UPDATE tbresponsable SET cedularesponsable='".$responsable->getCedula()."',nombreresponsable='"   .$responsable->getNombre()."',apellido1responsable='".$responsable->getApellido1()."',apellido2responsable='".$responsable->getApellido2()."',telefonoresponsable='" .$responsable->getTelefono()."'WHERE idresponsable='" .$responsable->getId()."'");
		}

		function ConsultarSiTieneLlaveFornea($id) {
			
			// Crear a consulta MySQL.
			$query = "SELECT * FROM tbresponsable INNER JOIN tbbodega ON tbresponsable.idresponsable = tbbodega.responsablebodegaWHERE idresponsable = '".$id."'";

			// Ejecutamos la peticion.
			$resultado = $this->Query($query);

			if (mysqli_num_rows($resultado) > 0) {

				return 1;
			}

			return 0;
		}
	}
?>