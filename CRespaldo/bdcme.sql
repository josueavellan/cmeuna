-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-03-2019 a las 07:58:41
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdcme`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_una_bodega` (IN `_id` INT, `_nombrebodega` VARCHAR(30), `_descripcionbodega` VARCHAR(100), `_direccionbodega` VARCHAR(100), `_responsablebodega` INT(5))  begin
	update tbbodega 
	set 
		nombrebodega = _nombrebodega,
		descripcionbodega = _descripcionbodega,
		direccionbodega = _direccionbodega,
        responsablebodega = _responsablebodega
	where idbodega = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_una_ZonaRiesgo` (IN `_id` INT, `_lugar` VARCHAR(100), `_tipo_riesgo` VARCHAR(30), `_albergue_cercano` VARCHAR(100))  begin
	update tbzonariesgo 
	set 
		lugarzonariesgo = _lugar,
		tipozonariesgo = _tipo_riesgo,
		alberguezonariesgo = _albergue_cercano
	where idzonariesgo = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_un_Albergue` (IN `_id` INT, `_nombrealbergue` VARCHAR(30), `_capacidadalbergue` INT(4), `_encargadoalbergue` VARCHAR(100), `_localidadalbergue` VARCHAR(100), `_serviciosalbergue` INT(3), `_telefonoalbergue` INT(11))  begin
	set @id_responsable := (select idresponsable from tbresponsable where nombreresponsable = _encargadoalbergue);
	update tbalbergues 
	set 
		nombrealbergue = _nombrealbergue,
        capacidadalbergue = _capacidadalbergue,
        encargadoalbergue = @id_responsable,
        localidadalbergue = _localidadalbergue,
        serviciosalbergue = _serviciosalbergue,
        telefonoalbergue = _telefonoalbergue
	where idalbergue = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_un_Insumos` (IN `_id` INT, `_articuloinsumo` VARCHAR(30), `_institucioninsumo` VARCHAR(30), `_cantidadinsumo` VARCHAR(5), `_observacioninsumo` VARCHAR(100), `_bodegainsumo` INT(5))  begin
	update tbinsumo 
	set 
		articuloinsumo = _articuloinsumo, 
        institucioninsumo = _institucioninsumo, 
        cantidadinsumo = _cantidadinsumo,
        observacioninsumo = _observacioninsumo,
        bodegainsumo = _bodegainsumo
	where idvoluntario = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_un_Responsable` (IN `_id` INT, `_cedularesponsable` VARCHAR(11), `_nombreresponsable` VARCHAR(11), `_apellido1responsable` VARCHAR(30), `_apellido2responsable` VARCHAR(30), `_telefonoresponsable` VARCHAR(11))  begin
	update tbresponsable 
	set 
		cedularesponsable = _cedularesponsable,
		nombreresponsable = _nombreresponsable,
		apellido1responsable = _apellido1responsable,
		apellido2responsable = _apellido2responsable,
		telefonoresponsable = _telefonoresponsable
	where idresponsable = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_un_Voluntario` (IN `_id` INT, `_nombrevoluntario` VARCHAR(30), `_telefonovoluntario` VARCHAR(11), `_detallevoluntario` VARCHAR(100))  begin
	update tbvoluntario 
	set 
		nombrevoluntario = _nombrevoluntario,
		telefonovoluntario = _telefonovoluntario,
		detallevoluntario = _detallevoluntario
	where idvoluntario = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_especifico_Albergue` (IN `_dato` VARCHAR(100))  begin
	select * from tbalbergues where nombrealbergue = _dato or capacidadalbergue = _dato or localidadalbergue = _dato;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_especifico_Insumo` (IN `_dato` VARCHAR(100))  begin
	select * from tbinsumo where articuloinsumo = _dato
							or institucioninsumo = _dato
							or cantidadinsumo = _dato
							or bodegainsumo = _dato;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_integrante_especifico_Bodega` (IN `_dato` VARCHAR(100))  begin
    select * from tbbodega where responsablebodega = _dato or direccionbodega = _dato or nombrebodega = _dato;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_integrante_especifico_Voluntario` (IN `_dato` VARCHAR(100))  begin
	select * from tbvoluntario where nombrevoluntario = _dato or telefonovoluntario = _dato;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_por_bodega_un_Insumo` (IN `_id` INT(5))  begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
		from tbinsumo 
		inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
		where bodegainsumo = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `cargar_datos_bodega_en_Insumos` ()  begin
	select nombrebodega from tbbodega;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `cargar_nombres_responsables_Albergue` ()  begin
	select nombreresponsable from tbresponsable;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_nombres_albergues` ()  begin
	select nombrealbergue from tbalbergues;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_tabla_Albergue` (IN `_pagina` INT(10))  begin
	select idalbergue, nombrealbergue, capacidadalbergue, nombreresponsable, localidadalbergue, serviciosalbergue, telefonoalbergue FROM tbalbergues 
				INNER JOIN tbresponsable ON tbalbergues.encargadoalbergue = tbresponsable.idresponsable
				ORDER BY idalbergue DESC LIMIT _pagina, 6;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_tabla_Bodega` (IN `_pagina` INT(10))  begin
	/*select * from tbbodega order by idbodega desc limit _pagina, 6;*/
    select idbodega, nombrebodega, descripcionbodega, direccionbodega, nombreresponsable 
		from tbbodega inner join tbresponsable on tbbodega.responsablebodega = tbresponsable.idresponsable
        where idbodega != 1;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_tabla_Insumos` (IN `_pagina` INT(10))  begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
		from tbinsumo inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega 
        order by idinsumo desc limit _pagina,6;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_tabla_Responsable` (IN `_pagina` INT(10))  begin
	select * from tbresponsable where idresponsable != 0 order by idresponsable desc limit _pagina, 6;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_tabla_Voluntario` (IN `_pagina` INT(10))  begin
	select * from tbvoluntario order by idvoluntario desc limit _pagina, 6;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_tabla_ZonaRiesgo` ()  begin
	select * from tbzonariesgo;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_una_Bodega` (IN `_id` INT)  begin
	select * from tbbodega where idbodega = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_una_ZonaRiesgo` (IN `_id` INT)  begin
	select * from tbzonariesgo where idzonariesgo = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_un_Insumos` (IN `_id` INT)  begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega 
		from tbinsumo 
		inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
		where idinsumo = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_un_Responsable` (IN `_id` INT)  begin
	select * from tbresponsable where idresponsable = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_un_Voluntario` (IN `_id` INT)  begin
	select * from tbvoluntario where idvoluntario = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `contar_filas_Albergue` ()  begin
	select count(idalbergue) from tbalbergues;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `contar_filas_Bodegas` ()  begin
	select count(idbodega) from tbbodega;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `contar_filas_Insumos` ()  begin
	select count(idinsumo) from tbinsumo;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `contar_filas_Responsable` ()  begin
	select count(idresponsable) from tbresponsable;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `contar_filas_Voluntario` ()  begin
	select count(idvoluntario) from tbvoluntario;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_albergues_de_zonas_de_riesgo` (IN `_id` INT(10))  begin
    set @nombre := (select nombrealbergue from tbalbergues where idalbergue = _id);
	update tbzonariesgo set alberguezonariesgo = 'Sin definir'
    where alberguezonariesgo = @nombre;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_tabla_Albergue` ()  begin
	delete from tbalbergues;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_tabla_Insumos` ()  begin
	delete from tbinsumo;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_tabla_Responsable` ()  begin
	delete from tbresponsable;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_tabla_Voluntario` ()  begin
	delete from tbzonariesgo;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_tabla_ZonaRiesgo` ()  begin
	delete from tbzonariesgo;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_una_Bodega` (IN `_id` INT)  begin
    update tbinsumo set bodegainsumo = 1 where bodegainsumo = _id;
    delete from tbbodega where idbodega = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_una_ZonaRiesgo` (IN `id` INT)  begin
	delete from tbzonariesgo where idzonariesgo = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_un_Albergue` (IN `_id` INT)  begin
    set @nombre := (select nombrealbergue from tbalbergues where idalbergue = _id);
	update tbzonariesgo set alberguezonariesgo = 'Sin definir' where alberguezonariesgo = @nombre;
    delete from tbalbergues where idalbergue = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_un_Insumos` (IN `_id` INT)  begin
	delete from tbinsumo where idinsumo = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_un_Responsable` (IN `_id` INT)  begin
	update tbbodega set responsablebodega = 0 where responsablebodega = _id;
	delete from tbresponsable where idresponsable = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_un_Voluntario` (IN `id` INT)  begin
	delete from tbvoluntario where idvoluntario = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_una_Bodega` (IN `_nombreBodega` VARCHAR(30), `_descripcionBodega` VARCHAR(100), `_direccionBodega` VARCHAR(100), `_responsableBodega` VARCHAR(30))  begin
	insert into tbbodega (
		nombrebodega, 
        descripcionbodega,
        direccionbodega,
        responsablebodega
	) values (
		_nombreBodega,
		_descripcionBodega, 
		_direccionBodega,
        _responsableBodega
	);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_una_ZonaRiesgo` (IN `_lugar` VARCHAR(100), `_tipo_riesgo` VARCHAR(30), `_albergue_cercano` VARCHAR(100), `_latitud` DOUBLE, `_longitud` DOUBLE)  begin
	insert into tbzonariesgo (
		lugarzonariesgo, 
        tipozonariesgo, 
        alberguezonariesgo, 
        latitud, 
        longitud)
        values (_lugar, _tipo_riesgo, _albergue_cercano, _latitud, _longitud);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_un_Albergue` (IN `_nombrealbergue` VARCHAR(30), `_capacidadalbergue` INT(4), `_encargadoalbergue` INT(5), `_localidadalbergue` VARCHAR(100), `_serviciosalbergue` INT(3), `_telefonoalbergue` INT(11))  begin
	insert into tbalbergues (
		nombrealbergue, 
        capacidadalbergue, 
        encargadoalbergue,
        localidadalbergue.
        serviciosalbergue,
        telefonoalbergue
	) values (
		_nombrealbergue, 
        _capacidadalbergue, 
        _encargadoalbergue,
        _localidadalbergue.
        _serviciosalbergue,
        _telefonoalbergue
	);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_un_Insumos` (IN `_articuloinsumo` VARCHAR(30), `_institucioninsumo` VARCHAR(11), `_cantidadinsumo` VARCHAR(5), `_observacioninsumo` VARCHAR(100), `_bodegainsumo` INT(5))  begin
	insert into tbinsumo (
		articuloinsumo, 
        institucioninsumo, 
        cantidadinsumo,
        observacioninsumo,
        bodegainsumo
	) values (
		_articuloinsumo,
		_institucioninsumo, 
		_cantidadinsumo,
        _observacioninsumo,
        _bodegainsumo
	);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_un_Responsable` (IN `_cedularesponsable` VARCHAR(11), `_nombreresponsable` VARCHAR(11), `_apellido1responsable` VARCHAR(30), `_apellido2responsable` VARCHAR(30), `_telefonoresponsable` VARCHAR(11))  begin
	insert into tbresponsable (
	cedularesponsable,
    nombreresponsable,
    apellido1responsable,
    apellido2responsable,
    telefonoresponsable
	) values (
	_cedularesponsable,
    _nombreresponsable,
    _apellido1responsable,
    _apellido2responsable,
    _telefonoresponsable
	);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_un_Voluntario` (IN `_nombrevoluntario` VARCHAR(30), `_telefonovoluntario` VARCHAR(11), `_detallevoluntario` VARCHAR(100))  begin
	insert into tbvoluntario (
		nombrevoluntario, 
        telefonovoluntario, 
        detallevoluntario
	) values (
		_nombrevoluntario,
		_telefonovoluntario, 
		_detallevoluntario
	);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_id_bodega_por_nombre_de_Insumos` (IN `_nombre` VARCHAR(100))  begin
	select idbodega from tbbodega where nombrebodega =_nombre;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_id_responsable_por_nombre` (IN `_nombre` VARCHAR(100))  begin
	select idresponsable from tbresponsable where nombreresponsable = _nombre;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_id_responsable_por_nombre_Albergue` (IN `_nombre` VARCHAR(30))  begin
	select idresponsable from tbresponsable where nombreresponsable =_nombre;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_nombres_responsables_de_bodega` ()  begin
	select nombreresponsable from tbresponsable;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_nombre_responsable_por_id_bodega` (IN `_id` VARCHAR(5))  begin
	select nombreresponsable from tbresponsable where idresponsable = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `si_tiene_llave_foranea_Bodega` (IN `_id` VARCHAR(5))  begin
	select * from tbbodega inner join tbinsumo on tbinsumo.bodegainsumo = tbbodega.idbodega where idbodega = _id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verificar_cedula_repetida_Responsable` (IN `_id` INT, `_cedula` VARCHAR(11))  begin
	select idresponsable, cedularesponsable from tbresponsable where cedularesponsable = _cedula;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbalbergues`
--

CREATE TABLE `tbalbergues` (
  `idalbergue` int(5) NOT NULL,
  `nombrealbergue` varchar(30) NOT NULL,
  `capacidadalbergue` int(4) DEFAULT NULL,
  `encargadoalbergue` int(5) DEFAULT NULL,
  `localidadalbergue` varchar(100) NOT NULL,
  `serviciosalbergue` int(3) DEFAULT NULL,
  `telefonoalbergue` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbalbergues`
--

INSERT INTO `tbalbergues` (`idalbergue`, `nombrealbergue`, `capacidadalbergue`, `encargadoalbergue`, `localidadalbergue`, `serviciosalbergue`, `telefonoalbergue`) VALUES
(6, 'aaaaaa', 0, 0, '', 0, 11111111),
(7, 'bbbbbbbbbbbbbbbb', 0, 0, '', 0, 33333333),
(8, 'ASADA', 100, 0, 'La victoria', 1, 12312312),
(9, 'a', 0, 0, 'a', 0, 31231231),
(10, 'a', 0, 0, 'a', 0, 0),
(11, 'a', 0, 0, 'a', 3, 12),
(12, 'qwqeqw', 0, 0, 'werwe', 0, 0),
(13, 'eewewe', 21312312, 0, 'wddsqs', 1, 0),
(14, 'ewiefbwe', 31231, 0, 'efiejbf', 0, 1312312),
(15, 'weqew', 231231, 0, 'weqwe', 0, 231231),
(16, '2132', 231231, 3, '123123', 1, 23123),
(17, 'wqeqweqwe', 312123, 0, 'eqweqwe', 0, 12312312),
(18, 'weqweqwe', 1312312, 0, 'weqweqwe', 0, 23321),
(19, 'qewweqwe', 0, 0, '', 0, 0),
(21, 'nombre2', 100, 0, 'localidad', 0, 86755409);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbbodega`
--

CREATE TABLE `tbbodega` (
  `idbodega` int(5) NOT NULL,
  `nombrebodega` varchar(30) NOT NULL,
  `descripcionbodega` varchar(100) NOT NULL,
  `direccionbodega` varchar(100) NOT NULL,
  `responsablebodega` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbbodega`
--

INSERT INTO `tbbodega` (`idbodega`, `nombrebodega`, `descripcionbodega`, `direccionbodega`, `responsablebodega`) VALUES
(1, 'Sin definir', '', '', '0'),
(12, 'dadasds', 'dadasds', 'dasdasd', '3'),
(13, 'q', 'qqqq', 'qqqq', '3'),
(14, 'aaa', 'aaa', 'aaa', '3'),
(15, 'bbb', 'bbb', 'bbb', '0'),
(16, 'ccc', 'ccc', 'ccc', '0'),
(17, 'ddd', 'ddd', 'ddd', '3'),
(18, 'eee', 'eee', 'eee', '3'),
(19, 'eee', 'eee', 'eee', '2'),
(20, 'nombre', 'descripcion', 'direccion', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbdirectorio`
--

CREATE TABLE `tbdirectorio` (
  `idIntegrante` int(5) NOT NULL,
  `cedulaIntegrante` varchar(20) NOT NULL,
  `nombreIntegrante` varchar(30) NOT NULL,
  `apellido1Integrante` varchar(30) NOT NULL,
  `apellido2Integrante` varchar(30) DEFAULT NULL,
  `telefonoIntegrante` varchar(18) DEFAULT NULL,
  `institucionrepresentadaIntegrante` varchar(50) DEFAULT NULL,
  `puestoIntegrante` varchar(30) NOT NULL,
  `comiteIntegrante` varchar(50) DEFAULT NULL,
  `correoIntegrante` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbdirectorio`
--

INSERT INTO `tbdirectorio` (`idIntegrante`, `cedulaIntegrante`, `nombreIntegrante`, `apellido1Integrante`, `apellido2Integrante`, `telefonoIntegrante`, `institucionrepresentadaIntegrante`, `puestoIntegrante`, `comiteIntegrante`, `correoIntegrante`) VALUES
(1, '123456789', 'Default', 'Default', 'Default', '11122233', 'Default', 'Propietario', 'Municipal de Emergencias', ''),
(2, '123456789', 'Default', 'Default', 'Default', '11122244', 'Default', 'Asistente', 'Ejecutivo de Emergencias', ''),
(6, '701930053', 'Eithel ', 'Trigueros', 'Rodríguez', '88841320', 'UNA', 'Asistente', 'Cubujuquí', ''),
(7, '115950406', 'Dennis', 'Muñoz', 'Azofeifa', '83838522', 'UNA', 'Propietario', 'Rio Frio', ''),
(8, '702520355', 'Yazmín', 'Jiménez ', 'Guerrero', '84081630', 'Banco Nacional', 'Propietario', 'Ninguno', ''),
(9, '115950328', 'Michael', 'Salas', 'Granados', '87542165', 'Design Soft', 'Propietario', 'Ninguno', ''),
(10, '115950407', 'Dennis', 'Muñoz', 'Madrigal', '27643713/89467505', 'Ninguna', 'Propietario', 'Ninguno', ''),
(11, '603750305', 'Josue', 'Avellan', 'Alvarado', '86186343', 'FUNDECOR', 'Asistente', 'Rio Frio', ''),
(12, '115950408', 'Juan', 'Granados', 'Acuña', '27643718', 'INDER', 'Asistente', 'Municipal de Emergencias', ''),
(13, '207824689', 'Ana', 'Rodriguez', 'Alfaro', '86547821', 'MAG', 'Asistente', 'Municipal de Emergencias', ''),
(14, '308541287', 'Priscila', 'Montoya', 'Aguilar', '85745123', 'ASADA', 'Propietario', 'Municipal de Emergencias', ''),
(15, '607389175', 'Alexa', 'Salazar', 'Picado', '20145789', 'MEP', 'Asistente', 'Ejecutivo de Emergencias', ''),
(16, '687924571', 'Joel', 'Alfaro', 'Ortiz', '85764923', 'FUNDECOR', 'Asistente', 'Municipal de Emergencias', ''),
(17, '705893115', 'Sofia', 'Aguilar', 'Morales', '60524841', 'CNE', 'Asistente', 'Municipal de Emergencias', ''),
(18, '504712846', 'Ramon', 'Baltodano', 'Ramirez', '86432812', 'Bomberos CR', 'Asistente', 'Municipal de Emergencias', ''),
(19, '604784125', 'Marilyn', 'De Los Angeles', 'Marin', '85974512', 'MEP', 'Propietario', 'Ejecutivo de Emergencias', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinsumo`
--

CREATE TABLE `tbinsumo` (
  `idinsumo` int(5) NOT NULL,
  `articuloinsumo` varchar(30) NOT NULL,
  `institucioninsumo` varchar(30) NOT NULL,
  `cantidadinsumo` varchar(5) DEFAULT NULL,
  `observacioninsumo` varchar(100) DEFAULT NULL,
  `bodegainsumo` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbinsumo`
--

INSERT INTO `tbinsumo` (`idinsumo`, `articuloinsumo`, `institucioninsumo`, `cantidadinsumo`, `observacioninsumo`, `bodegainsumo`) VALUES
(10, '1', '', '1', '', 1),
(13, 'camas', '', '10', '', 1),
(14, 'colchones', 'Sarapiqui', '100', 'ebfoEWBFilwejbi', 1),
(15, 'colver', 'efweifwej ', '2312', 'wdwf', 1),
(16, '131233', '23123123', '12312', '2312312', 1),
(17, '11', '3213123', '23123', '123132', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpersona`
--

CREATE TABLE `tbpersona` (
  `personaid` int(5) NOT NULL,
  `personacedula` varchar(9) NOT NULL,
  `personanombre` varchar(30) NOT NULL,
  `personaapellido1` varchar(30) NOT NULL,
  `personaapellido2` varchar(30) NOT NULL,
  `personacargo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbresponsable`
--

CREATE TABLE `tbresponsable` (
  `idresponsable` int(5) NOT NULL,
  `cedularesponsable` varchar(11) NOT NULL,
  `nombreresponsable` varchar(30) NOT NULL,
  `apellido1responsable` varchar(30) NOT NULL,
  `apellido2responsable` varchar(30) NOT NULL,
  `telefonoresponsable` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbresponsable`
--

INSERT INTO `tbresponsable` (`idresponsable`, `cedularesponsable`, `nombreresponsable`, `apellido1responsable`, `apellido2responsable`, `telefonoresponsable`) VALUES
(0, '', 'Sin definir', '', '', ''),
(2, '1-1595-0328', 'Michaell', 'Salas', 'Granados', '86-75-54-06'),
(3, '1-1595-0406', 'Dennis', 'Muñoz', 'Azofeifa', '83-83-85-22'),
(13, '1-1111-1111', 'Luiz', 'Guillermo', 'Sanabria', '12-34-56-78'),
(14, '13456789', 'Josue', 'Avellan', 'Alvarado', '85858471'),
(15, '98765432', 'Rachel', 'Bolivar', 'Solano', '84745896'),
(16, '74185296', 'Gerberth ', 'Aleman', 'Fonseca', '87542163'),
(17, '96635285', 'Ileana', 'Schmidt', 'Fonseca', '857415263'),
(18, '96857478', 'Carlos', 'Escalante', 'Solano', '96855241'),
(19, '63524174', 'Wilberth', 'Rodriguez', 'Recino', '32658754'),
(20, '63524174', 'Steven', 'Cruz', 'Sancho', '84659887'),
(21, '321654714', 'Cristian', 'Brenes', 'Granados', '84635241'),
(22, '6587544212', 'Eithel', 'Trigueros', 'Rodriguez', '54184653');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbvehiculo`
--

CREATE TABLE `tbvehiculo` (
  `idVehiculo` int(11) NOT NULL,
  `placaVehiculo` varchar(15) NOT NULL,
  `tipoVehiculo` varchar(30) NOT NULL,
  `tipocombustibleVehiculo` varchar(30) NOT NULL,
  `capacidadVehiculo` int(11) NOT NULL,
  `descripcionVehiculo` varchar(100) NOT NULL,
  `institucionrepresentadaVehiculo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbvehiculo`
--

INSERT INTO `tbvehiculo` (`idVehiculo`, `placaVehiculo`, `tipoVehiculo`, `tipocombustibleVehiculo`, `capacidadVehiculo`, `descripcionVehiculo`, `institucionrepresentadaVehiculo`) VALUES
(1, 'default', 'default', 'default', 0, 'default', 'default'),
(2, 'default', 'default', 'default', 0, 'default', 'default'),
(3, '123456', 'Pick Up', 'Diesel', 4, 'Color blanco y de cajón', 'UNA'),
(4, 'A123', 'Automovil', 'Gasolina', 4, 'Color azul de 4 puertas', 'Fundecor'),
(5, 'A321', 'Buseta', 'Gaseoso', 12, 'Azul con logo de la UNA', 'UNA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbvoluntario`
--

CREATE TABLE `tbvoluntario` (
  `idvoluntario` int(5) NOT NULL,
  `nombrevoluntario` varchar(30) DEFAULT NULL,
  `telefonovoluntario` varchar(11) DEFAULT NULL,
  `detallevoluntario` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbvoluntario`
--

INSERT INTO `tbvoluntario` (`idvoluntario`, `nombrevoluntario`, `telefonovoluntario`, `detallevoluntario`) VALUES
(11, 'Luis', '22-22-22-22', 'Estoy desempleado, asi que tengo tiempo libre'),
(12, 'Pedro', '11-22-33-44', 'detalle  '),
(13, 'Carlos', '84524163', 'algo del mundo quiero hacer'),
(14, 'Maria', '85966352', 'quiero ayudar mi comunidad'),
(15, 'Jorge', '74859663', 'me gustar participar en actividades sociales'),
(16, 'Emily', '85526341', 'siempre quise ayudar ante las emergencias'),
(17, 'Diego', '85966352', 'quiero vincular mi empresa con esta acción'),
(18, 'Alexandra', '85749663', 'quiero ayudar con la parte de atención de personas'),
(19, 'Kevin', '85965210', 'quiero aportar mi colaboración en los albergues con actividades formativas'),
(20, 'Yazmin', '85206374', 'soy lider de un grupo de scouts que podrían apoyar'),
(21, 'Esteban', '87986554', 'mi empresa puede aportar víveres '),
(22, 'Nicole', '85417496', 'Podría ayudar con el papeleo ante las emergencias'),
(23, 'Kenneth', '85301245', 'soy bueno en las redes y divulgando informacion'),
(24, 'Paula', '84868281', 'soy experta en actividades lúdicas para los niños en albergues');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbzonariesgo`
--

CREATE TABLE `tbzonariesgo` (
  `idzonariesgo` int(5) NOT NULL,
  `lugarzonariesgo` varchar(100) DEFAULT NULL,
  `tipozonariesgo` varchar(30) DEFAULT NULL,
  `alberguezonariesgo` varchar(30) DEFAULT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbzonariesgo`
--

INSERT INTO `tbzonariesgo` (`idzonariesgo`, `lugarzonariesgo`, `tipozonariesgo`, `alberguezonariesgo`, `latitud`, `longitud`) VALUES
(18, 'Casa de Michael', 'Inundacion', 'ASADA', 10.3228914, -83.9126362),
(19, 'Casa de Avellan', 'Sequia', 'Sin definir', 10.320309, -83.906806),
(21, 'Casa de Dennis', 'Caida de meteorito', 'Sin definir', 10.321221, -83.913768),
(22, 'Universidad', 'Lluvia acida', 'Sin definir', 10.3189586, -83.92290849999999),
(23, 'prueba', 'adasas', 'aaaaaa', 10.324337, -83.925004),
(24, 'AAAAAa', 'aaa', 'Sin definir', 10.318960299999999, -83.92290679999999),
(25, 'zzzzzz', 'zzzzzzz', 'Sin definir', 10.321203667633291, -83.92549840327149),
(29, 'Carenalga', 'destello', 'Sin definir', 10.445450383447593, -84.04731461166989),
(30, 'a', 'a', 'Sin definir', 10.4456192, -84.0433664);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbalbergues`
--
ALTER TABLE `tbalbergues`
  ADD PRIMARY KEY (`idalbergue`);

--
-- Indices de la tabla `tbbodega`
--
ALTER TABLE `tbbodega`
  ADD PRIMARY KEY (`idbodega`);

--
-- Indices de la tabla `tbdirectorio`
--
ALTER TABLE `tbdirectorio`
  ADD PRIMARY KEY (`idIntegrante`);

--
-- Indices de la tabla `tbinsumo`
--
ALTER TABLE `tbinsumo`
  ADD PRIMARY KEY (`idinsumo`),
  ADD KEY `bodegainsumo` (`bodegainsumo`);

--
-- Indices de la tabla `tbpersona`
--
ALTER TABLE `tbpersona`
  ADD PRIMARY KEY (`personaid`);

--
-- Indices de la tabla `tbresponsable`
--
ALTER TABLE `tbresponsable`
  ADD PRIMARY KEY (`idresponsable`);

--
-- Indices de la tabla `tbvehiculo`
--
ALTER TABLE `tbvehiculo`
  ADD PRIMARY KEY (`idVehiculo`);

--
-- Indices de la tabla `tbvoluntario`
--
ALTER TABLE `tbvoluntario`
  ADD PRIMARY KEY (`idvoluntario`);

--
-- Indices de la tabla `tbzonariesgo`
--
ALTER TABLE `tbzonariesgo`
  ADD PRIMARY KEY (`idzonariesgo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbalbergues`
--
ALTER TABLE `tbalbergues`
  MODIFY `idalbergue` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `tbbodega`
--
ALTER TABLE `tbbodega`
  MODIFY `idbodega` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `tbinsumo`
--
ALTER TABLE `tbinsumo`
  MODIFY `idinsumo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `tbpersona`
--
ALTER TABLE `tbpersona`
  MODIFY `personaid` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbresponsable`
--
ALTER TABLE `tbresponsable`
  MODIFY `idresponsable` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `tbvoluntario`
--
ALTER TABLE `tbvoluntario`
  MODIFY `idvoluntario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `tbzonariesgo`
--
ALTER TABLE `tbzonariesgo`
  MODIFY `idzonariesgo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
