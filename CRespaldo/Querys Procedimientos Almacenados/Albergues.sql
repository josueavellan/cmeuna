/***********************************************************************************/
/************************************ Albergues ************************************/

/***********************************************************************************/
/*Llamar a los procedimientos almacenados*/
call consultar_tabla_Albergue(01);
call consultar_un_Albergue(27);
call eliminar_tabla_Albergue;
call eliminar_un_Albergue(1);
call insertar_un_Albergue('nombre', 'capacidad', 'encargado', 'localidad', 'servicios', 'telefono');
call actualizar_un_Albergue(21, 'nombre', 100, 'Andres', 'localidad', 0, 86755409);
call contar_filas_Albergue;
call buscar_especifico_Albergue('dato');
call cargar_nombres_responsables_Albergue;
call obtener_id_responsable_por_nombre_Albergue('nombre');

/***********************************************************************************/
/*Elimar los procediminetos almacenados*/
drop procedure if exists consultar_tabla_Albergue;
drop procedure if exists consultar_un_Albergue;
drop procedure if exists eliminar_tabla_Albergue;
drop procedure if exists eliminar_un_Albergue;
drop procedure if exists insertar_un_Albergue;
drop procedure if exists actualizar_un_Albergue;
drop procedure if exists contar_filas_Albergue;
drop procedure if exists buscar_especifico_Albergue;
drop procedure if exists cargar_nombres_responsables_Albergue;
drop procedure if exists obtener_id_responsable_por_nombre_Albergue;

use unagrupo1;
/***********************************************************************************/
/*Consultar toda la tabla*/
delimiter $$
create procedure consultar_tabla_Albergue (
	in _pagina int(10)
) begin
	select idalbergue, nombrealbergue, capacidadalbergue, nombreresponsable, localidadalbergue, serviciosalbergue, telefonoalbergue FROM tbalbergues 
				INNER JOIN tbresponsable ON tbalbergues.encargadoalbergue = tbresponsable.idresponsable
				ORDER BY idalbergue DESC LIMIT _pagina, 6;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar un Albergue*/
delimiter $$
create procedure consultar_un_Albergue (
	in _id int
) begin
	select * from tbalbergues where idalbergue = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Eliminar toda la tabla*/
delimiter $$
create procedure eliminar_tabla_Albergue ()
begin
	delete from tbalbergues;
end $$
delimiter ;

/***********************************************************************************/
/*eliminar solo un Albergue*/
delimiter $$
create procedure eliminar_un_Albergue (in id int)
begin
	delete from tbalbergues where idalbergue = id;
end $$
delimiter ;

/***********************************************************************************/
/*Insertar un nuevo Albergue*/
delimiter $$
create procedure insertar_un_Albergue (
	in _nombrealbergue varchar(30),
    _capacidadalbergue int(4),
    _encargadoalbergue int(5),
    _localidadalbergue varchar(100),
    _serviciosalbergue int(3),
    _telefonoalbergue int(11)
) begin
	insert into tbalbergues (
		nombrealbergue, 
        capacidadalbergue, 
        encargadoalbergue,
        localidadalbergue.
        serviciosalbergue,
        telefonoalbergue
	) values (
		_nombrealbergue, 
        _capacidadalbergue, 
        _encargadoalbergue,
        _localidadalbergue.
        _serviciosalbergue,
        _telefonoalbergue
	);
end $$
delimiter ;

/***********************************************************************************/
/*Actualizar un Albergue*/
delimiter $$
create procedure actualizar_un_Albergue (
	in _id int,
    _nombrealbergue varchar(30),
    _capacidadalbergue int(4),
    _encargadoalbergue varchar(100),
    _localidadalbergue varchar(100),
    _serviciosalbergue int(3),
    _telefonoalbergue int(11)
) begin
	set @id_responsable := (select idresponsable from tbresponsable where nombreresponsable = _encargadoalbergue);
	update tbalbergues 
	set 
		nombrealbergue = _nombrealbergue,
        capacidadalbergue = _capacidadalbergue,
        encargadoalbergue = @id_responsable,
        localidadalbergue = _localidadalbergue,
        serviciosalbergue = _serviciosalbergue,
        telefonoalbergue = _telefonoalbergue
	where idalbergue = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Contar todas las fillas que existen en la tabla*/
delimiter $$
create procedure contar_filas_Albergue ()
begin
	select count(idalbergue) from tbalbergues;
end $$
delimiter ;

/***********************************************************************************/
/*Buscar una albergue especifico*/
delimiter $$
create procedure buscar_especifico_Albergue (
	in _dato varchar(100)
) begin
	select * from tbalbergues where nombrealbergue = _dato or capacidadalbergue = _dato or localidadalbergue = _dato;
end $$
delimiter ;

/***********************************************************************************/
/*Obrener todos los nombres de los responsables*/
delimiter $$
create procedure cargar_nombres_responsables_Albergue ()
begin
	select nombreresponsable from tbresponsable;
end $$
delimiter ;

/***********************************************************************************/
/*Obtener el id del responsable por el nombre*/
delimiter $$
create procedure obtener_id_responsable_por_nombre_Albergue (in _nombre varchar(30))
begin
	select idresponsable from tbresponsable where nombreresponsable =_nombre;
end $$
delimiter ;

/***********************************************************************************/
/***********************************************************************************/
/***********************************************************************************/
delimiter $$
create procedure eliminar_un_Albergue (in _id int)
begin
    set @nombre := (select nombrealbergue from tbalbergues where idalbergue = _id);
	update tbzonariesgo set alberguezonariesgo = 'Sin definir' where alberguezonariesgo = @nombre;
    delete from tbalbergues where idalbergue = _id;
end $$
delimiter ;