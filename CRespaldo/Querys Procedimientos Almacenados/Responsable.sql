/***********************************************************************************/
/************************************ Responsable **********************************/

/***********************************************************************************/
/*Llamar a los procedimientos almacenados*/
call consultar_tabla_Responsable(00);
call consultar_un_Responsable(14);
call eliminar_tabla_Responsable;
call eliminar_un_Responsable(14);
call insertar_un_Responsable('cedula', 'nombre', 'apellido1', 'apellido2', 'telefono');
call actualizar_un_Responsable('id', 'cedula', 'nombre', 'apellido1', 'apellido2', 'telefono');
call contar_filas_Responsable;
call verificar_cedula_repetida_Responsable('id', 'cedula');

/***********************************************************************************/
/*Elimar los procediminetos almacenados*/
drop procedure if exists consultar_tabla_Responsable;
drop procedure if exists consultar_un_Responsable;
drop procedure if exists eliminar_tabla_Responsable;
drop procedure if exists eliminar_un_Responsable;
drop procedure if exists insertar_un_Responsable;
drop procedure if exists actualizar_un_Responsable;
drop procedure if exists contar_filas_Responsable;
drop procedure if exists verificar_cedula_repetida_Responsable;

use unagrupo1;
/***********************************************************************************/
/*Consultar toda la tabla*/
delimiter $$
create procedure consultar_tabla_Responsable (
	in _pagina int(10)
) begin
	select * from tbresponsable where idresponsable != 0 order by idresponsable desc limit _pagina, 6;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar un Responsable*/
delimiter $$
create procedure consultar_un_Responsable (
	in _id int
) begin
	select * from tbresponsable where idresponsable = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Eliminar toda la tabla*/
delimiter $$
create procedure eliminar_tabla_Responsable ()
begin
	delete from tbresponsable;
end $$
delimiter ;

/***********************************************************************************/
/*eliminar solo un Responsable*/
delimiter $$
create procedure eliminar_un_Responsable (in _id int)
begin
	update tbbodega set responsablebodega = 0 where responsablebodega = _id;
	delete from tbresponsable where idresponsable = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Insertar un nuevo Responsable*/
delimiter $$
create procedure insertar_un_Responsable (
	in _cedularesponsable varchar(11),
    _nombreresponsable varchar(30),
    _apellido1responsable varchar(30),
    _apellido2responsable varchar(30),
    _telefonoresponsable varchar(11)
) begin
	insert into tbresponsable (
	cedularesponsable,
    nombreresponsable,
    apellido1responsable,
    apellido2responsable,
    telefonoresponsable
	) values (
	_cedularesponsable,
    _nombreresponsable,
    _apellido1responsable,
    _apellido2responsable,
    _telefonoresponsable
	);
end $$
delimiter ;

/***********************************************************************************/
/*Actualizar un Responsable*/
delimiter $$
create procedure actualizar_un_Responsable (
	in _id int,
    _cedularesponsable varchar(11),
    _nombreresponsable varchar(11),
    _apellido1responsable varchar(30),
    _apellido2responsable varchar(30),
    _telefonoresponsable varchar(11)
) begin
	update tbresponsable 
	set 
		cedularesponsable = _cedularesponsable,
		nombreresponsable = _nombreresponsable,
		apellido1responsable = _apellido1responsable,
		apellido2responsable = _apellido2responsable,
		telefonoresponsable = _telefonoresponsable
	where idresponsable = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Contar todas las fillas que existen en la tabla*/
delimiter $$
create procedure contar_filas_Responsable ()
begin
	select count(idresponsable) from tbresponsable;
end $$
delimiter ;

/***********************************************************************************/
/*Contar todas las fillas que existen en la tabla*/
delimiter $$
create procedure verificar_cedula_repetida_Responsable (
	in _id int,
    _cedula varchar(11)
) begin
	select idresponsable, cedularesponsable from tbresponsable where cedularesponsable = _cedula;
end $$
delimiter ;

/***********************************************************************************/
/*Buscar Responsable especifico*/