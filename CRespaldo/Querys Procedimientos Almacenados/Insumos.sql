/***********************************************************************************/
/************************************ Insumos **************************************/

/***********************************************************************************/
/*Llamar a los procedimientos almacenados*/
call consultar_tabla_Insumos(01);
call consultar_un_Insumos(27);
call eliminar_tabla_Insumos;
call eliminar_un_Insumos(1);
call insertar_un_Insumos('articulo', 'institucion', 'cantidad', 'detalle', 'bodega');
call actualizar_un_Insumos(27, 'articulo', 'institucion', 'cantidad', 'detalle', 'bodega');
call contar_filas_Insumos;
call buscar_especifico_Insumo('dato');
call cargar_datos_bodega_en_Insumos;
call obtener_id_bodega_por_nombre_de_Insumos('nombre');
call buscar_por_bodega_un_Insumo('id');

/***********************************************************************************/
/*Elimar los procediminetos almacenados*/
drop procedure if exists consultar_tabla_Insumos;
drop procedure if exists consultar_un_Insumos;
drop procedure if exists eliminar_tabla_Insumos;
drop procedure if exists eliminar_un_Insumos;
drop procedure if exists insertar_un_Insumos;
drop procedure if exists actualizar_un_Insumos;
drop procedure if exists contar_filas_Insumos;
drop procedure if exists buscar_especifico_Insumo;
drop procedure if exists cargar_datos_bodega_en_Insumos;
drop procedure if exists obtener_id_bodega_por_nombre_de_Insumos;
drop procedure if exists buscar_por_bodega_un_Insumo;

use unagrupo1;
/***********************************************************************************/
/*Consultar toda la tabla*/
delimiter $$
create procedure consultar_tabla_Insumos (
	in _pagina int(10)
) begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
		from tbinsumo inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega 
        order by idinsumo desc limit _pagina, 6;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar un Insumos*/
delimiter $$
create procedure consultar_un_Insumos (
	in _id int
) begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega 
		from tbinsumo 
		inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
		where idinsumo = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Eliminar toda la tabla*/
delimiter $$
create procedure eliminar_tabla_Insumos ()
begin
	delete from tbinsumo;
end $$
delimiter ;

/***********************************************************************************/
/*eliminar solo un Insumos*/
delimiter $$
create procedure eliminar_un_Insumos (
	in _id int
) begin
	delete from tbinsumo where idinsumo = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Insertar un nuevo Insumos*/
delimiter $$
create procedure insertar_un_Insumos (
	in _articuloinsumo varchar(30),
    _institucioninsumo varchar(11),
    _cantidadinsumo varchar(5),
    _observacioninsumo varchar(100),
    _bodegainsumo int(5)
) begin
	insert into tbinsumo (
		articuloinsumo, 
        institucioninsumo, 
        cantidadinsumo,
        observacioninsumo,
        bodegainsumo
	) values (
		_articuloinsumo,
		_institucioninsumo, 
		_cantidadinsumo,
        _observacioninsumo,
        _bodegainsumo
	);
end $$
delimiter ;

/***********************************************************************************/
/*Actualizar un Insumos*/
delimiter $$
create procedure actualizar_un_Insumos (
	in _id int,
    _articuloinsumo varchar(30),
    _institucioninsumo varchar(30),
    _cantidadinsumo varchar(5),
    _observacioninsumo varchar(100),
    _bodegainsumo int(5)
) begin
	update tbinsumo 
	set 
		articuloinsumo = _articuloinsumo, 
        institucioninsumo = _institucioninsumo, 
        cantidadinsumo = _cantidadinsumo,
        observacioninsumo = _observacioninsumo,
        bodegainsumo = _bodegainsumo
	where idvoluntario = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Conrtar las filas de la tabla de Insumos*/
delimiter $$
create procedure contar_filas_Insumos ()
begin
	select count(idinsumo) from tbinsumo;
end $$
delimiter ;

/***********************************************************************************/
/*Buscar un insumo en especifico*/
delimiter $$
create procedure buscar_especifico_Insumo (
	in _dato varchar(100)
) begin
	select * from tbinsumo where articuloinsumo = _dato
							or institucioninsumo = _dato
							or cantidadinsumo = _dato
							or bodegainsumo = _dato;
end $$
delimiter ;

/***********************************************************************************/
/*Cargar datos de la bodega a Insumos*/
delimiter $$
create procedure cargar_datos_bodega_en_Insumos ()
begin
	select nombrebodega from tbbodega;
end $$
delimiter ;

/***********************************************************************************/
/*Obtener id de la bodega por el nombre*/
delimiter $$
create procedure obtener_id_bodega_por_nombre_de_Insumos (
	in _nombre varchar(100)
) begin
	select idbodega from tbbodega where nombrebodega =_nombre;
end $$
delimiter ;

/***********************************************************************************/
/*Buscar un insumo por nombre de bodega*/
delimiter $$
create procedure buscar_por_bodega_un_Insumo (
	in _id int(5)
) begin
	select idinsumo, articuloinsumo, institucioninsumo, cantidadinsumo, observacioninsumo, nombrebodega
		from tbinsumo 
		inner join tbbodega on tbinsumo.bodegainsumo = tbbodega.idbodega
		where bodegainsumo = _id;
end $$
delimiter ;