/***********************************************************************************/
/************************************ Voluntarios **********************************/

/***********************************************************************************/
/*Llamar a los procedimientos almacenados*/
call consultar_tabla_Voluntario(01);
call consultar_un_Voluntario(12);
call eliminar_tabla_Voluntario;
call eliminar_un_Voluntario(14);
call insertar_un_Voluntario('Michael', '86-75-54-06', 'minami no shima wa');
call actualizar_un_Voluntario(12, 'pero', '11-22-33-44', 'detalle');
call contar_filas_Voluntario;
call buscar_integrante_especifico_Voluntario('22-22-22-22');

/***********************************************************************************/
/*Elimar los procediminetos almacenados*/
drop procedure if exists consultar_tabla_Voluntario;
drop procedure if exists consultar_un_Voluntario;
drop procedure if exists eliminar_tabla_Voluntario;
drop procedure if exists eliminar_un_Voluntario;
drop procedure if exists insertar_un_Voluntario;
drop procedure if exists actualizar_un_Voluntario;
drop procedure if exists contar_filas_responsable;
drop procedure if exists buscar_integrante_especifico_Voluntario;

/***********************************************************************************/
/*Consultar toda la tabla*/
use unagrupo1;
delimiter $$

create procedure consultar_tabla_Voluntario (
	in _pagina int(10)
) begin
	select * from tbvoluntario order by idvoluntario desc limit _pagina, 6;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar un voluntario*/
delimiter $$
create procedure consultar_un_Voluntario (
	in _id int
) begin
	select * from tbvoluntario where idvoluntario = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Eliminar toda la tabla*/
delimiter $$
create procedure eliminar_tabla_Voluntario ()
begin
	delete from tbzonariesgo;
end $$
delimiter ;

/***********************************************************************************/
/*eliminar solo un voluntario*/
delimiter $$
create procedure eliminar_un_Voluntario (in id int)
begin
	delete from tbvoluntario where idvoluntario = id;
end $$
delimiter ;

/***********************************************************************************/
/*Insertar un nuevo Voluntario*/
delimiter $$
create procedure insertar_un_Voluntario (
	in _nombrevoluntario varchar(30),
    _telefonovoluntario varchar(11),
    _detallevoluntario varchar(100)
) begin
	insert into tbvoluntario (
		nombrevoluntario, 
        telefonovoluntario, 
        detallevoluntario
	) values (
		_nombrevoluntario,
		_telefonovoluntario, 
		_detallevoluntario
	);
end $$
delimiter ;

/***********************************************************************************/
/*Actualizar un Voluntario*/
delimiter $$
create procedure actualizar_un_Voluntario (
	in _id int,
    _nombrevoluntario varchar(30),
    _telefonovoluntario varchar(11),
    _detallevoluntario varchar(100)
) begin
	update tbvoluntario 
	set 
		nombrevoluntario = _nombrevoluntario,
		telefonovoluntario = _telefonovoluntario,
		detallevoluntario = _detallevoluntario
	where idvoluntario = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Contar las filas de la tabla de un Voluntario*/
delimiter $$
create procedure contar_filas_Voluntario ()
begin
	select count(idvoluntario) from tbvoluntario;
end $$
delimiter ;

/***********************************************************************************/
/*Buscar un integrante en especifico*/
delimiter $$
create procedure buscar_integrante_especifico_Voluntario (
	in _dato varchar(100)
) begin
	select * from tbvoluntario where nombrevoluntario = _dato or telefonovoluntario = _dato;
end $$
delimiter ;