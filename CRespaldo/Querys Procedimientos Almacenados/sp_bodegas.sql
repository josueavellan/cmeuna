/***********************************************************************************/
/*****************Procedimientos almacenados de Bodega *****************************/

/***********************************************************************************/
/*Llamar a los procedimientos almacenados*/
call consultar_tabla_Bodega(01);
call consultar_una_Bodega(12);
call eliminar_tabla_Bodega;
call eliminar_una_Bodega(2);
call insertar_una_Bodega ('nombre', 'descripcion', 'direccion', 'Dennis');
call actualizar_una_Bodega(01, 'nombre', 'descripcion', 'direccion', 'Dennis');
call contar_filas_Bodega;
call buscar_integrante_especifico_Bodega('22-22-22-22');
call obtener_nombres_responsables_de_bodega;
call obtener_id_responsable_por_nombre('nombre');
call si_tiene_llave_foranea_Bodega(01);
call obtener_nombre_responsable_por_id_bodega(02);

/***********************************************************************************/
/*Elimar los procediminetos almacenados*/
drop procedure if exists consultar_tabla_Bodega;
drop procedure if exists consultar_una_Bodega;
drop procedure if exists eliminar_tabla_Bodega;
drop procedure if exists eliminar_una_Bodega;
drop procedure if exists insertar_una_Bodega;
drop procedure if exists actualizar_una_Bodega;
drop procedure if exists contar_filas_Bodega;
drop procedure if exists buscar_integrante_especifico_Bodega;
drop procedure if exists obtener_nombres_responsables_de_bodega;
drop procedure if exists obtener_id_responsable_por_nombre;
drop procedure if exists si_tiene_llave_foranea_Bodega;
drop procedure if exists obtener_nombre_responsable_por_id_bodega;

/***Es para grupo1una*/
use unagrupo1;

/***********************************************************************************/
/*Consultar toda la tabla de bodegas*/
delimiter $$
create procedure consultar_tabla_Bodega (in _pagina int(10))
begin
	/*select * from tbbodega order by idbodega desc limit _pagina, 6;*/
    select idbodega, nombrebodega, descripcionbodega, direccionbodega, nombreresponsable 
		from tbbodega inner join tbresponsable on tbbodega.responsablebodega = tbresponsable.idresponsable
        where idbodega != 1;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar una bodega*/
delimiter $$
create procedure consultar_una_Bodega (in _id int)
begin
	select * from tbbodega where idbodega = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Eliminar toda la tabla de bodegas*/
delimiter $$
create procedure eliminar_tabla_Bodega ()
begin
	delete from tbbodega;
end $$
delimiter ;

/***********************************************************************************/
/*eliminar solo una bodega*/
delimiter $$
create procedure eliminar_una_Bodega (in _id int)
begin
    update tbinsumo set bodegainsumo = 1 where bodegainsumo = _id;
    delete from tbbodega where idbodega = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Insertar una nueva bodega*/
delimiter $$
create procedure insertar_una_Bodega (
	in _nombreBodega varchar(30),
    _descripcionBodega varchar(100),
    _direccionBodega varchar(100),
    _responsableBodega varchar(30)
) begin
	insert into tbbodega (
		nombrebodega, 
        descripcionbodega,
        direccionbodega,
        responsablebodega
	) values (
		_nombreBodega,
		_descripcionBodega, 
		_direccionBodega,
        _responsableBodega
	);
end $$
delimiter ;

/***********************************************************************************/
/*Actualizar una bodega*/
delimiter $$
create procedure actualizar_una_bodega (
	in _id int,
    _nombrebodega varchar(30),
    _descripcionbodega varchar(100),
    _direccionbodega varchar(100),
    _responsablebodega int(5)
) begin
	update tbbodega 
	set 
		nombrebodega = _nombrebodega,
		descripcionbodega = _descripcionbodega,
		direccionbodega = _direccionbodega,
        responsablebodega = _responsablebodega
	where idbodega = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Contar las filas de la tabla bodegas*/
delimiter $$
create procedure contar_filas_Bodegas ()
begin
	select count(idbodega) from tbbodega;
end $$
delimiter ;

/***********************************************************************************/
/*Buscar un integrante en especifico*/
delimiter $$
create procedure buscar_integrante_especifico_Bodega (in _dato varchar(100))
begin
    select * from tbbodega where responsablebodega = _dato or direccionbodega = _dato or nombrebodega = _dato;
end $$
delimiter ;

/***********************************************************************************/
/*Obtener los ombres de los responsables*/
delimiter $$
create procedure obtener_nombres_responsables_de_bodega ()
begin
	select nombreresponsable from tbresponsable;
end $$
delimiter ;

/***********************************************************************************/
/*obtener id resppnsable por el nombre*/
delimiter $$
create procedure obtener_id_responsable_por_nombre (in _nombre varchar(100))
begin
	select idresponsable from tbresponsable where nombreresponsable = _nombre;
end $$
delimiter ;

/***********************************************************************************/
/*verificar si tiene llave foranea.*/
delimiter $$
create procedure si_tiene_llave_foranea_Bodega (in _id varchar(5))
begin
	select * from tbbodega inner join tbinsumo on tbinsumo.bodegainsumo = tbbodega.idbodega where idbodega = _id;
end $$
delimiter ;

/***********************************************************************************/
/*obtener el nombre del responsable por el id*/
delimiter $$
create procedure obtener_nombre_responsable_por_id_bodega (in _id varchar(5))
begin
	select nombreresponsable from tbresponsable where idresponsable = _id;
end $$
delimiter ;