/***********************************************************************************/
/************************************ZONAS DE RIESGO********************************/

/***********************************************************************************/
/*Llamar a los procedimientos almacenados*/
call consultar_tabla_ZonaRiesgo;
call consultar_una_ZonaRiesgo(27);
call eliminar_tabla_ZonaRiesgo;
call eliminar_una_ZonaRiesgo(1);
call insertar_una_ZonaRiesgo('lugar', 'tipo', 'albergue', 1.0, 2.0);
call actualizar_una_ZonaRiesgo(27, 'lugar', 'tipo', 'albergue');
call consultar_nombres_albergues;
call eliminar_albergues_de_zonas_de_riesgo(7);

/***********************************************************************************/
/*Elimar los procediminetos almacenados*/
drop procedure if exists consultar_tabla_ZonaRiesgo;
drop procedure if exists consultar_una_ZonaRiesgo;
drop procedure if exists eliminar_tabla_ZonaRiesgo;
drop procedure if exists eliminar_una_ZonaRiesgo;
drop procedure if exists insertar_una_ZonaRiesgo;
drop procedure if exists actualizar_una_ZonaRiesgo;
drop procedure if exists consultar_nombres_albergue;
drop procedure if exists eliminar_albergues_de_zonas_de_riesgo;

use unagrupo1;
/***********************************************************************************/
/*Consultar toda la tabla*/
delimiter $$
create procedure consultar_tabla_ZonaRiesgo ()
begin
	select * from tbzonariesgo;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar una Zona de Riesgo*/
delimiter $$
create procedure consultar_una_ZonaRiesgo (
	in _id int
) begin
	select * from tbzonariesgo where idzonariesgo = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Eliminar toda la tabla*/
delimiter $$
create procedure eliminar_tabla_ZonaRiesgo ()
begin
	delete from tbzonariesgo;
end $$
delimiter ;

/***********************************************************************************/
/*eliminar solo una Zona de Riesgo*/
delimiter $$
create procedure eliminar_una_ZonaRiesgo (in id int)
begin
	delete from tbzonariesgo where idzonariesgo = id;
end $$
delimiter ;

/***********************************************************************************/
/*Insertar una nueva Zona de Riesgo*/
delimiter $$
create procedure insertar_una_ZonaRiesgo (
	in _lugar varchar(100),
    _tipo_riesgo varchar(30),
    _albergue_cercano varchar(100),
    _latitud double,
    _longitud double
) begin
	insert into tbzonariesgo (
		lugarzonariesgo, 
        tipozonariesgo, 
        alberguezonariesgo, 
        latitud, 
        longitud
	) values (
		_lugar,
		_tipo_riesgo, 
		_albergue_cercano, 
		_latitud, 
		_longitud
	);
end $$
delimiter ;

/***********************************************************************************/
/*Actualizar una Zona de Riesgo*/
delimiter $$
create procedure actualizar_una_ZonaRiesgo (
	in _id int,
    _lugar varchar(100),
    _tipo_riesgo varchar(30),
    _albergue_cercano varchar(100)
) begin
	update tbzonariesgo 
	set 
		lugarzonariesgo = _lugar,
		tipozonariesgo = _tipo_riesgo,
		alberguezonariesgo = _albergue_cercano
	where idzonariesgo = _id;
end $$
delimiter ;

/***********************************************************************************/
/*Consultar los nombres de los albergues*/
delimiter $$
create procedure consultar_nombres_albergues ()
begin
	select nombrealbergue from tbalbergues;
end $$
delimiter ;

/***********************************************************************************/
/***********************************************************************************/
/***********************************************************************************/
/*************************** Insertar datos quemados ********************************/
call insertar_una_ZonaRiesgo('Casa de Michael', 'Incendio', 'ASADA', 1.0, 2.0);
call insertar_una_ZonaRiesgo('Casa de Dennis', 'Caida de meteorito', 'ASADA', 1.0, 2.0);
call insertar_una_ZonaRiesgo('Casa de Josue', 'Inundacion', 'ASADA', 1.0, 2.0);
call insertar_una_ZonaRiesgo('Univercidad', 'Sequia', 'albergue', 1.0, 2.0);
call insertar_una_ZonaRiesgo('Cruce de Cisneros', 'Atropello', 'ASADA', 1.0, 2.0);
call insertar_una_ZonaRiesgo('Centro de la Victoria', 'Robo', 'ASADA', 1.0, 2.0);
call insertar_una_ZonaRiesgo('Puente Rojo', 'Negro de Wasap', 'ASADA', 1.0, 2.0);

/***********************************************************************************/
/***********************************************************************************/
/*desuso*/
delimiter $$
create procedure eliminar_albergues_de_zonas_de_riesgo (
	in _id int
) begin
    set @nombre := (select nombrealbergue from tbalbergues where idalbergue = _id);
	update tbzonariesgo set alberguezonariesgo = 'Sin definir'
    where alberguezonariesgo = @nombre;
end $$
delimiter ;
